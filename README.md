# croc_wlt: wafer-level testing software for CMS Readout Chips (CROCs)

Python software to perform wafer-level testing of the CMS Readout Chips for High
Luminosity LHC.

## Installation

The information for setting up and running the software can be found in the
[Software guide][software guide] page of the project's [Wiki page][project wiki].

## Documentation

The documentation of **croc_wlt** is divided in two levels: the first is the
**croc_wlt** GitLab Wiki; the second is the low-level source code documentation
via docstrings.

### Wiki

The documentation for **croc_wlt** can be found in the
[project's Wiki page][project wiki]. The Wiki describes both the hardware and
software setups and also contains a software guide which describes how to setup
and use the software.

### Low-level documentation

The Low-level documentation for the package and its modules can be found in the
source code as Python docstrings. It can also be read from the command line
using the `pydoc` tool.

For example, to read the documentation for the `chiptester` module, you can
run:

```
pydoc wlt.chiptester
```

More information on `pydoc` can be found on its [Python documentation page].

[project wiki]: https://gitlab.cern.ch/croc_testing/croc_wlt/-/wikis/home
[software guide]: https://gitlab.cern.ch/croc_testing/croc_wlt/-/wikis/Software-guide
[Python documentation page]: https://docs.python.org/3/library/pydoc.html
