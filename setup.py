from setuptools import setup, find_packages

setup(
	name = "croc_wlt",
	version = "0.1",
	author = "Michael Grippo",
	description = (
		"Python software to perform wafer-level testing of the CMS Readout "
		"Chips for High Luminosity LHC"
	),
	packages = find_packages(),
	install_requires = [
		"pyserial",
		"numpy",
		"toml",
		"matplotlib",
		"PyPDF2"
	],
	#entry_points = {}
)
