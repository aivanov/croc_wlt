#!/bin/bash

###
#
# Note about Ph2_ACF's setup.sh:
# it's not possible to source it from another folder because
# the script sets the Ph2_ACF working folder using $(pwd).
#
# A better way of getting the path to the folder which contains
# the script is to use the following:
# BASE_DIR = $(  realpath $( dirname ${BASH_SOURCE[0]} )  )
#
###

###
#
# To do:
# - edit the paths in CMSIT.xml depending on the environment
#   variables set in this script
#
###

HEADER=$(printf %.s_ {1..60})
HEADER+="\n"

echo -e "$HEADER"

echo "* Defining environment variables"
export WLT_DIR="$( realpath $( dirname ${BASH_SOURCE[0]} ) )"
export DAQ_DIR="$WLT_DIR/daq"
export DATA_DIR="$WLT_DIR/data"

ACF_LOCATION="/home/waferprobing/scratch/Ph2_ACF/current"

if test -e "$ACF_LOCATION"; then

	export ACF_DIR="$ACF_LOCATION"

	echo "* Sourcing Ph2_ACF's setup.sh"
	cd $ACF_DIR
	source setup.sh > /dev/null
	cd $OLDPWD

else

	echo "* $(basename $ACF_LOCATION) folder not found in $(realpath $(dirname $ACF_LOCATION ) )"
	echo "* Adjust the ACF_LOCATION variable and try again"

fi

echo "* All done"

echo -e "$HEADER"
