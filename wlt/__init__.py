"""
Python package for the wafer-level testing (WLT) of the CMS Readout Chips(CROCs)
for the High Luminosity upgrade.

Modules:
	wafertester: operates the probe station (chuck movement, status, ...)

	chiptester: runs a certain list of tests on a chip

	wafermap: utility module to create wafer maps with the wafer probing results

	waferanalyzer: analyzes the outputs of the wafer probing process

Packages:
	hardware: contains the libraries used to manage the probe station, the 
		power supplies and the Wafer Probing Auxiliary Card (WPAC)
	daq: manages the CMS Inner Tracker DAQ
	analysis: contains the utilities used for the analysis of waferprobing 
		results
"""

import os

#constants used for the powering **mode** of the chip
LDO_POWERING = "ldo"
SHUNT_POWERING = "shunt"
DIRECT_POWERING = "direct"

#constants for the powering **configuration** of the chip
STARTUP_POWER = "startup_power"
STANDARD_POWER = "standard_power"
LOW_POWER = "low_power"

#class used to stop the waferprobing process altogether
class AbortWaferprobing(Exception):
	"""Interrupts the waferprobing process when raised."""


#class used to stop the waferprobing process in case of high contact resistance
class HighContactResistance(AbortWaferprobing):
	"""
	Exception raised when the contact resistance detected is too high.

	It inherits from AbortWaferprobing, so it is capable of aborting the 
	waferprobing process.
	"""


#checking that the setup.sh file has been sourced
if "WLT_DIR" not in os.environ:
	raise RuntimeError("Environment variables not defined because the setup.sh "
		"script has not been sourced. Perform this operation and try again.")
