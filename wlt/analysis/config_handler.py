"""Handler utility for analysis configuration files"""

import argparse

parser = argparse.ArgumentParser(description='Default configuration for the WaferAnalyzer')
parser.add_argument('name', type=str, default=None, nargs='?',
                    help='Name of the configuration to display')
parser.add_argument('-d', '--debug', action='store_true',
                    help='Run the debugger to examine the configurations')
args = parser.parse_args()

def handle(configs):
	"""Handling the command-line options"""

	# Displaying the specified configuration configuration
	if args.name is not None:
		if args.name in configs:
			print(f'Displaying configuration: {args.name}')
			print(configs[args.name])
		else:
			print(f'Configuration not defined: {args.name}')

	else:
		# Counting the configurations (-1 for the GLOBAL configuration)
		print(f'Total number of configurations: {len(configs)-1}')
		counts = {'wafer': [0, 0], 'chip': [0, 0]}
		for name, config in configs.items():
			dest = 'wafer'
			if 'aggregate' not in config or config['aggregate'] != 'histogram':
				dest = 'chip'
			counts[dest][0] += 1
			if config['skip']:
				counts[dest][1] += 1

		for name, (n_total, n_skipped) in counts.items():
			print(f'  {name}: {n_total}  ({n_skipped} skipped)')
