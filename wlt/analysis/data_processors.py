#pylint: disable = E1101, R0914
"""
Collection of functions for common data-processing tasks,
which can be used inside lambda functions of the plotting config files.
"""

import numpy as np
import ROOT as R

from wlt.analysis.utils import is_iterable


def fit_pol(data=None, x=None, y=None, fit_range=None, sigma=None, deg=1):
	"""Straight-line fit."""
	default = tuple([[0.0]*(deg+1)] + [-1.0, [], []])
	vals_x = []
	vals_y = []
	if data is not None and isinstance(data, dict):
		for k, v in data.items():
			vals_x.append(k)
			vals_y.append(v)
	elif x is not None and y is not None:
		for ix, iy in zip(x, y):
			vals_x.append(ix)
			vals_y.append(iy)
	else:
		print('ERROR: Neither data nor x-y points provided for `fit_pol1`')
		return None
	# Filtering the data points according to the fit range
	v_x = []
	v_y = []
	for ix, iy in zip(vals_x, vals_y):
		if fit_range and fit_range[0] is not None:
			if ix < fit_range[0][0] or ix > fit_range[0][1]:
				continue
		if fit_range and fit_range[1] is not None:
			if iy < fit_range[1][0] or iy > fit_range[1][1]:
				continue
		v_x.append(ix)
		v_y.append(iy)

	if len(v_x) > deg+1:
		# Performing the polynomial fit
		weights = None
		if is_iterable(sigma):
			weights = [1.0/s for s in sigma]
		elif sigma is not None:
			weights = [1.0/sigma]*len(v_x)
		result = np.polyfit(v_x, v_y, deg, w=weights, full=True)
		par = result[0][::-1]
		res = result[1]
		# Calculating the chi-squared
		if len(res) < 1:
			return default
		chi2 = res[0] / (len(v_x) - deg - 1)
		# Returning the polynomial parameter values with the lowest order first
		return par, chi2, v_x, v_y

	if len(v_x) == deg+1:
		# Calculating the parameters directly
		chi2 = 1.0
		slope = (v_y[1] - v_y[0]) / (v_x[1] - v_x[0])
		offset = v_y[0] - v_x[0] * slope
		par = [offset, slope]

		return par, chi2, v_x, v_y

	return default


def fit_gauss_root(hist, opt='W', return_input=False):
	"""Fit a distribution with a gaussian"""

	# Defining the output container
	output = {
		'const': 0, 'mean': 0, 'sigma': 0,
		'chi2': 0, 'v_x': [], 'v_y': [], 'type': 'gaus'
	}

	if not hist:
		return output

	# Creating the Gaussian fit function
	fitf = R.TF1('fitf', 'gaus')
	# Initialising the parameters
	try:
		bin_max = hist.GetMaximumBin()
		fitf.SetParameter(0, hist.GetBinContent(bin_max))
		fitf.SetParameter(1, hist.GetBinCenter(bin_max))
		fitf.SetParameter(2, hist.GetRMS()/2.0)
	except ValueError:
		print('ERROR: Trying to fit object of unsupported type:', hist)
		return output

	# Defining the fit range
	rng_left = fitf.GetParameter(1) - hist.GetRMS()*3
	rng_right = fitf.GetParameter(1) + hist.GetRMS()*3
	# Filling the lists of points to be fitted
	bin_id = hist.FindBin(rng_left)
	while bin_id < hist.GetNbinsX():
		if hist.GetBinLowEdge(bin_id) > rng_right:
			break
		output['v_x'].append(hist.GetBinCenter(bin_id))
		output['v_y'].append(hist.GetBinContent(bin_id))
		bin_id += 1
	# Fitting the distribution
	fitr = hist.Fit(fitf, 'S0Q'+opt, '', rng_left, rng_right)
	if fitr == 0:
		output['const'] = fitf.GetParameter(0)
		output['mean'] = fitf.GetParameter(1)
		output['sigma'] = fitf.GetParameter(2)
		output['chi2'] = fitf.GetChisquare()

	if return_input:
		return hist

	return output


def th2_to_arr(obj):
	"""Creates a 1D distribution of Z values from a 2D histogram"""
	arr = np.asarray(R.TArrayF(obj), dtype=np.float16)
	# Setting bins with unphysical values to 0
	arr[np.isnan(arr)] = 0
	arr[arr == float('+inf')] = 0
	arr[arr == float('-inf')] = 0
	return arr


def arr_to_th1(arr, binning=None):
	"""Converts the input array to a TH1D histogram"""
	# Using automatic binning if no binning defined
	if binning is None:
		binning = np.histogram_bin_edges(arr, 'auto')
		binning = (len(binning)-1, binning[0], binning[1])
	# Creating the histogram
	histo = R.TH1D('h_temp', '', int(binning[0]), binning[1], binning[2])
	# Filling all the entries into the histogram
	histo.FillN(len(arr), arr.astype(np.float64), R.nullptr)

	return histo


def mean(arr):
	"""Calculates the Mean of the input data array."""

	return np.mean(arr)


def rms(arr):
	"""Calculates the RMS of the input data array."""

	return np.std(arr)

def median(arr):
	"""Calculates the Median of the input data array."""

	return np.median(arr)
