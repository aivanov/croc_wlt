"""Handler utility for region-definition configuration files"""

import argparse
import re
import importlib.util

parser = argparse.ArgumentParser(description='Region definitions for the WaferAnalyzer')
parser.add_argument('name', type=str, default=None, nargs='?',
                    help='Name of the region to display')
parser.add_argument('-c', '--compare', type=str, metavar='FILE.py',
                    help='Compare to regions from the input Python file')
args = parser.parse_args()

def handle(regions):
	"""Handling command-line options in an analyzer_regions definition file"""

	# Loading the input regions to be compared to the current one
	regions_in = None
	if args.compare:
		config_loader = importlib.util.spec_from_file_location('REGIONS', args.compare)
		config = importlib.util.module_from_spec(config_loader)
		config_loader.loader.exec_module(config)
		regions_in = config.REGIONS

		for name, region_def in regions_in.items():
			if name not in regions:
				print(f'- {name}: \t{region_def}')

	# Displaying the configured regions
	for name, region_def in regions.items():
		if args.name is not None and not re.search(args.name, name):
			continue
		region, _, _, _, _ = region_def
		# Checking for correct ordering
		for i_value, value in enumerate(region[:-1]):
			if value > region[i_value+1]:
				print(f'ERROR: Values not in ascending order: {name}')
				print(region)
		if not region[-1] > region[0]:
			print(f'ERROR: Values not in ascending order: {name}')
			print(region)
		# Comparing to the input definitions
		prefix = ''
		if regions_in:
			if name not in regions_in:
				prefix = '+'
			elif regions_in[name][0] == region_def[0]:
				continue
			else:
				prefix = '~'
		# Not doing anything if no comparison to be done
		if prefix == '':
			continue
		print(f'{prefix} {name}: \t{region_def}')
		# Displaying definition from the comparison file if it differs
		if prefix == '~':
			print(' '*(len(name)+4) + f'\t{regions_in[name]}')
	print('Validation finished')
