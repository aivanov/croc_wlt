#pylint: disable = E1101, W0702
"""Utility functions."""

import os
import re
import datetime
import copy

import ROOT as R

RE_INPUT_JSON = {
	# chiptester format used in current tests
	'chiptester': r'chip_(\w+)-(\w+)-(\w)(\w)_(\d+)_(\d+)',
	# old chiptester format used for wafer N6Y250-04F3
	'chiptester_old': r'chip_(\w+)x(\w+)(\w)(\w)_(\d+)_(\d+)',
	# waferanalyzer output
	'waferanalyzer': r'_wafer_data_(\w+)-(\w+)_(\d+)_(\d+)',
}


def parse_filename(filename, strict=False):
	"""Parses a filename of the log file to extract bookkeeping information 
	from it."""

	file_name, _ = os.path.splitext(filename)
	# chiptester/waferanalyzer output formats
	patterns = dict(RE_INPUT_JSON)
	if strict:
		for name in patterns:
			patterns[name] = fr'^{patterns[name]}$'
	matches = None
	# Trying each pattern
	for name, pattern in patterns.items():
		matches = re.search(pattern, file_name, re.IGNORECASE)
		if matches:
			break
	if not matches:
		if not strict:
			print(f'ERROR: Could not parse filename: {filename}')
		return None
	# Constructing the parse output dictionary
	chip_id = (0, 0)
	batch_id = matches.group(1)
	wafer_id = matches.group(2)
	if 'chiptester' in name:
		chip_id = (int(matches.group(3), 16), int(matches.group(4), 16))
		date, time = matches.group(5), matches.group(6)
	elif name == 'waferanalyzer':
		date, time = matches.group(3), matches.group(4)
	scan_time = datetime.datetime.strptime(f'{date} {time}', '%Y%m%d %H%M%S')
	return {'batch': batch_id, 'wafer': wafer_id, 'chip': chip_id, 'scan_time': scan_time}


def chip_name_to_id(name):
	"""Converts chip name to the ID tuple of integers"""
	if not isinstance(name, str) or len(name) != 2:
		raise ValueError(f'Chip ID `{name}` should consist of two values: column, row')
	try:
		return tuple(int(v, 16) for v in name)
	except:
		raise ValueError(f'Invalid chip name: {name}')


def parse_json_in(x):
	"""Parser of the JSON input to convert text to the proper format."""

	if all(k.isnumeric() for k in x.keys()):
		return {int(k): v for k, v in x.items()}
	return x


def is_iterable(x):
	"""Checks whether a variable is of iterable type."""

	try:
		iter(x)
		return True
	except:
		return False


def optimal_binsize(regions, bin_size=1.0, nbins_min=10, nbins_max=100):
	"""Calculates the optimal bin size for the given axis range"""

	rng = max(regions) - min(regions)
	# Making finer binning if the default is too coarse
	while rng / bin_size < nbins_min:
		bin_size /= 5.0
	# Making coarser binning if the default is too fine
	scales = [i*j for i in (1, 10, 100, 1000) for j in (1, 2, 5)]
	for scale in scales:
		bin_size *= scale
		if rng / bin_size < nbins_max:
			break
	return bin_size


def dict_data_by_path(data, path, path_delimiter='/', key_delimiter=':'):
	"""Finds the element inside a dictionary using `parent/child` path 
	notation."""

	path_sequence = path.split(path_delimiter)
	data_current = data
	try:
		for path_name in path_sequence:
			if data_current is None:
				break
			# Using name as a key:value filter in a list
			if key_delimiter in path_name:
				element_selected = None
				for element in data_current:
					element_is_valid = True
					# Checking against each key:value pair separated by '&'
					for key_value_cut in path_name.split('&'):
						key, value = key_value_cut.split(key_delimiter)
						# Skipping element if any key:value requirement not satisfied
						if str(element[key]) != value:
							element_is_valid = False
							break
					# Picking the first matching element
					if element_is_valid:
						element_selected = element
						break
				data_current = element_selected
			# Using name as a dictionary key
			else:
				key_found = False
				# Checking the name against each key
				for key in data_current.keys():
					if not re.search(fr'^{path_name}$', str(key)):
						continue
					data_current = data_current[key]
					key_found = True
					break
				if not key_found:
					raise KeyError()
		return data_current
	except KeyError:
		return None


def obj_by_path(file_id, path, path_delimiter='/', file_delimiter=':'):
	"""Finds an object inside a file supporting ROOT file inputs"""

	if not path.startswith('@FILE'):
		return None

	# Building the input file path
	file_base = os.path.splitext(file_id)[0]
	file_path, obj_path = path.split(file_delimiter)
	file_path = file_path.replace('@FILE', file_base)

	# Stopping if input file doesn't exist
	if not os.path.isfile(file_path):
		return None

	# Opening the ROOT file
	if os.path.splitext(file_path)[1] == '.root':
		obj = None
		file_in = R.TFile(file_path)

		path_sequence = obj_path.split(path_delimiter)
		# Trying every element of the sequence
		for i_el, path_el in enumerate(path_sequence):
			# Getting the object from current directory when it's the last element
			if i_el == len(path_sequence) - 1:
				if path_el.startswith('*'):
					obj = R.gDirectory.FindObjectAny(path_el)
				else:
					obj = R.gDirectory.Get(path_el)
				break
			# Entering directly into the directory if full name is provided
			if '*' not in path_el:
				R.gDirectory.Get(path_el).cd()
			else:
				dir_list = R.gDirectory.GetListOfKeys()
				for key in dir_list:
					dir_name = key.GetName()
					if not re.match(path_el, dir_name):
						continue
					R.gDirectory.Get(dir_name).cd()

		# Copying the object into memory and closing the file
		obj = copy.deepcopy(obj)
		file_in.Close()

		return obj
	return None
