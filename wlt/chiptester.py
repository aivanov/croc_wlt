#!/usr/bin/env python3
"""
Module used for handling the chip-testing component of the software.

This module defines the ChipTester class, which is the object that performs
the various tests on the chip. This class interacts with the CMS DAQ and with
the power supplies; it doesn't, however, interact with the probe station. It
also sends commands to the Wafer Probing Auxiliary Card (WPAC) exploiting the
library located in wlt.hardware.wpac. Moreover, this module uses the power
supplies package wlt.hardware.instruments.
"""

import os
import sys
import json
import logging
import time

from wlt import LDO_POWERING
from wlt import SHUNT_POWERING
from wlt import DIRECT_POWERING

from wlt import STARTUP_POWER
from wlt import STANDARD_POWER
from wlt import LOW_POWER

from wlt.duts import get_waferprobing_id
from wlt.duts.chips import ITKPIX_CHIP
from wlt.duts.chips import CROC_CHIP
from wlt.duts.chips import CHIPS
from wlt.duts.chips import CHIP_DATA

from wlt.config import main_config
from wlt import daq
from wlt.daq import DAQException, DAQTimeout
from wlt import AbortWaferprobing
from wlt import HighContactResistance
from wlt.hardware.wpac import wpac
import wlt.hardware.instruments as instr

#TODO: make method to measure and log temperature sensors on the WPAC

class TestFailed(Exception):
	"""Exception raised when a testing routine fails unexpectedly at runtime."""


class AbortTesting(Exception):
	"""Exception raised in case chip testing must be stopped, such as when the
	edge sensors do not signal wafer contact."""


class ChipTester:
	"""
	Class which manages the testing of the RD53 chips.

	This class handles the power supplies, the WPAC and interacts with the CMS
	DAQ in order to carry out the tests.
	"""

	def __init__(self, chip_type, chip_col, chip_row, batch_id, wafer_id,
			enabled_tests, config_tests, data_dir = None,
			log_level = logging.INFO):
		"""
		Initializes the ChipTester object.

		The constructor sets up the class-level logging and checks the input
		parameters.

		Parameters
		----------
		chip_type : str
			The type of the chip under test. The valid values are defined by 
			the chip constants defined in `wlt.duts.chips`.
		chip_col, chip_row : int
			Chip position on the wafer.
		batch_id : str
			Wafer batch identifier from foundry.
		wafer_id : str
			Wafer identifier from foundry.
		enabled_tests : dict
			Dictionary containing the enabled tests.
		config_tests: dict
			Dictionary containing the configuration for the various tests.
		data_dir : str or path, optional
			Location in which the logs and results are saved.
		log_level : int, optional
			Log level for the class-level logger. Use default log levels from 
			the logging Python module.

		Raises
		------
		ValueError
			This exception is raised in the following cases: no list of enabled 
			tests is provided; invalid chip type.
		RuntimeError
			This exception is raised if the device files for the instruments 
			can not be opened.
		"""

		#chip location information
		self.chip_col = chip_col
		self.chip_row = chip_row
		self.batch_id = batch_id
		self.wafer_id = wafer_id

		#adding a custom chip configuration
		self.chip_config = {
			#custom values for the chip registers
			"registers": {}
		}

		#string representation of chip information
		self.chip_str = (
			f"{self.batch_id}-{self.wafer_id}-"
			f"{self.chip_col:X}{self.chip_row:X}"
		)

		#chip type attribute initialization
		self.chip_type = None

		#checking the enabled_tests parameter
		if enabled_tests is None:
			raise ValueError("The dict of enabled tests must be provided!")

		#initialization of enabled_tests attribute
		self.enabled_tests = enabled_tests

		#checking the config_tests parameter
		if config_tests is None:
			raise ValueError("The tests configuration must be provided!")

		#initialization of the config_tests attribute
		self.config_tests = config_tests

		#checking chip type
		if chip_type not in CHIPS:
			raise ValueError(f"Invalid chip type: {chip_type}")
		self.chip_type = chip_type

		#checking the data_dir parameter
		if data_dir is None:
			data_dir = main_config.DATA_DIR

		#checking if the data folder exists
		if not os.path.isdir(data_dir):
			os.makedirs(data_dir)

		#setting the data_dir attribute
		self.data_dir = data_dir

		#setting the wpac attribute
		#TO DO: remove from ChipTester attributes
		self.wpac = None

		#setting the powering mode attribute
		self.powering_mode = None

		#generating the base file name with the chip serial number and the
		#current time. This is the common part of file names that is used for
		#the outputs (JSON, ROOT, logs, ...)
		init_time = time.strftime("%Y%m%d_%H%M%S")
		self.base_file_name = f"chip_{self.chip_str}_{init_time}"

		#initializing the DAQ retries dictionary
		self.daq_retries = {}

		#################
		# logging setup #
		#################

		#log level attribute
		self.log_level = log_level

		#main logger
		#TODO: using logging.LoggerAdapter class in order to add chip SN?
		self.logger = logging.getLogger(
			f"ChipTester ({self.wafer_id}-"
			f"{self.chip_col:X}{self.chip_row:X})"
		)
		self.logger.setLevel(self.log_level)

		#file handler
		log_file_name = f"{self.base_file_name}.log"
		log_file_path = os.path.join(self.data_dir, log_file_name)
		file_handler = logging.FileHandler(log_file_path)
		file_handler.setLevel(self.log_level)

		#console handler
		console_handler = logging.StreamHandler()
		console_handler.setLevel(self.log_level)

		#defining formatter
		log_formatter = logging.Formatter(
			"%(asctime)s | %(name)-20s | %(levelname)-8s | %(message)s"
		)

		#setting formatters
		file_handler.setFormatter(log_formatter)
		console_handler.setFormatter(log_formatter)

		#adding handlers
		self.logger.addHandler(file_handler)
		self.logger.addHandler(console_handler)

		###########
		# results #
		###########

		#dictionary to store the test results
		self.results = {}

		#adding chip data to the results
		self.results["chip_data"] = { #should also add enabled tests and config
			"batch_id": self.batch_id,
			"wafer_id": self.wafer_id,
			"chip_col": self.chip_col,
			"chip_row": self.chip_row
		}

		#initialization of results file
		data_file_name = f"{self.base_file_name}.json"
		self.data_file = os.path.join(self.data_dir, data_file_name)

	#decorator used to manage DAQ communication problems
	def _safe_daq_run(self, func, n_tries = 2):
		"""
		Decorator used to run again a certain DAQ test if a DAQException is 
		detected.

		Parameters
		----------
		func : function
			The function to be decorated.
		n_tries : int
			Maximum number of tries.

		Returns
		-------
		function
			The decorated DAQ function.

		Raises
		------
		RuntimeError
			If the run fails.
		"""

		def inner(*args, **kwargs):
			is_ok = False
			#setting to 0 the number of retries if there are no data
			if func.__name__ not in self.daq_retries.keys():
				self.daq_retries[func.__name__] = 0
			#loop on the number of tries to be performed
			for i in range(n_tries):
				#try to run the DAQ function
				try:
					#calling the function
					val = func(*args, **kwargs)
					#code for handling the successful execution
					is_ok = True
					self.logger.debug(f"DAQ function {func.__name__} executed "
						"successfully")
					#the run has been successful, so don't increment the counter
					break
				#handle a DAQ error
				except (DAQException, RuntimeError) as e:
					self.logger.error(f"DAQ function {func.__name__} "
						"terminated unexpectedly!")
					self.logger.debug(e)
					#increment the DAQ retries counter
					self.daq_retries[func.__name__] += 1
			#checking the result
			if is_ok is False:
				raise RuntimeError("Could not run the requested DAQ function!")

			#return of the inner function
			return val

		#return of the decorator
		return inner

	def initialize_wpac(self, settings):
		"""
		Initializes the communication with the Wafer Probing Auxiliary Card.

		Parameters
		----------
		settings : dict
			Dictionary containing the WPAC configuration.

		Raises
		------
		RuntimeError
			If the initialization of the WPAC communication fails.
		"""

		self.logger.info("Starting initialization of WPAC communication")

		#initializing a WPAC object
		try:
			self.wpac = wpac.Wpac(**settings)
		except ValueError as e:
			self.logger.exception("Wrong parameter(s) detected while "
				"initializing the communication with the WPAC!")
			raise RuntimeError("Could not initialize the communication with "
				"the WPAC!") from e

		#starting the communication
		try:
			self.wpac.open()
		except (IOError, RuntimeError) as e:
			self.logger.exception("Error detected while initializing the "
				"communication with the WPAC!")
			raise RuntimeError("Could not initialize the communication with "
				"the WPAC!") from e

		self.logger.info("Completed initialization of WPAC communication")

	def initialize_power_board(self):
		"""
		Initializes the Power Board.

		The board is configured to source VIN to the chip but the output is 
		disabled by default.

		Raises
		------
		RuntimeError
			If the initialization fails.
		"""

		self.logger.info("Starting initialization of the Power Board")

		#Configure VIN as the default powering voltage and disable the
		#short between the power supply channels
		try:
			self.wpac.set_power_vin()
			self.wpac.set_vin_short(False)
		except Exception as e:
			raise RuntimeError("Could not configure the Power Board!") from e

		self.logger.info("Completed initialization of the Power Board")

	def initialize_instruments(self):
		"""
		Initialize the instruments needed for chip testing.

		Raises
		------
		RuntimeError
			If the initialization process fails.
		AbortWaferprobing
			If the communication with the instruments fails.
		"""

		self.logger.info("Starting initialization of the instruments")

		#try to initialize the communication with the instruments
		try:
			instr.utils.open_instruments()
		except RuntimeError as e:
			self.logger.error("Error while trying to connect to the instruments")
			raise RuntimeError("Could not initialize the communication to the  "
				"instruments!") from e

		#checking the communication with the instruments
		try:
			instr.utils.check_instruments()
		except RuntimeError as e:
			raise AbortWaferprobing("Could not communicate with "
				"instruments!") from e

		#make sure that the chip is not powered
		self.logger.debug("Disabling chip power for safety")
		instr.utils.disable_chip_power()

		#reset and configure
		self.logger.info("Configuring the instruments")
		try:
			instr.utils.setup_instruments()
		except RuntimeError as e:
			self.logger.error("Error while trying to initialize the instruments")
			raise RuntimeError("Could not initialize the instruments!") from e

		#reset trips on VINA/VIND power supply
		self.logger.debug("Resetting chip trips")
		try:
			instr.utils.reset_chip_trip()
		except Exception:
			self.logger.exception("Failed to reset chip trips!")
			raise

		self.logger.info("Completed initialization of the instruments")

	def _reset_wpac(self, tries = 5):
		"""
		Tries a certain number of times to reset the WPAC.

		Parameters
		----------
		tries : int
			The maximum number of retries to perform in case of a 
			non-responding WPAC.

		Raises
		------
		TypeError
			If the `tries` parameter is not integer.
		ValueError
			If the `tries` parameter is not greater than 0.
		RuntimeError
			If reset fails.
		"""

		#checking that the `tries` parameter is an integer
		if not isinstance(tries, int):
			raise TypeError("The number of tries must be an integer!")

		#checking that the `tries` parameter is non-negative
		if tries <= 0:
			raise ValueError("The number of tries must be non-negative!")

		#resetting the WPAC: the probe card is power-cycled and reinitialized
		is_reset_completed = False
		reset_counter = 0

		while reset_counter < tries:
			try:
				self.wpac.init()
			except Exception:
				self.logger.exception("Could not reset the WPAC on try "
					f"{reset_counter + 1}!")
				is_reset_completed = False
				reset_counter += 1
				continue
			else:
				self.logger.info("WPAC reset completed successfully")
				is_reset_completed = True
				break

		if not is_reset_completed:
			message = "Could not reset the WPAC!"
			self.logger.error(message)
			raise RuntimeError(message)

	def initialize_probe_card(self):
		"""
		Resets the probe card and initializes it.

		The probe card is power-cycled and initialized again by sending a reset 
		signal to the WPAC. After initialization, the status of all probe card 
		registers is logged.

		Raises
		------
		RuntimeError
			If the reset of the WPAC fails.
		"""

		self.logger.info("Starting probe card initialization")

		#resetting the WPAC. The probe card is power-cycled and reinitialized
		self.logger.info("Reinitializing the WPAC")
		try:
			self._reset_wpac()
		except RuntimeError as e: #WPAC reset fails
			self.logger.exception("Could not reset the WPAC!")
			raise RuntimeError("Failed to initialize the WPAC. Aborting "
				"testing!") from e

		#reading all probe card registers
		try:
			config_dict = self.wpac.read_all_registers()
		except Exception: #TODO: make catched exception more specific
			self.logger.exception("Failed to read probe card registers!")
		else:
			#logging the status of the probe card after initialization
			for key, val in config_dict.items():
				self.logger.debug(f"{key}: {val}")

		self.logger.info("Completed probe card initialization")

	def test_edge_sensors(self):
		"""
		Checks that both edge sensors signal contact with the wafer.

		If both edge sensors are in the correct state to signal a wafer 
		contact, the event is logged, otherwise a RuntimeError exception is 
		raised.

		Raises
		------
		TestFailed
			If at least one edge sensor shows that there's no contact, this 
			exception is raised.
		"""

		#read ES status from WPAC
		edge_sensors_status = self.wpac.read_edge_sensors()

		#check if there's contact, otherwise raise an exception
		if all(edge_sensors_status):
			self.logger.info("Contact with wafer established")
		else:
			message = "Bad contact with wafer!"
			self.logger.error(message)
			raise TestFailed(message)

	def initialize_daq(self, use_default_config = True, remove_logs = True,
			remove_dat = False):
		"""
		Initializes the data acquisition system.

		Performs a reset of the FC7 board.

		Parameters
		----------
		use_default_config : bool, optional
			Use the default chip configuration.
		remove_logs : bool, optional
			Removes the DAQ logs during the initialization.
		remove_dat : bool, optional
			Removes the temperature sensor calibration data.

		Raises
		------
		RuntimeError
			If the initialization of the DAQ fails.
		"""

		self.logger.info("Starting initialization of DAQ system")

		#clearing/generating the files needed to run the DAQ
		self.logger.debug("Initializing DAQ configuration")
		try:
			daq.init(use_default_config=use_default_config,
				remove_logs=remove_logs, remove_dat=remove_dat)
		except RuntimeError as e:
			raise RuntimeError("Could not initialize the DAQ!") from e

		#try to reset the FC7 board
		self.logger.info("Resetting the DAQ board")
		try:
			daq.reset()
		except RuntimeError as e:
			self.logger.error(e)
			raise RuntimeError("Could not reset the FC7 board!") from e

		self.logger.info("Completed initialization of DAQ system")

	def initialize_chip(self, mode = LDO_POWERING, vin = 1.7, iin = 1, ovp = 2,
			ocp = 1, vdda = 1.2, vddd = 1.2):
		"""
		Configures the probe card for LDO or shunt mode and powers the chip.

		initialize_chip() and close() are the only methods that update the 
		powering_mode attribute of the class.

		Parameters
		----------
		mode : str, optional
			Powering mode. Can be equal to LDO_POWERING, SHUNT_POWERING or 
			DIRECT_POWERING.
		vin, vdd : float, optional
			Input voltage limit for powering the chip.
		iin : float, optional
			Input current limit for powering the chip.
		ovp : float, optional
			Overvoltage protection.
		ocp : float, optional
			Overcurrent protection.

		Raises
		------
		ValueError
			Exception raised if the `mode` parameter is not one of the 
			following: LDO_POWERING, SHUNT_POWERING, DIRECT_POWERING.
		TestFailed
			If a power supply trip is detected.
		"""

		#checking the requested powering mode
		if mode not in [LDO_POWERING, SHUNT_POWERING, DIRECT_POWERING]:
			message = f"Unrecognised powering mode: {mode}"
			self.logger.error(message)
			raise ValueError(message)

		#reading all probe card registers
		try:
			config_dict = self.wpac.read_all_registers()
		except Exception: #TODO: make catched exception more specific
			self.logger.exception("Failed to read probe card registers!")
		else:
			#logging the status of the probe card after initialization
			for key, val in config_dict.items():
				self.logger.debug(f"{key}: {val}")

		#checking the current powering mode
		if self.powering_mode is not None:
			self.logger.info(f"Current powering mode is: {self.powering_mode}")

		#make sure that the chip is not powered
		instr.utils.disable_chip_power()

		#clearing power supply trips
		try:
			instr.utils.check_chip_trip()
		except instr.utils.TripError as e:
			self.logger.error(e)
			self.logger.warning("Clearing trip condition")
			instr.utils.reset_chip_trip()

		#configuring the power board
		if mode in [LDO_POWERING, SHUNT_POWERING]:
			self.wpac.set_power_vin()
		elif mode == DIRECT_POWERING:
			self.wpac.set_power_vdd()

		#configuring the power supplies and the power board
		self.logger.info(f"Setting powering mode: {mode}")
		if mode == LDO_POWERING:
			instr.utils.set_ldo_mode(vin=vin, iin=iin, ovp=ovp, ocp=ocp)
			self.wpac.disable_shunt_mode()
			self.powering_mode = LDO_POWERING
		elif mode == SHUNT_POWERING:
			instr.utils.set_shunt_mode(vin=vin, iin=iin, ovp=ovp, ocp=ocp)
			self.wpac.enable_shunt_mode()
			self.powering_mode = SHUNT_POWERING
		elif mode == DIRECT_POWERING:
			instr.utils.set_direct_mode(vdda=vdda, vddd=vddd, iin=iin, ovp=ovp,
				ocp=ocp)
			self.wpac.disable_shunt_mode()
			self.powering_mode = DIRECT_POWERING

		#enabling chip power
		self.logger.info("Powering the chip under test")
		instr.utils.enable_chip_power()

		#check for power supply trips
		self.logger.info("Waiting for power supply trips")
		time.sleep(1)
		try:
			instr.utils.check_chip_trip()
		except instr.utils.TripError as e:
			self.logger.error(e)
			raise TestFailed("Power supply trips detected!") from e

		#reading all temperature sensors
		self.logger.info("Reading all WPAC temperature sensors")
		for id_ in self.wpac.temp_sens_ids:
			meas = self.wpac.read_temperature(id_, use_kelvin=True)
			self.logger.info(f"Temperature sensor {id_}: {meas} K")

	def measure_mux_voltage(self, register, subtract_ground = True,
			reset_state = False):
		"""
		Measures a voltage from the output of the probe card multiplexer.

		This method selects the required output by sending the command to the 
		WPAC, waits for the output to stabilize and reads it with the 
		source-meter unit.

		Parameters
		----------
		register : str
			The name of the output of the probe card multiplexer to select.
		subtract_ground : bool, optional
			If True, the relevant ground voltage is subtracted from the 
			measurement.
		reset_state : bool, optional
			If True, the multiplexer output is reset to the default value.

		Returns
		-------
		float or None
			The measured voltage, or None if the measurement could not be 
			performed.
		"""

		#measured value
		meas = None

		#multiplexer configuration
		try:
			self.wpac.select_mux_output(register)
		except Exception:
			self.logger.exception(f"Could not select MUX output: {register}")
		else:
			#getting a voltage reading from the SMU
			meas = instr.utils.get_voltage_measurement()

		#perform ground subtraction if enabled and if the measurement has been
		#performed
		if (subtract_ground is True) and (meas is not None):
			#subtracting the ground from the previous measurement
			gnd_name = self.wpac.get_mux_ground(register)

			#checking that it's not the probe card ground
			if gnd_name != "GND":
				#performing the subtraction
				self.wpac.select_mux_output(gnd_name)
				gnd = instr.utils.get_voltage_measurement()
				meas -= gnd

				#logging the ground subtraction
				self.logger.debug(f"Subtracted value from {register} using "
					f"{gnd_name}: {gnd:.3f} V")

		time.sleep(0.03)

		#resetting the status of the multiplexer if configured to do so
		if reset_state is True:
			self.wpac.select_mux_default()

		return meas

	def measure_bandgap_voltage(self):
		"""
		Measures the bandgap voltage.

		This is a critical measurement, because the measuring instrument can 
		introduce huge transients on chip power.
		"""

		self.logger.info("Starting measurement of bandgap voltage")

		#getting the source-meter unit
		try:
			keithley = instr.utils.DEVICES["METER"]
		except Exception:
			self.logger.exception("Could not get the source-meter object!")
			return None

		#turning off the chip
		self.logger.debug("Turning off chip")
		instr.utils.disable_chip_power()

		#setting the right mux output
		self.logger.debug("Changing the multiplexer output")
		self.wpac.select_mux_output("R_IREF")

		#setting lower source-meter compliance for safety
		self.logger.debug("Lowering source-meter compliance")
		keithley.write("SENS:VOLT:PROT 0.52")

		#turning the Keithley on **before** the chip
		self.logger.debug("Turning on source-meter output")
		keithley.enable_output()

		#turning the chip back on
		self.logger.debug("Turning on chip")
		instr.utils.enable_chip_power()

		#measuring V_BGR
		self.logger.debug("Performing the measurement")
		try:
			meas = float(keithley.read_output())
			self.logger.debug(f"V_BGR + GND_BGR = {meas:2.3f} V")
		except Exception:
			self.logger.debug("Something went wrong while measuring V_BGR!")
			return None

		#turning off the chip
		self.logger.debug("Turning off chip")
		instr.utils.disable_chip_power()

		#turning the Keithley off (gives smaller transients than turning it on)
		self.logger.debug("Turning off source-meter output")
		keithley.disable_output()

		#resetting source-meter compliance
		self.logger.debug("Resetting source-meter compliance")
		keithley.write("SENS:VOLT:PROT 3")

		#resetting the probe card mux
		self.logger.debug("Resetting the default MUX value")
		self.wpac.select_mux_default()

		#turning the chip back on for the final time
		self.logger.debug("Turning on chip")
		instr.utils.enable_chip_power()

		#measuring the ground
		self.logger.debug("Measuring V_BGR ground")
		gnd_bgr_name = self.wpac.get_mux_ground("R_IREF")
		gnd_bgr = self.measure_mux_voltage(gnd_bgr_name)
		self.logger.debug(f"{gnd_bgr_name}: {gnd_bgr:2.3f} V")

		#subtracting the ground
		meas -= gnd_bgr
		self.logger.debug(f"V_BGR = {meas:2.3f} V")

		#check chip trip
		try:
			instr.utils.check_chip_trip()
		except instr.utils.TripError as e:
			self.logger.error("Chip trip while measuring the bandgap voltage!")
			self.logger.error(e)
			raise TestFailed("Power supply trips detected!") from e

		self.logger.info("Completed measurement of bandgap voltage")

		return meas

	def test_power_consumption(self, r_gnd_cut = 0.07):
		"""
		Measures the power consumption of the chip.

		Parameters
		----------
		r_gnd_cut : float or int, optional
			Cut value for the maximum ground resistance. Used to check for bad 
			wafer contact.

		Returns
		-------
		dict
			The dictionary containing the results.

		Raises
		------
		TypeError
			If the `r_gnd_cut` parameter has an invalid type.
		RuntimeError
			Raised when an invalid powering mode is detected or when the chip 
			power at contact is too low.
		"""

		self.logger.info("Measuring power consumption of the chip")

		#checking parameter
		if isinstance(r_gnd_cut, (float, int)) is False:
			raise TypeError("Wrong type for parameter r_gnd_cut: "
				f"{type(r_gnd_cut)}")

		is_in_ldo_mode = (self.powering_mode == LDO_POWERING)
		is_in_shunt_mode = (self.powering_mode == SHUNT_POWERING)

		#check that the powering mode is valid
		if (is_in_ldo_mode is False) and (is_in_shunt_mode is False):
			message = "Invalid powering mode detected!"
			self.logger.error(message)
			raise RuntimeError(message)

		self.logger.info("Chip powering scheme is set to "
			f"{self.powering_mode} mode")

		#get measurement dictionary from instr.utils
		measurements = instr.utils.measure_chip_power_vin()
		results_dict = {
			"is_power_good": None,
			"is_tripping": None
		}
		results_dict.update(measurements)

		#measured values from instrument
		iina = measurements["IINA"]
		iind = measurements["IIND"]
		vina = measurements["VINA"]
		vind = measurements["VIND"]

		#input voltages measured on the probe card
		vina_pc = self.measure_mux_voltage("VINA_PC", subtract_ground=False)
		vind_pc = self.measure_mux_voltage("VIND_PC", subtract_ground=False)

		#input voltages sensed from chip
		vina_sns = self.measure_mux_voltage("VINA_SNS")
		vind_sns = self.measure_mux_voltage("VIND_SNS")

		#core voltages
		vdda = self.measure_mux_voltage("VDDA")
		vddd = self.measure_mux_voltage("VDDD")

		#logging the first part of the results
		self.logger.info(f"Analog core: {iina:.3f} A @ {vina:.3f} V")
		self.logger.info(f"Digital core: {iind:.3f} A @ {vind:.3f} V")
		self.logger.info(f"VINA_SNS: {vina_sns:.3f} V, VIND_SNS: {vind_sns:.3f} V")
		self.logger.info(f"VINA_PC: {vina_pc:.3f} V, VIND_PC: {vind_pc:.3f} V")
		self.logger.info(f"VDDA: {vdda:.3f} V, VDDD: {vddd:.3f} V")
		self.logger.info(f"VINA: {vina:.3f} V, VIND: {vind:.3f} V")

		#storing these first results
		results_dict["VINA_SNS"] = vina_sns
		results_dict["VIND_SNS"] = vind_sns
		results_dict["VDDA"] = vdda
		results_dict["VDDD"] = vddd

		#getting the contact resistance. The measurements do not have the
		#correction for GND shifts because the GND resistance is measured
		#separately
		vina_sns_nognd = self.measure_mux_voltage("VINA_SNS", subtract_ground=False)
		vind_sns_nognd = self.measure_mux_voltage("VIND_SNS", subtract_ground=False)
		gnd_ana = self.measure_mux_voltage("GNDA_REF1")
		gnd_dig = self.measure_mux_voltage("GNDD_REF")
		gnd = gnd_ana #ground to be used for monitoring contact resistance

		#######################
		# Contact resistances #
		#######################

		#calculating and logging analog contact resistance
		try:
			r_ana = (vina_pc - vina_sns_nognd) / iina
		except ZeroDivisionError:
			self.logger.warning("Attempted division by 0 while calculating "
				"analog contact resistance")
			r_ana = None
		else:
			self.logger.info(f"Contact resistance (analog): {r_ana:.3f} ohm")

		#calculating and logging digital contact resistance
		try:
			r_dig = (vind_pc - vind_sns_nognd) / iind
		except ZeroDivisionError:
			self.logger.warning("Attempted division by 0 while calculating "
				"digital contact resistance")
			r_dig = None
		else:
			self.logger.info(f"Contact resistance (digital): {r_dig:.3f} ohm")

		#calculating and logging analogue ground contact resistance
		r_gnd_ana = None
		try:
			r_gnd_ana = gnd_ana / iina
		except ZeroDivisionError:
			self.logger.warning("Attempted division by 0 while calculating "
				"analogue ground contact resistance")
		else:
			self.logger.info(f"Contact resistance (GNDA): {r_gnd_ana:.3f} ohm")

		#calculating and logging digital ground contact resistance
		r_gnd_dig = None
		try:
			r_gnd_dig = gnd_dig / iind
		except ZeroDivisionError:
			self.logger.warning("Attempted division by 0 while calculating "
				"digital ground contact resistance")
		else:
			self.logger.info(f"Contact resistance (GNDD): {r_gnd_dig:.3f} ohm")

		#calculating and logging ground contact resistance
		try:
			r_gnd = gnd / (iina + iind)
		except ZeroDivisionError:
			self.logger.warning("Attempted division by 0 while calculating "
				"ground contact resistance")
			r_gnd = None
		else:
			self.logger.info(f"Contact resistance (GND): {r_gnd:.3f} ohm")

		#saving results on contact resistance
		results_dict["R_ANA"] = r_ana
		results_dict["R_DIG"] = r_dig
		results_dict["R_GND"] = r_gnd
		results_dict["R_GND_ANA"] = r_gnd_ana
		results_dict["R_GND_DIG"] = r_gnd_dig

		#starting to save the results
		self.results[f"power_startup_{self.powering_mode}"] = {}
		self.results[f"power_startup_{self.powering_mode}"] = results_dict
		self.dump_data()

		###################
		# Measuring V_BGR #
		###################

		#this measurement must be performed in a unique way because you don't
		#want to introduce transients at the voltage node that generates the
		#20 uA current
		try:
			v_bgr = self.measure_bandgap_voltage()
		except Exception:
			self.logger.exception("Bandgap voltage measurement failed!")
		else:
			self.logger.info(f"V_BGR = {v_bgr:2.3f} V")
			results_dict["R_IREF"] = v_bgr
			self.results[f"power_startup_{self.powering_mode}"] = results_dict
			self.dump_data()

		###############################
		# Measuring remaining signals #
		###############################

		mux_list = ["VDD_PRE", "VREF_ADC", "VOFS_LP", "VREF_OVP", "GNDA_REF1",
			"GNDA_REF2", "GNDD_REF"]
		for mux in mux_list:
			#try to perform the measurement and skip to next value if it fails
			try:
				meas = self.measure_mux_voltage(mux)
			except Exception:
				self.logger.exception(f"Could not measure {mux}!")
				continue
			#log the measured value
			self.logger.info(f"{mux}: {meas:.3f} V")
			results_dict[mux] = meas
			#check chip trip
			try:
				instr.utils.check_chip_trip()
			except instr.utils.TripError as e:
				self.logger.error(f"Chip trip while measuring {mux}!")
				self.logger.error(e)
				raise TestFailed("Power supply trips detected!") from e

		#saving data before performing the checks
		self.results[f"power_startup_{self.powering_mode}"].update(results_dict)
		self.dump_data()

		#check for power supply trips
		try:
			instr.utils.check_chip_trip()
			results_dict["is_tripping"] = False
		except instr.utils.TripError as e:
			self.logger.error(e)
			results_dict["is_tripping"] = True
			raise TestFailed("Power supply trips detected!") from e
		finally:
			self.results[f"power_startup_{self.powering_mode}"].update(results_dict)
			self.dump_data()

		#checking contact resistance at startup
		if r_gnd is not None:
			is_rgnd_wrong = (r_gnd > r_gnd_cut)
			if is_rgnd_wrong:
				self.logger.error("High contact resistance detected!")
				self.logger.error(f"R_GND above cut: {is_rgnd_wrong}")
				raise HighContactResistance("Contact resistance is too high!")
		else:
			self.logger.warning("Invalid contact resistance measurement, "
				"skipping check")

		#checking for wrong values in LDO mode
		if self.powering_mode == LDO_POWERING:
			#checking power consumption depending on chip type
			if self.chip_type == CROC_CHIP:
				is_power_good = (0.2 < iina < 1) and (0.2 < iind < 0.6) and (0 < gnd < 0.2)
			else:
				self.logger.warning("The tested chip type has no LDO cuts on "
					"power consumption!")

		#checking for wrong values in shunt mode
		if self.powering_mode == SHUNT_POWERING:
			#checking power consumption depending on chip type
			if self.chip_type == CROC_CHIP:
				is_power_good = (1.4 < vina_sns < 1.7) and (1.4 < vind_sns < 1.7) and (0 < gnd < 0.3)
			else:
				self.logger.warning("The tested chip type has no SLDO cuts on "
					"power consumption!")

		#checking that the chip passed the power consumption cuts
		if not is_power_good:
			self.logger.error(f"Measured values in {self.powering_mode} mode "
				"not within tolerance!")
			results_dict["is_power_good"] = False
			raise TestFailed("Wrong power consumption detected!")

		results_dict["is_power_good"] = True
		self.results[f"power_startup_{self.powering_mode}"].update(results_dict)
		self.dump_data()

		self.logger.info("Completed measurement of power consumption")

		return results_dict

	def measure_matrix_current(self):
		"""
		Measures the current drawn by the pixel matrix.

		The measurement is performed by subtraction: first, the current drawn 
		in the default configuration is measured; then, the pixel matrix is 
		configured in order to draw the minimum amount of current and the 
		measurement is performed again. The per-pixel current is the difference 
		between these two values divided by the number of pixels in the matrix.

		Raises
		------
		TestFailed
			If chip power could not be changed (either normal -> low or 
			low -> normal).
		"""

		self.logger.info("Starting measurement of pixel power consumption")

		#getting the number of pixels in the matrix for this chip
		pixel_count = CHIP_DATA[self.chip_type]["pixel_count"]

		#results dictionary
		results = {
			"VDDA": None,
			"VDDD": None,
			"I_pix_ana_ua": None,
			"I_pix_dig_ua": None,
			"I_ana_default": None,
			"I_dig_default": None,
			"I_ana_low": None,
			"I_dig_low": None,
		}

		#making sure that the chip is configured for normal power consumption
		try:
			self._safe_daq_run(daq.set_standard_chip_power)()
		except RuntimeError as e:
			self.logger.error("Failed to set default chip power!")
			raise TestFailed("Could not complete the pixel power "
				"measurement!") from e

		#measuring default chip power
		default_power_meas = instr.utils.measure_chip_power_vin()
		iina_default = default_power_meas["IINA"]
		iind_default = default_power_meas["IIND"]
		results["I_ana_default"] = iina_default
		results["I_dig_default"] = iind_default
		self.logger.info(f"Default power: {iina_default} A (analog); "
			f"{iind_default} A (digital)")

		#also measuring VDDA/D
		vdda = self.measure_mux_voltage("VDDA")
		vddd = self.measure_mux_voltage("VDDD")

		#setting the power consumption of the pixel matrix to a minimum
		try:
			self._safe_daq_run(daq.set_low_chip_power)()
		except RuntimeError as e:
			self.logger.error("Failed to set low chip power!")
			raise TestFailed("Could not complete the pixel power "
				"measurement!") from e

		#measuring chip power after setting the matrix to minimum power
		low_power_meas = instr.utils.measure_chip_power_vin()
		iina_low = low_power_meas["IINA"]
		iind_low = low_power_meas["IIND"]
		results["I_ana_low"] = iina_low
		results["I_dig_low"] = iind_low
		self.logger.info(f"Low power: {iina_low} A (analog); "
			f"{iind_low} A (digital)")

		#obtaining matrix power by subtraction and division by total number of
		#pixels. Also multiplying by 10^6 in order to convert to microampere
		i_pixel_ana_ua = 1e6 * (iina_default - iina_low) / pixel_count
		i_pixel_dig_ua = 1e6 * (iind_default - iind_low) / pixel_count

		#logging the results
		self.logger.info(f"Analog per-pixel current: {i_pixel_ana_ua:.3f} uA")
		self.logger.info(f"Digital per-pixel current: {i_pixel_dig_ua:.3f} uA")

		#saving the results
		results["VDDA"] = vdda
		results["VDDD"] = vddd
		results["I_pix_ana_ua"] = i_pixel_ana_ua
		results["I_pix_dig_ua"] = i_pixel_dig_ua
		self.results["pixel_power"] = dict(results)
		self.dump_data()

		self.logger.info("Restoring normal chip power")
		try:
			self._safe_daq_run(daq.set_standard_chip_power)()
		except RuntimeError as e:
			self.logger.error("Failed to set default chip power!")
			raise TestFailed("Could not complete the pixel power "
				"measurement!") from e

		#checking communication
		try:
			self._safe_daq_run(daq.program)()
		except RuntimeError:
			self.logger.error("Could not program the chip after power "
				"measurement!")

		self.logger.info("Completed measurement of pixel power consumption")

		return results

	def trim_iref_croc(self, target = 0.5, tolerance = 0.05,
			mux = "VOFS", scale = 5, resistance = 24.9e3):
		"""
		Performs the IREF trimming routine on CROC chips.

		The trimming is performed on the multiplexer channel set by the `mux` 
		parameter. The trimming is performed on the voltage and the IREF value 
		is calculated from the voltage as 
		(voltage - gnd) / (scale * resistance).

		Parameters
		----------
		target : float, optional
			The target value for the trimming.
		tolerance : float, optional
			If the trimmed value doesn't satisfy |target - trimmed| < tolerance, 
			the trimming is considered failed.
		mux : str, optional
			The multiplexer channel to use for the trimming.
		scale : int or float, optional
			The scale factor for the current.
		resistance : int or float, optional
			The resistance from the node at which the voltage is measured and 
			its ground.

		Returns
		-------
		dict
			The dictionary containing the results.
		"""

		#logging the details of the test
		self.logger.info("Starting IREF trimming routine")
		self.logger.info(f"Multiplexer output: {mux}")
		self.logger.info(f"Target value: {target}")
		self.logger.info(f"Tolerance: {tolerance}")
		self.logger.info(f"Scale factor: {scale}")
		self.logger.info(f"Resistance: {resistance} Ω")

		#trim values to use during the test
		trim_values = range(16) #0, ..., 15

		#initializing dictionary to store the results
		results_dict = {
			"data": {
				"trim_codes": [],
				"IREF": [],
				mux: [],
			},
			"measurements": {},
			"trim_value": None,
			"method": mux,
			"target": target,
			"tolerance": tolerance,
			"resistance": resistance,
			"scale": scale,
			"success": None
		}

		#variables employed to find the best IREF_TRIM value
		minimum_diff = None #minimum difference wrt correct value
		iref_trim_sel = None #best trim value

		#loop on trim values
		for trim_value in trim_values:
			#writing value to WPAC and measuring the voltage
			self.wpac.write_register("IREF_TRIM", trim_value)
			voltage = self.measure_mux_voltage(mux)
			iref = voltage / (scale * resistance)

			#adding data to results dictionary
			results_dict["data"]["trim_codes"].append(trim_value)
			results_dict["data"]["IREF"].append(iref)
			results_dict["data"][mux].append(voltage)

			#calculate the difference between target and measurement
			voltage_diff = abs(voltage - target)

			#check if this measurement is better than previous ones
			if minimum_diff is None:
				minimum_diff = voltage_diff
				iref_trim_sel = trim_value
			elif voltage_diff < minimum_diff:
				minimum_diff = voltage_diff
				iref_trim_sel = trim_value

			#logging data
			self.logger.info(f"IREF trim value: {trim_value}") #trim value
			self.logger.info(f"{mux}: {voltage:1.4f} V") #mux voltage
			self.logger.info(f"IREF: {iref * 1e6:2.3f} μA") #current value

		#check that the value found is within the tolerance
		best_iref_idx = results_dict["data"]["trim_codes"].index(iref_trim_sel)
		best_value = results_dict["data"][mux][best_iref_idx]
		is_in_tolerance = abs(best_value - target) < tolerance

		#logging the best value found and whether it is within tolerance or not
		self.logger.info(f"Best IREF_TRIM value found: {iref_trim_sel}")
		self.logger.info(f"Trimmed {mux}: {best_value:2.3f} V")
		if not is_in_tolerance:
			self.logger.warning("Trimmed reference current not within "
				"tolerance")

		#set the trim code which gives the trimmed IREF nearest to 4 uA, even if
		#the code is not in the required tolerance. This should be better than
		#setting the default value again, which can be even worse
		self.logger.info("Updating IREF_TRIM with the best trim value found")
		self.wpac.write_register("IREF_TRIM", iref_trim_sel)

		#the chip can be in an unstable state briefly after the end of the test,
		#so a brief sleep can help
		self.logger.info("Waiting for the chip to stabilize")
		time.sleep(1)

		################################

		#final measurements
		self.logger.info("Measuring chip signals after IREF trimming")

		#power supply measurements
		meas_dict = instr.utils.measure_chip_power_vin()
		for signal, value in meas_dict.items():
			self.logger.info(f"{signal}: {value}")
			results_dict["measurements"][signal] = value

		#multiplexer measurements
		for mux_sel in ["VDDA", "VDDD", "VOFS", "VOFS_LP", "VREF_ADC",
				"VDD_PRE", "VINA_SNS", "VIND_SNS", "GNDA_REF1", "GNDA_REF2",
				"GNDD_REF"]:
			value = self.measure_mux_voltage(mux_sel)
			self.logger.info(f"{mux_sel}: {value:.3f} V")
			results_dict["measurements"][mux_sel] = value

		################################

		#updating the results
		results_dict["trim_value"] = iref_trim_sel
		results_dict["success"] = is_in_tolerance
		self.results[f"iref_trim_{self.powering_mode}"] = dict(results_dict)

		#dumping data to file
		self.dump_data()

		self.logger.info("Completed IREF trimming routine")

		return results_dict

	def initialize_chip_registers(self):
		"""
		Configures the chip register using the default configuration.

		The method also checks for power supply events (trips) after the 
		configuration.

		Raises
		------
		TestFailed
			If programming the chip fails or if a power supply event is 
			detected (OVP, OCP, ...).
		"""

		self.logger.info("Starting chip configuration using default register "
			"values")

		#program the chip (default configuration)
		try:
			self._safe_daq_run(daq.program)()
		except RuntimeError as e:
			self.logger.error("Could not program the chip!")
			raise TestFailed("Chip configuration failed!") from e

		#also updating the configuration
		try:
			daq.write_registers(self.chip_config["registers"])
		except RuntimeError:
			message = "Failed to write custom chip register values!"
			self.logger.exception(message)
			raise RuntimeError(message)

		#waiting before checking the power supplies
		self.logger.info("Waiting for power supply trips")
		time.sleep(1)

		#check the power supplies for problems
		try:
			instr.utils.check_chip_trip()
		except instr.utils.TripError as e:
			self.logger.error("A trip condition has been detected while trying "
				"to initialize the chip! Aborting the testing!")
			raise TestFailed("Chip trip detected after chip "
				"configuration!") from e

		self.logger.info("Completed chip configuration")

	def test_communication(self, n_tries = 3, timeout = 10):
		"""
		Performs a communication test with the CMS DAQ.

		Tries `n_tries` times to communicate with the chip.

		Parameters
		----------
		n_tries : int
			Number of tries to perform for establishing the communication.

		Returns
		-------
		dict
			The dictionary containing the results.
		"""

		self.logger.info("Starting chip communication testing routine")

		#defining a function used for performing several tries in a loop
		def test_loop(n_tries, timeout):
			#number of initialization sequences performed while trying to
			#communicate with the chip
			n_seq = 0

			#total number of communication tries performed (sum on all
			#initialization sequences)
			n_tentatives = 0

			#counter for the global number of failed communication tries
			n_failed_tries = 0

			#loop on `n_tries`
			for i in range(n_tries):
				self.logger.info(f"Starting communication try number {i + 1}")
				try:
					n_seq_iter, n_tentatives_iter = daq.communication_test(timeout=timeout)
					self.logger.debug("Number of sequences: %s", n_seq_iter)
					self.logger.debug("Number of tentatives: %s", n_tentatives_iter)
					n_seq += n_seq_iter
					n_tentatives += n_tentatives_iter
					self.logger.info("Communication with chip established")
				except RuntimeError as e:
					self.logger.error(e)
					n_failed_tries += 1
					continue

			return n_failed_tries, n_seq, n_tentatives

		#first loop on `n_tries` for establishing the communication
		n_failed_tries, n_seq, n_tentatives = test_loop(n_tries, timeout=timeout)

		#set whether the chip answers or not
		threshold = 0.1
		if float(n_failed_tries / n_tries) > threshold:
			is_chip_responding = False
		else:
			is_chip_responding = True

		#if the communication works, log the data and update the
		#is_chip_responding attribute
		self.logger.info(f"Number of performed tries: {n_tries}")
		self.logger.info("Number of failed communication tests: "
			f"{n_failed_tries}")
		if is_chip_responding:
			self.logger.info("Number of failed DAQ initialization sequences "
				f": {n_seq}")
			self.logger.info("Number of failed DAQ communication tries "
				f": {n_tentatives}")
		else:
			self.logger.error("Could not communicate with the chip!")

		#saving data to file
		#initializing the results dictionary
		results = {
			"is_chip_responding": is_chip_responding,
			"n_tries": n_tries,
			"n_failed_tries": n_failed_tries,
			"n_failed_daq_sequences": n_seq,
			"n_failed_daq_tries": n_tentatives
		}
		self.results[f"communication_test_{self.powering_mode}"] = results
		self.dump_data()

		self.logger.info("Completed chip communication testing routine")

		return results

	def test_lanes(self, n_tries = 3):
		"""
		Performs a test of the communication lanes with the CMS DAQ.

		Tries `n_tries` times to communicate with the chip for each lane.

		Parameters
		----------
		n_tries : int
			Number of tries to perform for establishing the communication for 
			each lane.

		Returns
		-------
		dict
			The dictionary containing the results.
		"""

		self.logger.info("Starting Aurora lanes test")

		results = {}

		try:
			results = daq.lanes_test(n_tries=n_tries)
		except RuntimeError:
			self.logger.exception("Caught exception during data lanes test!")
			return results

		#saving results
		self.results["lanes_test"] = dict(results)
		self.dump_data()

		#TODO: manage case in which the test fails

		self.logger.info("Completed Aurora lanes test")

		return results

	def test_registers(self, type_ = "global"):
		"""
		Performs a test on the ability to write and read back registers from 
		the chip.

		Parameters
		----------
		type_ : str, optional
			Type of the register test to run. Can be either 'global' or 'pixel'.

		Raises
		------
		AbortTesting
			If the chip cannot be reconfigured after an unexpected DAQ problem.
		"""

		self.logger.info(f"Starting {type_} registers testing routine")

		#TODO: catch this exception
		if type_ not in ["global", "pixel"]:
			raise ValueError(f"Invalid value for parameter type_': {type_}")

		#counter for failed tries
		results = {
			"success": None,
			"n_failed_regs": None,
			"data": None
		}

		#output file
		csv_filename = f"{self.base_file_name}_RegTestWLT_{type_}.csv"
		destination = os.path.join(self.data_dir, csv_filename)

		try:
			#selecting the test function to run
			if type_ == "pixel":
				test_function = daq.pixel_registers_test
			else:
				test_function = daq.global_registers_test
			#wrapping the call to make it safer
			wrapped = self._safe_daq_run(test_function)
			failed_regs = wrapped(output_path=destination)
		except RuntimeError:
			self.logger.exception("Caught exception from registers test!")
			is_test_ok = None
			failed_regs = None

		#checking the results
		if failed_regs is not None:
			if isinstance(failed_regs, dict):
				is_test_ok = len(failed_regs) == 0
			else:
				self.logger.error("Unrecognized data from registers test!")

		#checking the results
		if is_test_ok is True:
			self.logger.info("No register W&R errors detected")
		else:
			#distinguish between a completed run that shows errors and a run
			#that ended abruptly
			if is_test_ok is False:
				self.logger.error("Register W&R errors detected!")
			elif is_test_ok is None:
				self.logger.error("The test failed!")

			#log all errors up to a cutoff value
			if failed_regs is not None:
				err_cntr = 0
				for reg in failed_regs.keys():
					if err_cntr > 10:
						self.logger.warning("Too many errors: limiting printout!")
						break
					self.logger.error(f"Register {reg} failed the test")
					err_cntr += 1

			#resetting the FC7 and reconfiguring the chip in order to avoid
			#faulty chip configurations
			self.logger.info("Resetting the FC7 and reconfiguring the chip")
			try:
				daq.reset()
				self._safe_daq_run(daq.program)()
			except (RuntimeError, DAQTimeout) as e:
				self.logger.exception("Could not reconfigure the chip!")
				raise AbortTesting("Could not reconfigure chip after "
					"failed registers test! Aborting testing!") from e

		#saving data
		results["success"] = is_test_ok
		results["n_failed_regs"] = len(failed_regs) if failed_regs is not None else None
		results["data"] = failed_regs
		self.results[f"{type_}_registers_test_{self.powering_mode}"] = results
		self.dump_data()

		self.logger.info(f"Completed {type_} registers testing routine")

		return failed_regs

	def trim_vdd_croc(self, target = 1.2, tolerance = 0.05, threshold = 1.29):
		"""
		Trims the analog and digital voltages of the RD53B chip under test.

		Parameters
		----------
		target : float, optional
			The target value for VDD trimming.
		tolerance : float, optional
			If the trimmed value doesn't satisfy |target - trimmed| < tolerance, 
			the trimming is considered failed.
		threshold : float, optional
			VDD* voltage threshold. Used to avoid keeping the chip above the
			maximum core voltage (1.32 V).

		Returns
		-------
		dict
			The dictionary containing the results.

		Raises
		------
		TestFailed
			If the best trim codes cannot be written at the end of the test.
		"""

		self.logger.info("Starting VDD trimming routine")

		#dictionary used to store the results
		results_dict = {
			"VDDA": {
				"data": {},
				"trim_value": None,
				"trimmed_vdd": None,
				"success": None
			},
			"VDDD": {
				"data": {},
				"trim_value": None,
				"trimmed_vdd": None,
				"success": None
			},
			"measurements": {}
		}

		#logging the voltage domain for which the loop starts
		self.logger.info("Starting trimming procedure")

		#dictionaries employed to find the best VDD trim value
		minimum_diff = {"VDDA": None, "VDDD": None}    #minimum different w.r.t. target
		trim_sel = {"VDDA": None, "VDDD": None}        #selected VDD trim code
		vdd_sel = {"VDDA": None, "VDDD": None}         #trimmed VDD
		is_in_tolerance = {"VDDA": None, "VDDD": None} #is the trimmed value within tolerance?
		max_trim = {"VDDA": None, "VDDD": None}        #maximum trim code before threshold
		skipped = [] #list of domains that have reached the skip phase

		#loop on trim values
		for trim in range(0, 16):
			#logging information
			self.logger.info(f"Starting measurement for trim value {trim}")

			#writing the VDD trim bits using the WPAC. The register written
			#is VOLTAGE_TRIM, which contains two additional one-bit fields
			#which are set to 0
			vdda_trim = trim if max_trim["VDDA"] is None else max_trim["VDDA"]
			vddd_trim = trim if max_trim["VDDD"] is None else max_trim["VDDD"]
			try:
				self._safe_daq_run(daq.write_vdd_trim_bits)(
					vrefa_trim=vdda_trim,
					vrefd_trim=vddd_trim
				)
			except RuntimeError as e:
				self.logger.warning("Register writing failed!")
				self.logger.debug(e)
				continue #skip to the next trim code

			#also measuring the drawn current and the input voltage
			measurements = instr.utils.measure_chip_power_vin()
			iina = measurements["IINA"]
			iind = measurements["IIND"]
			vina = measurements["VINA"]
			vind = measurements["VIND"]
			self.logger.debug(f"VINA: {vina} V; VIND: {vind} V")
			self.logger.debug(f"IINA: {iina} V; IIND: {iind} V")

			#measuring the grounds
			for ground in ["GNDA_REF1", "GNDA_REF2", "GNDD_REF"]:
				meas = self.measure_mux_voltage(ground)
				self.logger.debug(f"{ground}: {meas:.3f}")

			#looping on voltage domains
			for domain in ["VDDA", "VDDD"]:
				#if a limit has already been found, skip
				if domain in skipped:
					self.logger.warning(f"Skipping {domain} test at {trim}")
					continue

				#measure VDD*
				vdd = self.measure_mux_voltage(domain)

				#calculate the difference between target and measurement
				vdd_diff = abs(vdd - target)

				#check if this measurement is better than previous ones
				if minimum_diff[domain] is None:
					minimum_diff[domain] = vdd_diff
					trim_sel[domain] = trim
					vdd_sel[domain] = vdd
				elif vdd_diff < minimum_diff[domain]:
					minimum_diff[domain] = vdd_diff
					trim_sel[domain] = trim
					vdd_sel[domain] = vdd

				#adding data to results dictionary
				results_dict[domain]["data"][str(trim)] = vdd

				#logging
				self.logger.info(f"{domain}: {vdd:.3f} V")

				#if VDD is greater than a threshold, stop rising
				if vdd >= threshold:
					self.logger.warning(f"{domain} is above "
						f"{threshold} V, stopping")
					max_trim[domain] = trim - 1
					skipped.append(domain)

			#if both domains have to be skipped, end the loop
			if "VDDA" in skipped and "VDDD" in skipped:
				self.logger.warning("Both domains have reached the threshold, "
					"terminating the trim code loop")
				break

		#checking that there has been at least one measurement. If
		#`trim_sel` is None it means that **all** measurements at all trim
		#codes have failed!
		for domain in ["VDDA", "VDDD"]:
			if trim_sel[domain] is None:
				#setting a trim code equal to 8 if no data has been measured for
				#a given voltage domain
				self.logger.error(f"No trimming data for {domain}!")
				self.logger.warning("Setting the default value of 8")
				results_dict[domain]["trim_value"] = 8
				results_dict[domain]["success"] = False

				#nothing more can be done without data, so go to the next
				#voltage domain or end the loop if it is already at the last
				#voltage domain
				continue

			#updating the trim value and the trimmed VDD in the results dictionary
			results_dict[domain]["trim_value"] = trim_sel[domain]
			results_dict[domain]["trimmed_vdd"] = vdd_sel[domain]

			#check that the trimmed value is within the tolerance
			is_in_tolerance[domain] = abs(vdd_sel[domain] - target) < tolerance
			results_dict[domain]["success"] = is_in_tolerance[domain]
			if is_in_tolerance[domain] is False:
				self.logger.warning(f"Trimmed {domain} not within "
					"tolerance")

			#what if the **best** trim value causes the trimmed VDD to be
			#above 1.3 V? The chip should be discarded right away.
			#raise TestFailed()

			#logging the test results
			self.logger.info(f"Best trim value for {domain}: {trim_sel[domain]}")
			if results_dict[domain]["success"] is False:
				self.logger.error(f"Trimming failed for {domain}")

		#updating trim codes
		trim_sel_ana = trim_sel["VDDA"]
		trim_sel_dig = trim_sel["VDDD"]
		self.logger.debug(f"Setting trim codes: {trim_sel_ana} (ana); {trim_sel_dig} (dig)")
		try:
			wrapped = self._safe_daq_run(daq.write_vdd_trim_bits)
			wrapped(vrefa_trim=trim_sel_ana, vrefd_trim=trim_sel_dig)
		except RuntimeError as e:
			raise TestFailed("VDD trimming failed!") from e

		#adding results to results attribute
		self.results[f"vdd_trim_{self.powering_mode}"] = dict(results_dict)

		################################

		#final measurements
		self.logger.info("Measuring chip signals after VDD trimming")

		#power supply measurements
		meas_dict = instr.utils.measure_chip_power_vin()
		for signal, value in meas_dict.items():
			self.logger.info(f"{signal}: {value}")
			results_dict["measurements"][signal] = value

		#multiplexer measurements
		for mux in ["VDDA", "VDDD", "VOFS", "VOFS_LP", "VREF_ADC", "VDD_PRE",
				"VINA_SNS", "VIND_SNS", "GNDA_REF1", "GNDA_REF2", "GNDD_REF"]:
			value = self.measure_mux_voltage(mux)
			self.logger.info(f"{mux}: {value:.3f} V")
			results_dict["measurements"][mux] = value

		#setting MUX back to the default configuration
		self.wpac.select_mux_default()

		################################

		self.logger.info("Completed VDD trimming routine")

		return results_dict

	def set_vdd(self, vdda = 1.2, vddd = 1.2):
		"""
		Sets the core voltages of the chip (VDDA/D) to the selected value.

		Parameters
		----------
		vdda, vddd : float, optional
			The value to select for the core voltages.

		Raises
		------
		RuntimeError
			If no VDD trim data is found.
		AbortTesting
			If the writing of the VDD trim codes fails.
		"""

		self.logger.info(f"Setting VDDA = {vdda} V and VDDD = {vddd} V")

		#checking that the data is there
		if "vdd_trim_ldo" not in self.results.keys():
			raise RuntimeError("No VDD trim data found!")

		#dictionaries to store data
		data = {"VDDA": None, "VDDD": None}
		targets = {"VDDA": vdda, "VDDD": vddd}
		mins = {}
		trims = {}

		#looping on chip domains
		for domain in data.keys():
			#data initialization for the domain
			data[domain] = self.results["vdd_trim_ldo"][domain]["data"]
			mins[domain] = 2
			trims[domain] = 8
			#looping on the results
			for trim, voltage in data[domain].items():
				diff = abs(voltage - targets[domain])
				if diff < mins[domain]:
					trims[domain] = int(trim)
					mins[domain] = diff

		#logging the results
		for domain, trim in trims.items():
			self.logger.info(f"Trim value for {domain}: {trim}")

		#setting the values
		self.logger.info("Writing the trim codes")
		try:
			wrapped = self._safe_daq_run(daq.write_vdd_trim_bits)
			wrapped(vrefa_trim=trims["VDDA"], vrefd_trim=trims["VDDD"])
		except RuntimeError as e:
			self.logger.error("Failed to update VDD trim codes!")
			raise AbortTesting("Could not update VDD trim codes!") from e

		#setting MUX back to the default configuration
		self.wpac.select_mux_default()

		self.logger.info("VDD setting completed")

	def measure_iv_curve(self, currents, currents_temp = None, currents_mux = None,
			use_high_slope = False, rext = 2.2e3, quit_on_trip = True,
			is_shorted = False):
		"""
		Measures the IV curve of the chip in shunt-LDO mode setting with the 
		power supply set in constant current mode.

		Parameters
		----------
		currents : list of int or float
			List of currents at which the measurements must be performed.
		currents_temp, currents_mux : list of int or float, optional
			Values at which measurements with the on-chip temperature sensors 
			or multiplexers must be performed.
		use_high_slope : bool, optional
			Sets the alternative, high slope for the IV curve.
		rext : float or int, optional
			Value of the external resistor used to set the IV slope. 
		quit_on_trip : bool, optional
			Stop the routine if a trip condition is detected on either power 
			domain of the chip (analog, digital).
		is_shorted : bool, optional
			If the power supply channels are shorted, this must be set to True.

		Returns
		-------
		dict
			The dictionary containing the results.

		Raises
		------
		RuntimeError
			Exception raised in case the chip is not in shunt mode.
		ValueError
			Exception raised when a wrong value is provided for one of its 
			parameters.
		"""

		self.logger.info("Starting current scan of shunt-LDO IV curve")

		#check that the chip is in shunt mode
		if self.powering_mode != SHUNT_POWERING:
			message = "The IV curve can be performed only in shunt mode!"
			self.logger.error(message)
			raise RuntimeError(message)

		#TODO: check the `currents` list
		for current in currents:
			if (0 < current < 4.6) is False:
				raise ValueError(f"Invalid current value detected {current}!")

		#TODO: check the `currents_temp` list
		if currents_temp is None:
			currents_temp = []

		#TODO: check the `mux_temp` list
		if currents_mux is None:
			currents_mux = []

		#logging
		self.logger.info(f"Current points: {currents}")
		self.logger.info(f"Enabled use of high slope: {use_high_slope}")
		self.logger.info(f"R_EXT: {rext:.1f}")

		#initialize results dictionary
		results_dict = {
			#test configuration
			"currents": currents,
			"use_high_slope": use_high_slope,
			"rext": rext,
			#measurements
			"IINA": [],
			"IIND": [],
			"VINA": [],
			"VIND": [],
			"VDDA": [],
			"VDDD": [],
			"VDD_PRE": [],
			"VINA_SNS": [],
			"VIND_SNS": [],
			"VINA_PC": [],
			"VIND_PC": [],
			"VOFS": [],
			"VOFS_LP": [],
			"GNDA_REF1": [],
			"GNDA_REF2": [],
			"GNDD_REF": [],
			"REXTA": [],
			"REXTD": [],
			"VREFA": [],
			"VREFD": [],
			"IINA_mux": [],
			"IIND_mux": [],
			"IDDA_mux": [],
			"IDDD_mux": [],
			"k_ana": [],
			"k_dig": []
		}
		if use_high_slope:
			results_name = "shunt_iv_high_slope"
		else:
			results_name = "shunt_iv_low_slope"

		#if the channels are shorted, append '_shorted' to the name of the test
		if is_shorted is True:
			results_name += "_shorted"

		#changing the slope according to the `use_high_slope` boolean. This
		#selects an alternative resistor on the probe card. There is no
		#dependence on the default slope value
		if use_high_slope is True:
			self.wpac.set_high_shunt_slope()
		else:
			self.wpac.set_low_shunt_slope()

		#reading all temperature sensors
		self.logger.info("Reading all temperature sensors before the IV")
		for id_ in self.wpac.temp_sens_ids:
			meas = self.wpac.read_temperature(id_, use_kelvin=True)
			self.logger.info(f"Temperature sensor {id_}: {meas} K")

		#measurements loop
		for current in currents:
			#logging the current and setting it on the power supply
			self.logger.info(f"Setting shunt current: {current:2.3f}")
			instr.utils.set_shunt_current(current)

			#check trip condition
			try:
				instr.utils.check_chip_trip()
			except instr.utils.TripError:
				self.logger.error("A trip condition has been detected!")
				if quit_on_trip:
					#is it better to raise an exception or to perform cleanup
					#here? Probably the first
					self.logger.info("Skipping IV curve measurement")
					instr.utils.disable_chip_power()
					instr.utils.reset_chip_trip()
					self.logger.info("Setting the default slope configuration")
					self.wpac.set_default_shunt_slope()
					self.logger.info("Reinitializing chip in shunt mode")
					shunt_settings = self.config_tests["init_power_shunt"]
					self.initialize_chip(**shunt_settings)
					raise TestFailed("Trip condition detected!")

			#measure input voltages and currents
			meas_dict = instr.utils.measure_chip_power_vin()
			iina = meas_dict["IINA"]
			iind = meas_dict["IIND"]
			vina = meas_dict["VINA"]
			vind = meas_dict["VIND"]

			#measuring multiplexer voltages
			vina_sns = self.measure_mux_voltage("VINA_SNS")
			vind_sns = self.measure_mux_voltage("VIND_SNS")
			vina_pc = self.measure_mux_voltage("VINA_PC")
			vind_pc = self.measure_mux_voltage("VIND_PC")
			vdda = self.measure_mux_voltage("VDDA")
			vddd = self.measure_mux_voltage("VDDD")
			vdd_pre = self.measure_mux_voltage("VDD_PRE")
			vofs = self.measure_mux_voltage("VOFS")
			vofs_lp = self.measure_mux_voltage("VOFS_LP")
			gnda_ref1 = self.measure_mux_voltage("GNDA_REF1")
			gnda_ref2 = self.measure_mux_voltage("GNDA_REF2")
			gndd_ref = self.measure_mux_voltage("GNDD_REF")
			v_rext_a = self.measure_mux_voltage("REXTA")
			v_rext_d = self.measure_mux_voltage("REXTD")
			v_ref_a = self.measure_mux_voltage("VREFA")
			v_ref_d = self.measure_mux_voltage("VREFD")

			#initialization of chip multiplexer measurements
			iina_mux = None
			iind_mux = None
			isha_mux = None
			ishd_mux = None
			idda_mux = None
			iddd_mux = None

			#measuring internal signals
			xmux = None
			if current in currents_mux:
				self.wpac.select_mux_output("VMUX_OUT")
				try:
					wrapped = self._safe_daq_run(daq.probe_monitoring_mux)
					xmux = wrapped(type_="iv")
				except RuntimeError:
					self.logger.error("Could not read chip monitoring mux!")
					self.logger.warning("Continuing the testing")

				#calculating current consumption from the internal multiplexer
				try:
					iina_mux = (xmux["IMUX"]["IINA"] - gnda_ref1 / 5e3) * 21000
				except KeyError:
					pass
				try:
					iind_mux = (xmux["IMUX"]["IIND"] - gnda_ref1 / 5e3) * 21000
				except KeyError:
					pass
				try:
					isha_mux = (xmux["IMUX"]["ISHUNTA"] - gnda_ref1 / 5e3) * 21520
				except KeyError:
					pass
				try:
					ishd_mux = (xmux["IMUX"]["ISHUNTD"] - gnda_ref1 / 5e3) * 21520
				except KeyError:
					pass
				try:
					idda_mux = iina_mux - isha_mux
				except TypeError:
					pass
				try:
					iddd_mux = iind_mux - ishd_mux
				except TypeError:
					pass

			#calculating the *k* factor using VREF and the drawn current
			k_factor_ana = rext * iina / (vina_sns - v_rext_a)
			k_factor_dig = rext * iind / (vind_sns - v_rext_d)

			#logging the measurements
			self.logger.info(f"IINA: {iina:2.3f} A, IIND: {iind:2.3f} A")
			self.logger.info(f"VINA: {vina:2.3f} V, VIND: {vind:2.3f} V")
			self.logger.info(f"VDDA: {vdda:2.3f} V, VDDD: {vddd:2.3f} V")
			self.logger.info(f"VINA_SNS: {vina_sns:2.3f} V, VIND_SNS: {vind_sns:2.3f} V")
			self.logger.info(f"VINA_PC: {vina_pc:2.3f} V, VIND_PC: {vind_pc:2.3f} V")
			self.logger.info(f"VDD_PRE: {vdd_pre:2.3f} V")
			self.logger.info(f"VOFS: {vofs:2.3f} V")
			self.logger.info(f"VOFS_LP: {vofs_lp:2.3f} V")
			self.logger.info(f"GNDA_REF1: {gnda_ref1:2.3f} V")
			self.logger.info(f"GNDA_REF2: {gnda_ref2:2.3f} V")
			self.logger.info(f"GNDD_REF: {gndd_ref:2.3f} V")
			self.logger.info(f"REXTA: {v_rext_a:2.3f} V")
			self.logger.info(f"REXTD: {v_rext_d:2.3f} V")
			self.logger.info(f"VREFA: {v_ref_a:2.3f} V")
			self.logger.info(f"VREFD: {v_ref_d:2.3f} V")
			self.logger.info(f"k factor (ana): {k_factor_ana:3.2f}")
			self.logger.info(f"k factor (dig): {k_factor_dig:3.2f}")
			#multiplexer measurements
			try:
				self.logger.info(
					f"IINA (mux): {iina_mux:2.3f} A, "
					f" IIND (mux): {iind_mux:2.3f} A"
				)
				self.logger.info(
					f"IDDA (mux): {idda_mux:2.3f} A, "
					f"IDDD (mux): {iddd_mux:2.3f} A"
				)
				for mux, measurements in xmux.items():
					for key, val in measurements.items():
						mux_unit = "A" if mux == "IMUX" else "V"
						self.logger.debug(f"int_{key}: {val:.3e} {mux_unit}")
			except TypeError:
				self.logger.debug("Multiplexer measurement(s) not found")

			#add results to dictionary
			results_dict["IINA"].append(iina)
			results_dict["IIND"].append(iind)
			results_dict["VINA"].append(vina)
			results_dict["VIND"].append(vind)
			results_dict["VDDA"].append(vdda)
			results_dict["VDDD"].append(vddd)
			results_dict["VDD_PRE"].append(vdd_pre)
			results_dict["VINA_SNS"].append(vina_sns)
			results_dict["VIND_SNS"].append(vind_sns)
			results_dict["VINA_PC"].append(vina_pc)
			results_dict["VIND_PC"].append(vind_pc)
			results_dict["VOFS"].append(vofs)
			results_dict["VOFS_LP"].append(vofs_lp)
			results_dict["GNDA_REF1"].append(gnda_ref1)
			results_dict["GNDA_REF2"].append(gnda_ref2)
			results_dict["GNDD_REF"].append(gndd_ref)
			results_dict["REXTA"].append(v_rext_a)
			results_dict["REXTD"].append(v_rext_d)
			results_dict["VREFA"].append(v_ref_a)
			results_dict["VREFD"].append(v_ref_d)
			results_dict["k_ana"].append(k_factor_ana)
			results_dict["k_dig"].append(k_factor_dig)
			#multiplexer measurements
			results_dict["IINA_mux"].append(iina_mux)
			results_dict["IIND_mux"].append(iind_mux)
			results_dict["IDDA_mux"].append(idda_mux)
			results_dict["IDDD_mux"].append(iddd_mux)

			#logging the temperature
			if current in currents_temp:
				self.logger.info("Logging temperature")
				temperatures = self.measure_chip_temperatures()
				if temperatures is not None:
					for sensor, val in temperatures.items():
						self.logger.info(f"{sensor}: {val:2.2f} K")

			#saving results
			self.results[results_name] = dict(results_dict)
			self.dump_data()

		#set multiplexer back to default configuration
		self.wpac.select_mux_default()

		#reset default shunt configuration
		self.logger.info("Going back to default power configuration")
		shunt_settings = self.config_tests["init_power_shunt"]
		instr.utils.set_shunt_current(iin=shunt_settings["iin"])

		#setting back the default slope value, no matter what has been
		#configured before. This can be redundant if the default value and
		#the current one are the same, but this is better than leaving the slope
		#configured differently depending on which slope has been selected for
		#the test
		self.logger.info("Setting the default slope configuration")
		self.wpac.set_default_shunt_slope()

		#reading all temperature sensors
		self.logger.info("Reading all temperature sensors after the IV")
		for id_ in self.wpac.temp_sens_ids:
			meas = self.wpac.read_temperature(id_, use_kelvin=True)
			self.logger.info(f"Temperature sensor {id_}: {meas} K")

		self.logger.info("Completed current scan of shunt-LDO IV curve")

		return results_dict

	def measure_ldo_iv(self, voltages = None):
		"""
		Performs an IV curve of the LDO regulator.

		Parameters
		----------
		voltages : list of int or float
			The voltages at which the measurements must be performed.

		Returns
		-------
		dict
			The dictionary containing the results.

		Raises
		------
		ValueError
			If the provided configuration contains invalid points.
		RuntimeError
			If the chip is not powered in LDO mode.
		"""

		#check that the chip is in shunt mode
		if self.powering_mode != LDO_POWERING:
			message = "The LDO IV curve can be performed only in LDO mode!"
			self.logger.error(message)
			raise RuntimeError(message)

		self.logger.info("Starting LDO IV curve")

		#initializing the list of voltages if needed
		if voltages is None:
			voltages = [1.6, 1.5, 1.45, 1.4, 1.35]
		else:
			#if the list of voltages has been passed, check that there are no
			#strange values
			for voltage in voltages:
				if (0.9 < voltage <= 2) is False:
					raise ValueError("Invalid configuration detected for the "
						"LDO IV!")

		#logging the points at which the measurements will be performed
		self.logger.info(f"Voltage points: {voltages}")

		#initialize results dictionary
		results_dict = {
			#test configuration
			"voltages": voltages,
			#measurements
			"IINA": [],
			"IIND": [],
			"VINA": [],
			"VIND": [],
			"VDDA": [],
			"VDDD": [],
			"VDD_PRE": [],
			"VINA_SNS": [],
			"VIND_SNS": [],
			"VOFS": [],
			"GNDA_REF1": [],
			"GNDA_REF2": [],
			"GNDD_REF": [],
		}

		#looping on the voltages
		for voltage in voltages:
			#logging the voltage
			self.logger.info(f"Setting VIN={voltage:1.2f} V")

			#setting the voltage
			instr.utils.set_ldo_voltage(voltage)
			
			#measure input voltages and currents
			meas_dict = instr.utils.measure_chip_power_vin()
			iina = meas_dict["IINA"]
			iind = meas_dict["IIND"]
			vina = meas_dict["VINA"]
			vind = meas_dict["VIND"]

			#measuring multiplexer voltages
			vina_sns = self.measure_mux_voltage("VINA_SNS")
			vind_sns = self.measure_mux_voltage("VIND_SNS")
			vdda = self.measure_mux_voltage("VDDA")
			vddd = self.measure_mux_voltage("VDDD")
			vdd_pre = self.measure_mux_voltage("VDD_PRE")
			vofs = self.measure_mux_voltage("VOFS")
			gnda_ref1 = self.measure_mux_voltage("GNDA_REF1")
			gnda_ref2 = self.measure_mux_voltage("GNDA_REF2")
			gndd_ref = self.measure_mux_voltage("GNDD_REF")

			#logging the measurements
			self.logger.info(f"IINA: {iina:2.3f} A, IIND: {iind:2.3f} A")
			self.logger.info(f"VINA: {vina:2.3f} V, VIND: {vind:2.3f} V")
			self.logger.info(f"VDDA: {vdda:2.3f} V, VDDD: {vddd:2.3f} V")
			self.logger.info(f"VINA_SNS: {vina_sns:2.3f} V, VIND_SNS: {vind_sns:2.3f} V")
			self.logger.info(f"VDD_PRE: {vdd_pre:2.3f} V")
			self.logger.info(f"VOFS: {vofs:2.3f} V")
			self.logger.info(f"GNDA_REF1: {gnda_ref1:2.3f} V")
			self.logger.info(f"GNDA_REF2: {gnda_ref2:2.3f} V")
			self.logger.info(f"GNDD_REF: {gndd_ref:2.3f} V")

			#add results to dictionary
			results_dict["IINA"].append(iina)
			results_dict["IIND"].append(iind)
			results_dict["VINA"].append(vina)
			results_dict["VIND"].append(vind)
			results_dict["VDDA"].append(vdda)
			results_dict["VDDD"].append(vddd)
			results_dict["VDD_PRE"].append(vdd_pre)
			results_dict["VINA_SNS"].append(vina_sns)
			results_dict["VIND_SNS"].append(vind_sns)
			results_dict["VOFS"].append(vofs)
			results_dict["GNDA_REF1"].append(gnda_ref1)
			results_dict["GNDA_REF2"].append(gnda_ref2)
			results_dict["GNDD_REF"].append(gndd_ref)

		#saving results
		self.results["ldo_iv"] = dict(results_dict)
		self.dump_data()

		#set multiplexer back to default configuration
		self.wpac.select_mux_default()

		self.logger.info("Completed LDO IV curve")

		return results_dict

	def analog_scan(self):
		"""
		Performs an analog scan on the chip frontend with the CMS DAQ.

		Returns
		-------
		dict
			The dictionary containing the results.

		Raises
		------
		TestFailed
			If the analog scan could not be completed.
		"""

		self.logger.info("Starting analog scan test")

		#path for the output of the scan
		root_filename = f"{self.base_file_name}_AnalogScan.root"
		destination = os.path.join(self.data_dir, root_filename)

		#running the scan
		try:
			#wrapping the DAQ call to make it safer
			wrapped = self._safe_daq_run(daq.analog_scan)
			results = wrapped(output_path=destination)
		except RuntimeError as e:
			self.logger.error("Analog scan failed due to DAQ error!")
			raise TestFailed("Analog scan failed!") from e

		#getting the results and logging
		try:
			n_enabled = results["n_enabled"]
			occ_enabled = results["occupancy_enabled"]
			occ_disabled = results["occupancy_disabled"]
		except KeyError:
			self.logger.warning("Failed to access analog scan data!")
		else:
			self.logger.info(f"Enabled pixels: {n_enabled}")
			self.logger.info(f"Occupancy for enabled pixels: {occ_enabled}")
			self.logger.info(f"Occupancy for disabled pixels: {occ_disabled}")

		#saving data
		self.results["analog_scan"] = results
		self.dump_data()

		self.logger.info("Completed analog scan test")

		return results

	def digital_scan(self):
		"""
		Performs a digital scan on the chip frontend with the CMS DAQ.

		Returns
		-------
		dict
			The dictionary containing the results.

		Raises
		------
		TestFailed
			If the digital scan could not be completed.
		"""

		self.logger.info("Starting digital scan test")

		#path for the output of the scan
		root_filename = f"{self.base_file_name}_DigitalScan.root"
		destination = os.path.join(self.data_dir, root_filename)

		#running the scan
		try:
			#wrapping the DAQ call to make it safer
			wrapped = self._safe_daq_run(daq.digital_scan)
			results = wrapped(output_path=destination)
		except RuntimeError as e:
			self.logger.error("Digital scan failed due to DAQ error!")
			raise TestFailed("Digital scan failed!") from e

		#getting the results and logging
		try:
			n_enabled = results["n_enabled"]
			occ_enabled = results["occupancy_enabled"]
			occ_disabled = results["occupancy_disabled"]
		except KeyError:
			self.logger.warning("Failed to access digital scan data!")
		else:
			self.logger.info(f"Enabled pixels: {n_enabled}")
			self.logger.info(f"Occupancy for enabled pixels: {occ_enabled}")
			self.logger.info(f"Occupancy for disabled pixels: {occ_disabled}")

		#saving data
		self.results["digital_scan"] = results
		self.dump_data()

		self.logger.info("Completed digital scan test")

		return results

	def threshold_scan(self, vcal_med, vcal_high_range, vcal_high_step,
			type_ = "coarse"):
		"""
		Runs a threshold scan injection on the chip.

		Parameters
		----------
		vcal_med : int
			Value of the `VCAL_MED` injection register.
		vcal_high_range : tuple of int
			Range for the `VCAL_HIGH` injection register.
		vcal_high_step : int
			Step for the `VCAL_HIGH` injection register.
		type_ : str, optional
			Threshold scan type. Can be either 'coarse' or 'fine'.

		Returns
		-------
		dict
			The dictionary containing the results.
		"""

		self.logger.info("Starting threshold scan routine")

		if type_ not in ["coarse", "fine"]:
			raise ValueError(f"Unrecognized threshold scan type: {type_}")

		self.logger.info(f"Scan type: {type_}")
		self.logger.info(f"VCAL_MED: {vcal_med}")
		self.logger.info(f"VCAL_HIGH range: {vcal_high_range}")
		self.logger.info(f"VCAL_HIGH step: {vcal_high_step}")

		#path for the output of the scan
		root_filename = f"{self.base_file_name}_ThresholdScan_{type_}.root"
		destination = os.path.join(self.data_dir, root_filename)

		#running the scan
		try:
			wrapped = self._safe_daq_run(daq.threshold_scan)
			results = wrapped(
				output_path=destination,
				vcal_med=vcal_med,
				vcal_high_range=vcal_high_range,
				vcal_high_step=vcal_high_step
			)
		except RuntimeError as e:
			self.logger.error("Threshold scan failed due to DAQ error!")
			raise TestFailed("Threshold scan failed!") from e

		#logging results
		for key, val in results.items():
			self.logger.debug(f"{key} = {val}")

		#saving data
		self.results[f"threshold_scan_{type_}_{self.powering_mode}"] = results
		self.dump_data()

		self.logger.info("Completed threshold scan routine")

		return results

	def threshold_trim(self, target, n_steps):
		"""
		Performs a trimming of the `TDAC` pixel registers in order to make the 
		threshold more uniform across the pixel matrix.

		Parameters
		----------
		target : int
			`VCAL_HIGH` - `VCAL_MED` target for the threshold trimming 
			procedure.
		n_steps : int
			The number of steps to perform during the threshold equalization 
			routine. This parameter has a big effect on the runtime.

		Raises
		------
		TypeError
			If the `target` parameter is not an integer.
		"""

		self.logger.info("Starting threshold trimming routine")

		if not isinstance(target, int):
			raise TypeError("The 'target' parameter must be an integer!")

		if not isinstance(n_steps, int):
			raise TypeError("The 'n_steps' parameter must be an integer!")

		self.logger.info(f"The threshold target is {target}")
		self.logger.info(f"Number of steps: {n_steps}")

		#path for the output CSV with TDAC data
		csv_filename = f"{self.base_file_name}_TDAC.csv"
		root_filename = f"{self.base_file_name}_ThresholdEqualization.root"
		tdac_dest = os.path.join(self.data_dir, csv_filename)
		root_dest = os.path.join(self.data_dir, root_filename)

		try:
			wrapped = self._safe_daq_run(daq.threshold_equalization)
			wrapped(
				target_threshold=target,
				n_steps=n_steps,
				output_path_tdac=tdac_dest,
				output_path_root=root_dest
			)
		except RuntimeError as e:
			self.logger.error("Threshold equalization failed due to DAQ error!")
			raise TestFailed("Threshold equalization failed!") from e

		self.logger.info("Completed threshold trimming routine")

	def measure_injection_capacitance(self, power_configuration = STANDARD_POWER):
		"""
		Measures the internal injection capacitance of the chip.

		Parameters
		----------
		power_configuration : str, optional
			Used to select in which power configuration the test must be 
			performed. The possible values are package-wide constants.

		Returns
		-------
		dict
			Dictionary with the results.

		Raises
		------
		ValueError
			If an incorrect power configuration is detected.
		TestFailed
			If something goes wrong during the testing.
		"""

		self.logger.info("Starting injection capacitance measurement")
		self.logger.info(f"Power configuration: {power_configuration}")

		#checking the powering configuration
		if power_configuration not in [STANDARD_POWER, LOW_POWER]:
			raise ValueError("Invalid power configuration!")

		#setting low power if requested
		if power_configuration == LOW_POWER:
			self.logger.info("Setting the low power configuration for the chip")
			try:
				self._safe_daq_run(daq.set_low_chip_power)()
			except RuntimeError as e:
				self.logger.error("Failed to set low chip power!")
				raise TestFailed("Could not complete the injection capacitance "
					"measurement!") from e

		#setting the output of the probe card multiplexer
		self.logger.info("Setting up the probe card")
		self.wpac.select_mux_output("VMUX_OUT")

		#paths for the output data
		#TODO: move this to DAQ subpackage
		json_filename = f"{self.base_file_name}_InjCap.json"
		json_dest = os.path.join(self.data_dir, json_filename)

		#running the calibration
		self.logger.info("Running the calibration")
		try:
			wrapped = self._safe_daq_run(daq.injection_capacitance_measurement)
			results = wrapped(output_path_json=json_dest)
		except RuntimeError as e:
			raise TestFailed("Injection capacitance measurement failed!") from e
		finally:
			self.logger.info("Reconfiguring the source-meter unit")
			instr.utils.setup_meter() #reconfigure the source-meter
			self.wpac.select_mux_default() #reconfigure probe card mux
			#TODO: resetting chip if necessary

		#adding data to results
		self.results["inj_capacitance"] = {"power_configuration": power_configuration}
		self.results["inj_capacitance"].update(results)
		self.dump_data()

		#going back to the standard power configuration
		if power_configuration == LOW_POWER:
			self.logger.info("Going back to the standard power configuration "
				"for the chip")
			try:
				self._safe_daq_run(daq.set_standard_chip_power)()
			except RuntimeError as e:
				self.logger.error("Failed to set low chip power!")
				raise TestFailed("Could not complete the injection capacitance "
					"measurement!") from e

		self.logger.info("Completed injection capacitance measurement")

		return results

	def calibrate_adc(self, power_configuration = STANDARD_POWER):
		"""
		Measures the calibration curves of the on-chip ADC.

		Parameters
		----------
		power_configuration : str, optional
			Used to select in which power configuration the test must be 
			performed. The possible values are package-wide constants.

		Returns
		-------
		dict
			Dictionary with the results.

		Raises
		------
		ValueError
			If an incorrect power configuration is detected.
		TestFailed
			If something goes wrong during the testing.
		"""

		self.logger.info("Starting ADC calibration routine")
		self.logger.info(f"Power configuration: {power_configuration}")

		#checking the powering configuration
		if power_configuration not in [STANDARD_POWER, LOW_POWER]:
			raise ValueError("Invalid power configuration!")

		#setting low power if requested
		if power_configuration == LOW_POWER:
			self.logger.info("Setting the low power configuration for the chip")
			try:
				self._safe_daq_run(daq.set_low_chip_power)()
			except RuntimeError as e:
				self.logger.error("Failed to set low chip power!")
				raise TestFailed("Could not complete the ADC calibration!") from e

		#setting the output of the probe card multiplexer
		self.logger.info("Setting up the probe card")
		self.wpac.select_mux_output("VMUX_OUT")

		#paths for the output data
		#TODO: move this to DAQ subpackage
		json_filename = f"{self.base_file_name}_ADCCalib.json"
		root_filename = f"{self.base_file_name}_ADCCalib.root"
		json_dest = os.path.join(self.data_dir, json_filename)
		root_dest = os.path.join(self.data_dir, root_filename)

		#running the calibration
		self.logger.info("Running the calibration")
		try:
			wrapped = self._safe_daq_run(daq.adc_calibration)
			results = wrapped(
				output_path_root=root_dest,
				output_path_json=json_dest
			)
		except RuntimeError as e:
			raise TestFailed("ADC calibration failed!") from e
		finally:
			self.logger.info("Reconfiguring the source-meter unit")
			instr.utils.setup_meter() #reconfigure the source-meter
			self.wpac.select_mux_default() #reconfigure probe card mux
			#TODO: resetting chip if necessary

		#adding data to results
		self.results["adc_calibration"] = {"power_configuration": power_configuration}
		self.results["adc_calibration"].update(results)
		self.dump_data()

		#going back to the standard power configuration
		if power_configuration == LOW_POWER:
			self.logger.info("Going back to the standard power configuration "
				"for the chip")
			try:
				self._safe_daq_run(daq.set_standard_chip_power)()
			except RuntimeError as e:
				self.logger.error("Failed to set low chip power!")
				raise TestFailed("Could not complete the injection capacitance "
					"measurement!") from e

		self.logger.info("Completed ADC calibration routine")

		return results

	def calibrate_dacs(self, targets = None, update_dacs = False):
		"""
		Measures the calibration curves of the on-chip DACs.

		Parameters
		----------
		targets : list of (str, int) tuples or None, optional
			If the `targets` list is provided, the information about the best 
			values for each DAC can be obtained. The list contains tuples with 
			the number of the DAC register and the target value in ampere or 
			volt.
		update_dacs : bool, optional
			If True, the DACs are updated with the best value found.

		Returns
		-------
		dict
			Dictionary with the results.

		Raises
		------
		TestFailed
			If something goes wrong during the testing.
		"""

		#defining a helper function
		def _update_dac_values(results):
			"""Helper function to update the DAC values."""

			self.logger.info("Updating the DACs with the best values")

			#checking that the data is there
			try:
				best_values = results["best_values"]
			except KeyError:
				self.logger.error("Best values not found! Skipping")
				return

			#logging the best values
			for reg_name, reg_val in best_values.items():
				self.logger.debug(f"{reg_name}: {reg_val}")

			#updating the chip configuration
			self.chip_config["registers"] = best_values
			daq.CHIP_CONFIG["Registers"].update(best_values)

			#updating the values
			self.logger.info("Writing the updated registers")
			try:
				self._safe_daq_run(daq.write_registers)(best_values)
			except RuntimeError:
				self.logger.exception("Failed to update the registers!")

			#logging the completion of the update
			self.logger.info("DACs updated to best values")

		self.logger.info("Starting DACs calibration routine")

		#setting the output of the probe card multiplexer
		self.logger.info("Setting up the probe card")
		self.wpac.select_mux_output("VMUX_OUT")

		#paths for the output data
		#TODO: move this to DAQ subpackage
		json_filename = f"{self.base_file_name}_DACCalib.xml"
		root_filename = f"{self.base_file_name}_DACCalib.root"
		json_dest = os.path.join(self.data_dir, json_filename)
		root_dest = os.path.join(self.data_dir, root_filename)

		#running the calibration
		self.logger.info("Running the calibration")
		try:
			wrapped = self._safe_daq_run(daq.dac_calibration)
			results = wrapped(
				output_path_root=root_dest,
				output_path_json=json_dest
			)
		except RuntimeError as e:
			raise TestFailed("DACs calibration failed!") from e
		finally:
			self.logger.info("Reconfiguring the source-meter unit")
			instr.utils.setup_meter() #reconfigure the source-meter
			self.wpac.select_mux_default() #reconfigure probe card mux
			#TODO: resetting chip if necessary

		#adding data to results
		self.results["dac_calibration"] = results
		self.dump_data()

		#updating the DAC values if needed
		if update_dacs is True:
			_update_dac_values(results)

		self.logger.info("Completed DACs calibration routine")

		return results

	def calibrate_ring_osc(self):
		"""
		Measures the calibration curves of the ring oscillators.

		This test causes VDD trim codes to be set to 0!

		Returns
		-------
		dict
			Dictionary with the results.

		Raises
		------
		TestFailed
			If something goes wrong during the testing.
		"""

		#TODO: reset VDDD if it gets too low and the chip stops responding?

		self.logger.info("Starting ring oscillators calibration routine")

		#setting the output of the probe card multiplexer
		self.logger.info("Setting up the probe card")
		self.wpac.select_mux_output("VMUX_OUT")

		#TODO: move this to DAQ subpackage
		json_filename = f"{self.base_file_name}_RingOsc.json"
		root_filename = f"{self.base_file_name}_RingOsc.root"
		json_dest = os.path.join(self.data_dir, json_filename)
		root_dest = os.path.join(self.data_dir, root_filename)

		#running the calibration
		self.logger.info("Running the calibration")
		try:
			wrapped = self._safe_daq_run(daq.ringosc_calibration)
			results = wrapped(
				output_path_root=root_dest,
				output_path_json=json_dest
			)
		except RuntimeError as e:
			raise TestFailed("Ring oscillators calibration failed!") from e
		finally:
			self.logger.info("Reconfiguring the source-meter unit")
			instr.utils.setup_meter() #reconfigure the source-meter
			self.wpac.select_mux_default() #reconfigure probe card mux
			#TODO: resetting VDD trim bits if tests are performed after this one

		#adding data to results
		self.results["ringosc_calibration"] = results
		self.dump_data()

		self.logger.info("Completed ring oscillators calibration routine")

		return results

	def calibrate_temperature(self, temp_sens_id = 3, adc_samples = 10):
		"""
		Runs the temperature calibration routine.

		Parameters
		----------
		temp_sens_id : int, optional
			ID of WPAC temperature sensor to use for the calibration.
		adc_samples : int, optional
			Number of ADC measurements to perform.

		Returns
		-------
		dict
			Dictionary with the results.

		Raises
		------
		TestFailed
			If the temperature calibration fails.
		"""

		self.logger.info("Starting temperature calibration routine")

		#setting the output of the probe card multiplexer
		self.logger.info("Setting up the probe card")
		self.wpac.select_mux_output("VMUX_OUT")

		#TODO: move this to DAQ subpackage
		json_filename = f"{self.base_file_name}_TempCalib.json"
		json_dest = os.path.join(self.data_dir, json_filename)

		#move the chip to low power
		self.logger.info("Setting chip to low power")
		try:
			self._safe_daq_run(daq.set_low_chip_power)()
		except RuntimeError as e:
			self.logger.error("Failed to set low chip power!")
			raise TestFailed("Could not complete the temperature sensors "
				"calibration!") from e

		#waiting for the temperature to stabilize
		time.sleep(3)

		#reading all temperature sensors
		for id_ in self.wpac.temp_sens_ids:
			meas = self.wpac.read_temperature(id_, use_kelvin=True)
			self.logger.info(f"Temperature sensor {id_}: {meas} K")

		#reading the temperature for the calibration
		temp_cal = self.wpac.read_temperature(temp_sens_id, use_kelvin=True)
		self.logger.info("Temperature sensor to be used for the calibration: "
			f"{temp_sens_id}")
		self.logger.info("Temperature to be used for the calibration: "
			f"{temp_cal:2.2f} K")

		#running the calibration
		results = {}
		try:
			wrapped = self._safe_daq_run(daq.temperature_calibration)
			results = wrapped(
				wpac_port=self.wpac.port,
				temp_sens_id=temp_sens_id,
				adc_samples=adc_samples,
				output_path_json=json_dest
			)
		except RuntimeError:
			raise TestFailed("Temperature calibration failed!")
		finally:
			self.logger.info("Reconfiguring the source-meter unit")
			instr.utils.setup_meter() #reconfigure the source-meter
			self.wpac.select_mux_default() #reconfigure probe card mux

		#add calibration temperature to data
		results["temp_calib"] = temp_cal

		#measurements after the calibration
		try:
			temperatures = self.measure_chip_temperatures()
			results["temp_sensors"] = temperatures
			if temperatures is not None:
				for sensor, val in temperatures.items():
					self.logger.info(f"{sensor}: {val:2.2f} K")
		except RuntimeError:
			self.logger.warning("Could not measure chip temperatures!")

		#adding data to results
		self.results["temp_calib"] = results
		self.dump_data()

		#setting the chip back to normal power
		self.logger.info("Setting chip back to normal power")
		try:
			self._safe_daq_run(daq.set_standard_chip_power)()
		except RuntimeError as e:
			self.logger.error("Failed to set default chip power!")
			raise TestFailed("Could not complete the temperature sensors "
				"calibration!") from e

		self.logger.info("Completed temperature calibration routine")

		return results

	def measure_chip_temperatures(self):
		"""
		Measures the on-chip temperature sensors.

		After performing the measurement, the SMU is reconfigured.

		Returns
		-------
		dict
			Dictionary with the temperature measurements in kelvin unit.

		Raises
		------
		RuntimeError
			If the measurements fail.
		"""

		self.logger.info("Starting measurement of chip temperatures")

		#setting the output of the probe card multiplexer
		self.logger.info("Setting up the probe card")
		self.wpac.select_mux_output("VMUX_OUT")

		#performing the measurement
		try:
			wrapped = self._safe_daq_run(daq.read_temperature)
			results = wrapped()
		except RuntimeError:
			self.logger.error("Could not measure chip temperatures!")
			results = None
		finally:
			self.logger.info("Reconfiguring the source-meter unit")
			instr.utils.setup_meter() #reconfigure the source-meter
			self.wpac.select_mux_default() #reconfigure probe card mux

		self.logger.info("Chip temperature measurements completed")

		return results

	def test_data_merging(self):
		"""
		Test the data aggregation capabilities of the chip.

		Raises
		------
		TestFailed
			If something goes wrong during the testing or the test fails.
		"""

		self.logger.info("Starting data merging test")

		results = {}
		try:
			wrapped = self._safe_daq_run(daq.data_merging_test)
			results = wrapped()
		except RuntimeError as e:
			raise TestFailed("Data merging test failed!") from e

		#adding data to results
		self.results["data_merging"] = results
		self.dump_data()

		#checking results
		try:
			success = results["success"]
		except Exception:
			success = None
		if success is None:
			self.logger.warning("Required data not found in data merging "
				"results")
		elif success is False:
			raise TestFailed("Data merging test failed!")
		elif success is True:
			self.logger.info("Data merging test passed")

		#resetting the chip after the test
		try:
			daq.reset()
			self._safe_daq_run(daq.program)()
		except RuntimeError:
			self.logger.error("Could not reconfigure chip after data merging!")

		self.logger.info("Completed data merging test")

		return success

	def test_chip_id(self):
		"""
		Tests that the chip can communicate at all values of CHIP_ID.

		Returns
		-------
		dict
			Dictionary with the results.

		Raises
		------
		TestFailed
			If something goes wrong during the testing or the test fails.
		"""

		self.logger.info("Starting chip ID test")

		results = {}
		try:
			wrapped = self._safe_daq_run(daq.chip_id_test)
			results = wrapped(wpac_port=self.wpac.port)
		except RuntimeError as e:
			raise TestFailed("Chip ID test failed!") from e

		#adding data to results
		self.results["chip_id"] = results
		self.dump_data()

		self.logger.info("Completed chip ID test")

		return results

	def write_efuses(self):
		"""
		Write the chip efuses.

		The value written depends on the chip column/row and on the wafer batch 
		and identifier.

		Returns
		-------
		dict or None
			The results of the efuses-writing procedure or None if the process 
			is aborted due to missing wafer data.

		Raises
		------
		TestFailed
			If the efuses writing procedure fails.
		"""

		self.logger.info("Starting efuses writing procedure")

		#getting the waferprobing ID of the wafer
		wafer_id = get_waferprobing_id(self.batch_id, self.wafer_id)
		if wafer_id is None:
			self.logger.error("No data on waferprobing ID for the wafer under "
				"test, cannot write efuses!")
			return None

		#setting up the value to write to the efuses
		value = (self.chip_row) + (self.chip_col << 4) + (wafer_id << 8)
		self.logger.info(f"Writing value {value:#x} to efuses")

		#running the procedure and getting the results
		results = {}
		try:
			results = daq.write_efuses(wpac_port=self.wpac.port, value=value)
		except DAQException:
			message = "Efuses writing failed due to DAQ error!"
			self.logger.error(message)
			raise TestFailed(message)

		#checking the results of the efuses burning procedure
		try:
			success = results["success"]
		except KeyError:
			self.logger.warning("Could not find 'success' data in efuses "
				"results!")
		else:
			#check if the procedure has been performed successfully or not
			if success is False:
				raise TestFailed("Efuses writing failed!")
			self.logger.info("Efuses writing performed successfully")

		#adding data to results
		self.results["efuses"] = results
		self.dump_data()

		self.logger.info("Completed efuses writing procedure")

		return results

	def scan_chain_test(self):
		"""Runs a scan chain test on the digital chip bottom."""

		self.logger.info("Starting scan chain test")

		#performing some initial measurements
		meas_dict = instr.utils.measure_chip_power_vdd()
		iina = meas_dict["IINA"]
		iind = meas_dict["IIND"]
		vdda_ps = meas_dict["VDDA"]
		vddd_ps = meas_dict["VDDD"]
		vdda_sns = self.measure_mux_voltage("VDDA")
		vddd_sns = self.measure_mux_voltage("VDDD")
		self.logger.info(f"IINA: {iina:.3f} A; IIND: {iind:.3f} A")
		self.logger.info(f"VDDA (power supply): {vdda_ps:.3f} V; VDDD (power supply): {vddd_ps:.3f} V")
		self.logger.info(f"VDDA (chip): {vdda_sns:.3f} V; VDDD (chip): {vddd_sns:.3f} V")

		#setting the FC7 as the controller of the I2C bus
		self.logger.info("Selecting the FC7 as the I2C controller")
		self.wpac.set_i2c_fc7()

		#run the scan chain test and check if it has been successful
		self.logger.info("Running the scan chain test")
		try:
			results = daq.scan_chain_test()
		except RuntimeError as e:
			message = "Scan chain test failed unexpectedly!"
			self.logger.error(message)
			raise TestFailed(message)

		#putting back the WPAC as the I2C controller
		self.logger.info("Putting back the WPAC as the I2C controller")
		self.wpac.set_i2c_wpac()

		#adding data to results
		self.results["scan_chain"] = results
		self.results["scan_chain"]["measurements"] = {
			"VDDA": vdda_ps,
			"VDDD": vddd_ps,
			"VDDA_SNS": vdda_sns,
			"VDDD_SNS": vddd_sns,
			"IINA": iina,
			"IIND": iind,
		}
		self.dump_data()

		#checking the results
		is_successful = results["success"]
		if is_successful is True:
			self.logger.info("Scan chain test successful")
		else:
			message = "Scan chain test failed!"
			n_errors = results["n_errors"]
			self.logger.error(message)
			self.logger.error(f"Number of errors: {n_errors}")
			self.logger.info("Checking VDDA/D")
			for voltage in ["VDDA", "VDDD"]:
				meas = self.measure_mux_voltage(voltage)
				self.logger.info(f"{voltage}: {meas:.2f} V")
			raise TestFailed(message)

		self.logger.info("Completed scan chain test")

	def measure_chip_mux(self):
		"""
		Measures signals from the internal chip multiplexer.

		Raises
		------
		TestFailed
			If the procedure cannot be completed successfully.
		"""

		self.logger.info("Starting measurements from chip multiplexer")

		self.logger.info("Measuring the ground shifts")
		gnda_ref1 = self.measure_mux_voltage("GNDA_REF1")
		gnda_ref2 = self.measure_mux_voltage("GNDA_REF2")
		gndd_ref = self.measure_mux_voltage("GNDD_REF")

		self.logger.info("Setting the probe card multiplexer")
		self.wpac.select_mux_output("VMUX_OUT")

		#running the DAQ tool to perform the measurements
		try:
			wrapped = self._safe_daq_run(daq.probe_monitoring_mux)
			results = wrapped(type_="normal", gnda1=gnda_ref1, gnda2=gnda_ref2,
				gndd=gndd_ref)
		except RuntimeError:
			raise TestFailed("Could not measure signals from monitoring mux!")
		finally:
			self.logger.info("Resetting the probe card multiplexer")
			self.wpac.select_mux_default()

		#adding data to results
		self.results["chip_mux_monitoring"] = results
		self.results["chip_mux_monitoring"]["measurements"] = {
			"GNDA_REF1": gnda_ref1,
			"GNDA_REF2": gnda_ref2,
			"GNDD_REF": gndd_ref,
		}
		self.dump_data()

		self.logger.info("Completed measurements from chip multiplexer")

	def dump_data(self):
		"""Saves the results dictionary to a json file."""

		#saving data to file
		try:
			with open(self.data_file, "w") as file_:
				file_.write(json.dumps(self.results, indent=4))
		except Exception:
			self.logger.warning("Failed to write data to results file")

	def close(self):
		"""
		Safely terminates the execution of the chip testing process.

		First, chip power is turned off. Then, the power supplies are 
		reconfigured for the default LDO powering mode. The AUX voltage is also 
		reset.

		The WPAC is then reset in order to reinitialize the probe card with the 
		default configuration, in order to clear register changes.

		In the end, the connection to the instruments and to the WPAC is closed.
		"""

		#TODO: manage all cases (e.g., no communication with instruments/WPAC)

		#turn off chip power
		self.logger.info("Turning off chip")
		instr.utils.disable_chip_power()

		#get LDO power settings
		ldo_power_conf = self.config_tests["init_power_ldo"]
		vin_ldo = ldo_power_conf["vin"]
		iin_ldo = ldo_power_conf["iin"]
		ocp_ldo = ldo_power_conf["ocp"]
		ovp_ldo = ldo_power_conf["ovp"]

		#get back to LDO mode
		self.logger.info("Resetting the main chip power")
		instr.utils.set_ldo_mode(vin=vin_ldo, iin=iin_ldo, ocp=ocp_ldo,
			ovp=ovp_ldo)
		self.powering_mode = LDO_POWERING

		#reset VDD?

		#reset the WPAC (goes back to LDO by default)
		self.logger.info("Resetting the WPAC")
		self._reset_wpac()

		#close connection to power supplies
		self.logger.info("Closing the connection to the instruments")
		instr.utils.close_instruments()

		#close connection to WPAC
		self.logger.info("Closing the connection to the WPAC")
		self.wpac.close()

		#copy DAQ log to `data_dir` attribute
		log_name = f"{self.base_file_name}_DAQ.log"
		destination = os.path.join(f"{self.data_dir}", log_name)
		try:
			daq.copy_logs(destination)
		except RuntimeError:
			self.logger.warning("Could not copy DAQ log file!")

		#saving the DAQ retries information in the JSON
		self.results["daq_retries"] = self.daq_retries
		self.dump_data()

		#closing and removing log handlers
		for handler in self.logger.handlers:
			handler.close()
		self.logger.handlers.clear()

	#method used to split the main() method in smaller pieces
	def _main_ldo_mode(self):
		"""
		Private method for LDO testing.

		Raises
		------
		AbortTesting
			If the power consumption test fails.
		"""

		if self.enabled_tests["power_ldo"] is False:
			self.logger.warning("Skipping LDO testing")
			return

		#powering up the chip
		ldo_power_conf = self.config_tests["init_power_ldo"]
		try:
			self.initialize_chip(**ldo_power_conf)
		except TestFailed as e:
			self.logger.critical("Chip initialization in LDO mode failed!")
			raise AbortTesting("Chip initialization in LDO mode failed!") from e
		except Exception as e:
			self.logger.exception("Caught an unexpected error during chip "
				"initialization in LDO mode! Aborting testing")
			raise AbortTesting("Chip initialization in LDO mode failed!") from e

		#measure chip power
		if self.enabled_tests["power_ldo"] is True:
			test_ldo_conf = self.config_tests["init_power_meas_ldo"]
			try:
				self.test_power_consumption(**test_ldo_conf)
			except TestFailed as e:
				raise AbortTesting("Chip failed power measurement test, "
					"aborting the testing!") from e

		#iref trimming
		if self.enabled_tests["iref_trimming"] is True:
			iref_trim_conf = self.config_tests["iref_trim"]
			try:
				self.trim_iref_croc(**iref_trim_conf)
			except Exception:
				self.logger.exception("Caught an unexpected exception from "
					"IREF trimming routine!")

		#chip configuration
		try:
			self.initialize_chip_registers()
		except TestFailed as e:
			raise AbortTesting("Chip configuration failed! Aborting "
				"testing!") from e

		#communication test
		if self.enabled_tests["communication"] is True:
			self.test_communication(**self.config_tests["communication"])

		#burning efuses
		if self.enabled_tests["efuses"]:
			try:
				self.write_efuses()
			except TestFailed:
				self.logger.error("Writing of efuses failed!")

		#vdd trimming
		if self.enabled_tests["vdd_trimming"] is True:
			try:
				self.trim_vdd_croc(**self.config_tests["vdd_trim"])
			except TestFailed as e:
				self.logger.error(e)
				self.logger.warning("Continuing the testing")

		#setting a lower VDDD
		try:
			self.set_vdd(vdda=1.2, vddd=1.1)
		except RuntimeError:
			self.logger.warning("Could not set VDDD to a lower value")
		except AbortTesting:
			self.logger.error("Updating of VDD trim codes failed, aborting "
				"testing!")
			raise

		#perform a **global** registers test
		#this test must be performed in LDO due to the high current required
		if self.enabled_tests["global_registers_test"]:
			try:
				self.test_registers(type_="global")
			except TestFailed:
				self.logger.error("Testing of chip registers failed!")

		#putting VDDD back to default
		try:
			self.set_vdd(vdda=1.2, vddd=1.2)
		except RuntimeError:
			self.logger.warning("Could not set VDDD to default value")
		except AbortTesting:
			self.logger.error("Updating of VDD trim codes failed, aborting "
				"testing!")
			raise

		#injection capacitance measurement
		if self.enabled_tests["injection_capacitance"] is True:
			try:
				self.measure_injection_capacitance(power_configuration=LOW_POWER)
			except TestFailed:
				self.logger.exception("Injection capacitance measurement failed! "
					"Continuing the testing...")

		#ADC calibration
		if self.enabled_tests["adc_calibration"] is True:
			try:
				self.calibrate_adc(power_configuration=LOW_POWER)
			except TestFailed:
				self.logger.exception("ADC calibration failed! Continuing the "
					"testing...")

		#DAC calibration
		#this test must be performed in LDO due to the high current required
		if self.enabled_tests["dac_calibration"] is True:
			try:
				self.calibrate_dacs(**self.config_tests["dac_calibration"])
			except TestFailed:
				self.logger.exception("DACs calibration failed! Continuing the "
					"testing...")

		#ring oscillators
		if self.enabled_tests["ringosc_calibration"] is True:
			try:
				self.calibrate_ring_osc()
			except TestFailed:
				self.logger.exception("Ring oscillators calibration failed! "
					"Continuing the testing...")

		#temperature sensors calibration
		if self.enabled_tests["temp_calibration"] is True:
			try:
				self.calibrate_temperature(**self.config_tests["temp_calibration"])
			except TestFailed:
				self.logger.error("Temperature calibration failed!")

		#pixel power measurement
		#this test might affect chip stability
		if self.enabled_tests["pixel_power"]:
			try:
				self.measure_matrix_current()
			except TestFailed:
				self.logger.error("Failed to perform the pixel power "
					"measurement. Going on with the testing...")

		#perform an LDO IV curve
		if self.enabled_tests["ldo_iv"]:
			try:
				self.measure_ldo_iv(**self.config_tests["ldo_iv"])
			except Exception:
				self.logger.info("LDO IV curve failed! Going on with the "
					"testing...")

	#method used to split the main() method in smaller pieces
	def _main_shunt_mode(self):
		"""
		Private method for shunt-LDO testing.

		Raises
		------
		AbortTesting
			If the power consumption test fails.
		"""

		#checking if shunt-LDO testing is enabled
		#TODO: use another key instead of 'power_shunt'
		if self.enabled_tests["power_shunt"] is False:
			self.logger.warning("Skipping shunt-LDO testing")
			return

		#powering up the chip
		shunt_power_conf = self.config_tests["init_power_shunt"]
		try:
			self.initialize_chip(**shunt_power_conf)
		except Exception as e:
			self.logger.exception("Caught an error during chip initialization "
				"in shunt-LDO mode! Aborting testing")
			raise AbortTesting("Chip initialization in shunt-LDO mode "
				"failed!") from e

		#checking that the chip is in shunt-LDO for safety
		if self.powering_mode != SHUNT_POWERING:
			self.logger.error("Chip not in shunt-LDO after initialization!")
			self.logger.warning("Skipping the rest of shunt-LDO testing")
			return

		#measure chip power in shunt-LDO mode
		if self.enabled_tests["power_shunt"] is True:
			test_shunt_conf = self.config_tests["init_power_meas_shunt"]
			try:
				self.test_power_consumption(**test_shunt_conf)
			except TestFailed as e:
				raise AbortTesting("Chip failed power measurement test, "
					"aborting the testing!") from e

		#chip configuration
		try:
			self.initialize_chip_registers()
		except TestFailed as e:
			raise AbortTesting("Chip configuration failed! Aborting "
				"testing!") from e

		#measuring chip temperature
		try:
			temperatures = self.measure_chip_temperatures()
			self.results["shunt_temp_startup"] = temperatures
			self.dump_data()
			if temperatures is not None:
				for sensor, val in temperatures.items():
					self.logger.info(f"{sensor}: {val:2.2f} K")
		except RuntimeError:
			self.logger.warning("Could not measure chip temperatures!")

		#update VDD trim bits
		try:
			self._main_update_vdd_trim()
		except:
			raise

		#measure chip signals from monitoring mux
		if self.enabled_tests["chip_mux_monitoring"] is True:
			try:
				self.measure_chip_mux()
			except TestFailed:
				self.logger.error("Could not measure signals from monitoring mux!")

		#measuring shunt IV curves. It can raise the AbortTesting exception
		#if the chip can't get back to default power after the testing
		try:
			self._main_iv_testing()
		except AbortTesting:
			raise

		#setting a lower VDDD
		try:
			self.set_vdd(vdda=1.2, vddd=1.1)
		except RuntimeError:
			self.logger.warning("Could not set VDDD to a lower value")
		except AbortTesting:
			self.logger.error("Updating of VDD trim codes failed, aborting "
				"testing!")
			raise

		#Aurora lanes test
		if self.enabled_tests["lanes_test"] is True:
			lanes_test_conf = self.config_tests["lanes_test"]
			self.test_lanes(**lanes_test_conf)

		#chip ID test
		if self.enabled_tests["chip_id"] is True:
			try:
				self.test_chip_id()
			except TestFailed:
				self.logger.error("Chip ID test failed!")

		#perform a **pixel** registers test
		#this test must be performed in LDO due to the high current required
		if self.enabled_tests["pixel_registers_test"]:
			try:
				self.test_registers(type_="pixel")
			except TestFailed:
				self.logger.error("Testing of chip registers failed!")

		#data merging test
		if self.enabled_tests["data_merging"] is True:
			try:
				self.test_data_merging()
			except TestFailed as e:
				self.logger.error("Data merging test failed!")
				raise AbortTesting("Aborting chip testing") from e

		#digital scan
		if self.enabled_tests["digital_scan"] is True:
			try:
				self.digital_scan()
			except TestFailed as e:
				self.logger.error("Digital scan failed!")
				raise AbortTesting("Aborting chip testing") from e

		#analog scan
		if self.enabled_tests["analog_scan"] is True:
			try:
				self.analog_scan()
			except TestFailed as e:
				self.logger.error("Analog scan failed!")
				raise AbortTesting("Aborting chip testing") from e

		#threshold scan
		try:
			self._main_threshold_testing()
		except:
			raise

	#method used to split the main() method in smaller pieces
	def _main_direct_mode(self):
		"""
		Private method for testing using direct power.

		Raises
		------
		AbortTesting
			If the scan chain test fails.
		"""

		if self.enabled_tests["power_direct"] is False:
			self.logger.warning("Skipping direct powering testing")
			return

		#initialise the chip in direct powering mode
		direct_power_conf = self.config_tests["init_power_direct"]
		try:
			self.initialize_chip(**direct_power_conf)
		except TestFailed as e:
			self.logger.critical("Chip initialization in direct mode failed!")
			raise AbortTesting("Chip initialization in direct mode failed!") from e
		except Exception as e:
			self.logger.exception("Caught an unexpected error during chip "
				"initialization in direct mode! Aborting testing")
			raise AbortTesting("Chip initialization in direct mode failed!") from e

		#perform the scan chain test if enabled
		if self.enabled_tests["scan_chain"] is True:
			try:
				self.scan_chain_test()
			except TestFailed as e:
				self.logger.error("Scan chain test failed!")
				#raise AbortTesting("Scan chain test failed!") from e

	#method used to split the main() method in smaller pieces
	def _main_threshold_testing(self):
		"""
		Combines all testing related to the frontend threshold.

		First, a coarse threshold scan is performed; then, the threshold 
		equalization procedure is applied and then a fine threshold scan is 
		run.

		Raises
		------
		AbortTesting
			If the threshold scans or the equalization fail.
		"""

		#checking that threshold testing is enabled
		if self.enabled_tests["threshold_scan"] is False:
			self.logger.warning("Skipping threshold testing")
			return

		#coarse threshold scan
		threshold_conf = self.config_tests["threshold_scan"]
		trim_scan_data = {}
		try:
			trim_scan_data = self.threshold_scan(type_="coarse",
				**threshold_conf)
		except TestFailed:
			self.logger.error("Coarse threshold scan failed!")
			raise AbortTesting("Aborting chip testing")

		if self.enabled_tests["threshold_trimming"] is False:
			self.logger.warning("Skipping threshold trimming")
			return

		#threshold trimming. Gets the results directly from
		#`self.results`
		try:
			threshold_target = int(trim_scan_data["mean_threshold"])
		except KeyError:
			self.logger.error("Mean threshold data not found!")
			return

		try:
			n_steps = self.config_tests["threshold_trimming"]["n_steps"]
			self.threshold_trim(threshold_target, n_steps)
		except TestFailed as e:
			self.logger.error("Threshold trimming failed!")
			raise AbortTesting("Aborting chip testing") from e

		#setting the range for the fine scan
		vcal_med = self.config_tests["threshold_scan"]["vcal_med"]
		#fine_range in delta-VCAL
		fine_range = (threshold_target - 150,
			threshold_target + 200)
		fine_step = 25

		#running the fine threshold scan
		try:
			self.threshold_scan(type_="fine", vcal_med=vcal_med,
				vcal_high_range=fine_range, vcal_high_step=fine_step)
		except TestFailed as e:
			self.logger.error("Fine threshold scan failed!")
			raise AbortTesting("Aborting chip testing") from e

	#method used to split the main() method in smaller pieces
	def _main_iv_testing(self):
		"""
		Contains all testing steps related to the measurement of the shunt 
		IV curves.

		Raises
		------
		AbortTesting
			If the chip gets stuck in low power, making the remaining tests 
			useless.
		"""

		#checking powering mode
		if self.powering_mode != SHUNT_POWERING:
			self.logger.warning("Skipping shunt IV testing: chip not in shunt "
				"mode")
			return

		#booleans used to check
		is_low_iv_enabled = self.enabled_tests["shunt_iv_low_slope"]
		is_high_iv_enabled = self.enabled_tests["shunt_iv_high_slope"]
		is_shorted_iv_enabled = self.enabled_tests["shunt_iv_shorted"]
		is_iv_enabled = any(
			[is_low_iv_enabled, is_high_iv_enabled, is_shorted_iv_enabled]
		)

		if is_iv_enabled is False:
			self.logger.warning("Skipping measurement of shunt-LDO IV curves")
			return

		#default shunt IV with low slope
		if is_low_iv_enabled is True:
			low_iv_conf = self.config_tests["shunt_iv_low_slope"]
			try:
				self.measure_iv_curve(**low_iv_conf)
			except TestFailed:
				self.logger.error("IV curve with low slope failed!")

		#alternative shunt IV with high slope
		if is_high_iv_enabled is True:
			high_iv_conf = self.config_tests["shunt_iv_high_slope"]
			try:
				self.measure_iv_curve(**high_iv_conf)
			except TestFailed:
				self.logger.error("IV curve with high slope failed!")

		#IV with the power supplies shorted together
		if is_shorted_iv_enabled is True:
			#getting the configuration
			shorted_iv_conf = self.config_tests["shunt_iv_shorted"]
			
			#turn off the chip and reconfigure the power board
			instr.utils.disable_chip_power()
			self.wpac.set_vin_short(True)

			#turn on the chip and rewrite VDDA/D trim codes
			instr.utils.enable_chip_power()
			self._main_update_vdd_trim()

			#run the IV
			try:
				self.measure_iv_curve(**shorted_iv_conf)
			except TestFailed:
				self.logger.error("IV curve with shorted channels failed!")

			#turn off the chip and reconfigure the power board
			instr.utils.disable_chip_power()
			self.wpac.set_vin_short(False)

			#turn on the chip and rewrite VDDA/D trim codes
			instr.utils.enable_chip_power()
			self._main_update_vdd_trim()

	#method used to split the main() method in smaller pieces
	def _main_update_vdd_trim(self):
		"""
		Writes the VDD trim codes in shunt-LDO mode.

		The trim codes are those that have been found in LDO mode.

		Raises
		------
		AbortTesting
			If the procedure fails.
		"""

		#applying VDD trim values found in LDO mode
		self.logger.info("Writing VDD trim codes found in LDO mode")

		#dictionary to store the results
		results = {
			"VDDA": {"trim_value": None},
			"VDDD": {"trim_value": None},
			"measurements": {}
		}

		#checking that the VDD trim data is there
		try:
			trim_ana = self.results["vdd_trim_ldo"]["VDDA"]["trim_value"]
			trim_dig = self.results["vdd_trim_ldo"]["VDDD"]["trim_value"]
		except KeyError:
			self.logger.warning("No LDO VDD trim data found")
			return

		#trying to write the VDD trim data
		try:
			wrapped = self._safe_daq_run(daq.write_vdd_trim_bits)
			wrapped(vrefa_trim=trim_ana, vrefd_trim=trim_dig)
		except RuntimeError as e:
			self.logger.error("Failed to write VDD trim codes!")
			raise AbortTesting("Could not write VDD trim codes!") from e

		#saving the written trim values
		results["VDDA"]["trim_value"] = trim_ana
		results["VDDD"]["trim_value"] = trim_dig

		#performing power supply measurements
		meas_dict = instr.utils.measure_chip_power_vin()
		for signal, value in meas_dict.items():
			self.logger.info(f"{signal}: {value}")
			results["measurements"][signal] = value

		#performing MUX measurements
		self.logger.info("Performing measurements")
		for mux in ["VDDA", "VDDD", "VOFS", "VOFS_LP", "VREF_ADC", "VDD_PRE",
				"VINA_SNS", "VIND_SNS", "GNDA_REF1", "GNDA_REF2", "GNDD_REF"]:
			#measuring from the probe card multiplexer
			value = self.measure_mux_voltage(mux)
			self.logger.info(f"{mux}: {value:.3f} V")
			results["measurements"][mux] = value

		#resetting the probe card multiplexer
		self.wpac.select_mux_default()

		#storing the results
		self.results[f"vdd_trim_{self.powering_mode}"] = results
		self.dump_data()

		self.logger.info("Completed VDD trimming procedure in shunt-LDO mode")

	#chip testing main
	def main(self):
		"""
		Performs all the configured operations on the chip under test.

		The first step consists in initializing the probe card. Then, the edge 
		sensors are checked: if there is partial or no contact with the wafer, 
		testing is interrupted.

		Once these preliminary tests have been performed with success, the chip 
		itself is initialized. The power consumption at startup is measured.

		The next steps trim various chip parameters, such as the reference 
		current or the VDD voltages.

		Raises
		------
		HighContactResistance
			If the measured contact resistance is higher than a set threshold.
		AbortWaferprobing
			If a critical error happens during the testing (e.g., unresponsive 
			hardware) and the waferprobing process has to be stopped.
		"""

		self.logger.info(f"Starting testing of chip {self.chip_str}")

		time_start = time.time()

		#TODO: make a **loop** on enabled tests to reduce redundant code
		try:
			###########################
			# hardware initialization #
			###########################

			#WPAC initialization
			try:
				self.initialize_wpac(main_config.WPAC_CONF)
			except RuntimeError as e:
				raise AbortWaferprobing("Could not initialize the WPAC, "
					"aborting waferprobing!") from e

			#probe card initialization
			try:
				self.initialize_probe_card()
			except RuntimeError as e:
				raise AbortWaferprobing("Could not initialize the probe card, "
					"aborting waferprobing!") from e

			#instruments initialization
			try:
				self.initialize_instruments()
			except Exception as e:
				self.logger.error("Could not communicate with the instruments!")
				raise AbortWaferprobing("Could not initialize the instruments, "
					"aborting waferprobing!") from e

			#power board initialization
			try:
				self.initialize_power_board()
			except RuntimeError as e:
				raise AbortWaferprobing("Could not initialize the Power Board, "
					"aborting waferprobing!") from e

			#changes to default probe card configuration
			self.wpac.write_register("CHIP_ID", 15)

			#edge sensors
			try:
				self.test_edge_sensors()
			except TestFailed as e:
				self.logger.exception("Edge sensors check failed!")
				raise AbortTesting("Bad contact with wafer, aborting "
					"testing!") from e

			############
			# LDO mode #
			############

			#initialize the DAQ
			try:
				self.initialize_daq(use_default_config=True, remove_logs=True,
					remove_dat=True)
			except RuntimeError as e:
				self.logger.exception("Failed to reinitialize the DAQ!")
				raise AbortWaferprobing("Could not reset the DAQ board, "
					"aborting waferprobing!") from e

			#test in LDO mode
			try:
				self._main_ldo_mode()
			except:
				raise

			########################
			# Direct powering mode #
			########################

			try:
				self._main_direct_mode()
			except:
				raise

			##############
			# SHUNT mode #
			##############

			#initialize the DAQ
			try:
				self.initialize_daq(use_default_config=False, remove_logs=False,
					remove_dat=False)
			except RuntimeError as e:
				self.logger.error("Failed to reinitialize the DAQ!")
				raise AbortWaferprobing("Could not initialize the DAQ board, "
					"aborting waferprobing!") from e

			#test in shunt-LDO mode
			try:
				self._main_shunt_mode()
			except:
				raise

			#reading all temperature sensors in the end
			self.logger.info("Reading all temperature sensors")
			for id_ in self.wpac.temp_sens_ids:
				meas = self.wpac.read_temperature(id_, use_kelvin=True)
				self.logger.info(f"Temperature sensor {id_}: {meas} K")

			#####################
			# END OF SHUNT MODE #
			#####################

		except HighContactResistance:
			self.logger.error("High contact resistance, aborting testing!")
			raise
		except AbortWaferprobing:
			self.logger.error("Waferprobing has been aborted!")
			raise
		except AbortTesting:
			self.logger.error(f"Testing has been aborted for chip {self.chip_str}!")
		except KeyboardInterrupt:
			self.logger.warning("Catched a keyboard interrupt, aborting testing!")
			raise
		except Exception:
			self.logger.exception(f"Error while testing chip {self.chip_str}")

		time_stop = time.time()
		elapsed_time = time_stop - time_start

		self.logger.info(f"Completed testing of chip {self.chip_str}")
		self.logger.info(f"Elapsed time [s]: {elapsed_time:1.3f}")


if __name__ == "__main__":
	#ChipTester object initialization
	try:
		chip_tester = ChipTester(**main_config.CHIPTESTER_CONF)
	except Exception as e:
		sys.exit("An exception has occurred while trying to initialize a"
			f"ChipTester instance. Error: {e}")

	#running the main() method
	try:
		chip_tester.main()
	except Exception as e:
		print(f"Caught exception from main(): {e}")
	except KeyboardInterrupt:
		print("Caught a keyboard interrupt. Aborting run")
	finally:
		#try closing the ChipTester object whether the main method raised an
		#exception or not
		try:
			chip_tester.close()
		except Exception as e:
			sys.exit("An exception has occurred while trying to close a "
				f"ChipTester instance. Error: {e}")
