"""
Module for the configuration of the `daq` subpackage and, in turn, of Ph2_ACF.
"""

import copy

from wlt.duts.chips import CROC_CHIP
from wlt.duts.chips import ITKPIX_CHIP

#dictionary that stores the DAQ configuration (chip registers and configuration
#of testing routines) for different chips
DAQ_CONFIGS = {
	CROC_CHIP: {},
	ITKPIX_CHIP: {}
}

#common structure for chip configuration
_BASE_CHIP_CONFIG = {
	#pixel registers
	"Pixels": {"tdac": "tdac.csv"},
	#global registers
	"Registers": {},
	#special case: core columns
	"CoreColumns": {"disable": [], "disableInjections": []}
}

#common structure for tools configuration
#TODO: remove DACtest configuration from base config and move it to chips config
_BASE_TOOLS_CONFIG = {
	"RegReader": {
		"type": "RD53BRegReader", "args": {}
	},
	"RegTest": {
		"type": "RD53BRegTest",
		"args": {
			"testRegs": True,
			"testPixels": False,
			"debug": False,
			"saveSuccesses": False,
			"ServiceFrameSkip": 50,
			"customVectors": {
				"DAC_COMP_LIN": [170, 256, 85],
				"DAC_PREAMP_M_LIN": [170, 256, 85],
				"VOLTAGE_TRIM": [
					0b0010101010,
					0b0001010101,
					#0b1010001000,
					#0b0110001000,
				]
			}
		}
	},
	"DigitalScan": {
		"type": "RD53BInjectionTool",
		"args": {
			"injectionType": "Digital",
			"triggerDuration": 4,
			"triggerLatency": 133,
			"delayAfterPrime": 100,
			"delayAfterInject": 32,
			"delayAfterTrigger": 600,
			"offset": [0, 0],
			"size": [0, 0],
			"maskGen": [
				{"dim": 0, "size": 0, "parallel": False, "shift": [1]},
				{"dim": 1, "size": 4, "parallel": True, "shift": []},
				{"dim": 1, "size": 2, "parallel": True, "shift": []},
				{"dim": 0, "size": 1, "parallel": True, "shift": [4]},
				{"dim": 1, "size": 0, "parallel": True, "shift": []},
				{"dim": 0, "size": 8, "parallel": True, "shift": []}
			],
		}
	},
	"AnalogScan": {
		"type": "RD53BInjectionTool",
		"args": {
			"injectionType": "Analog",
			"nInjections": 50,
			"triggerDuration": 4,
			"triggerLatency": 133,
			"delayAfterPrime": 100,
			"delayAfterInject": 32,
			"delayAfterTrigger": 600,
			"offset": [0, 0],
			"size": [0, 0],
			"maskGen": [
				{"dim": 0, "size": 0, "parallel": False, "shift": [1]},
				{"dim": 1, "size": 4, "parallel": True, "shift": []},
				{"dim": 1, "size": 2, "parallel": True, "shift": []},
				{"dim": 0, "size": 1, "parallel": True, "shift": [4]},
				{"dim": 1, "size": 0, "parallel": True, "shift": []},
				{"dim": 0, "size": 8, "parallel": True, "shift": []}
			],
		}
	},
	"ThresholdScan": {
		"type": "RD53BThresholdScan",
		"args": {
			"injectionTool": "AnalogScan",
			"vcalMed": 300,
			"vcalRange": [0, 1000],
			"vcalStep": 30
		}
	},
	"ThresholdEqualization": {
		"type": "RD53BThresholdEqualization",
		"args": {
			"thresholdScan": "ThresholdScan",
			"injectionTool": "AnalogScan",
			"initialTDAC": 16,
			"targetThreshold": 0,
			"nSteps": 7
		}
	},
	"RingOsc": {
		"type": "RD53RingOscillatorWLT",
			"args": {
				"vmeter": "MyKeithley",
				"vmeterCH": "Front",
				"powerSupplyId": "MyKeithley",
				"powerSupplyChannelId": "Front",
				"minVDDD": 0,
				"maxVDDD": 15,
				"abortVDDD": 1.29
			}
	},
	"ShortRingOsc": {
		"type": "RD53ShortRingOscillator", "args": {}
	},
	"MuxScan": {
		"type": "RD53MuxScan", "args": {}
	},
	"IVScan": {
		"type": "RD53IVScan", "args": {
			"configFile": "config/iv_it_croc_sldo.xml",
			"type": "steps",
			"powerSupplyName": "TTi_MX",
			"multimeterName": "KeithleyMultimeter",
			"powerSupplyVoltageProtection": 3.0
		}
	},
	"ADCScan": {
		"type": "RD53ADCScan", "args": {}
	},
	"DACScan": {
		"type": "RD53DACScan", "args": {}
	},
	"TempSensor": {
		"type": "RD53TempSensor", "args": {}
	},
	"capMeasure": {
		"type": "RD53BCapMeasure",
		"args": {
			"keithleyId": "MyKeithley",
			"keithleyChannelId": "Front",
			"gndCorrection": True
		}
	},
	"ADCCalib": {
		"type": "RD53BADCCalib",
		"args": {
			"voltmeterId": "MyKeithley",
			"voltmeterChannelId": "Front",
			"powerSupplyId": "MyKeithley",
			"powerSupplyChannelId": "Front",
			"powerSupplySamples": 50,
			"nVoltages": 20,
			"adcSamples": 10,
			"minVoltage": 0.01,
			"maxVoltage": 0.8,
			"abortOnLimit": True
		}
	},
	"DACTest": {
		"type": "RD53BDACTest",
		"args": {
			"voltmeterId": "MyKeithley",
			"voltmeterChannelId": "Front",
			"keithSpeed": 1.0,
			"voltmeterSamples": 1,
			"currentMuxResistance": 5.00e3,
			"n": {
				"FC_BIAS_LIN": 3,
				"Vthreshold_LIN": 3,
				"COMP_LIN": 3,
				"COMP_STAR_LIN": 3,
				"LDAC_LIN": 3,
				"KRUM_CURR_LIN": 3,
				"PA_IN_BIAS_LIN": 3,
				"REF_KRUM_LIN": 3
			},
			"gndsel": 19,
			"debug": False,
			"target": {
				#"Vthreshold_LIN": ,
				"DAC_PREAMP_M_LIN": 28.4e-6,
				"DAC_PREAMP_T_LIN": 28.4e-6,
				"DAC_PREAMP_R_LIN": 28.4e-6,
				"DAC_PREAMP_L_LIN": 28.4e-6,
				"DAC_PREAMP_TR_LIN": 28.4e-6,
				"DAC_PREAMP_TL_LIN": 28.4e-6,
				"DAC_LDAC_LIN": 14.2e-6,
				"DAC_COMP_LIN": 10.3e-6,
				"DAC_COMP_TA_LIN": 10.1e-6,
				"DAC_GDAC_M_LIN": 0.460,
				"DAC_GDAC_L_LIN": 0.460,
				"DAC_GDAC_R_LIN": 0.460,
				"DAC_REF_KRUM_LIN": 0.365
				#"COMP_LIN": 10.3-6,
				#"COMP_STAR_LIN": 10.1-6,
				#"LDAC_LIN": 12.8-6,
				#"PA_IN_BIAS_LIN": 28.4-6,
			}
		}
	},
	"DataMergingTest": {
		"type": "RD53BDataMergingTestWLT",
		"args": {}
	},
	"ChipId": {
		"type": "RD53BChipIdTest",
		"args": {
			"wpacPort": "/dev/null", #must configure this at runtime
			"extraReads": 9
		}
	},
	"Efuse": {
		"type": "RD53BEfuseProgrammer",
		"args": {
			"wpacPort": "/dev/null", #must configure this at runtime
			"word": 0 #safety value to be overwriten at runtime
		}
	},
	"TempCalib": {
		"type": "RD53TempSensorWLT",
		"args": {
			"voltmeterId": "MyKeithley",
			"voltmeterChannelId": "Front",
			"useADC": False,
			"adcSamples": 10,
			"wpacTempSens": 4, #use the WPAC temperature sensor by default
			"wpacPort": "/dev/null",
		}
	},
	"TempMeas": {
		"type": "RD53BTemp",
		"args": {
			"voltmeterId": "MyKeithley",
			"voltmeterChannelId": "Front",
			"enSens": [
				"TEMPSENS_SLDOA",
				"TEMPSENS_SLDOD",
				"TEMPSENS_ACB"
			]
		}
	},
	"MonMuxTest": {
		"type": "RD53BMonMuxTest",
		"args": {
			"voltmeterId": "MyKeithley",
			"voltmeterChannelId": "Front",
			"currentMuxResistance": 5.00e3,
			"GNDA_REF1": 0.0,
			"GNDA_REF2": 0.0,
			"GNDD_REF": 0.0,
			"vList": [0, 3, 31, 32, 33, 34, 35, 36, 37, 38, 39],
			"iList": [0, 1, 2, 3, 4, 5, 6, 7, 8, 28, 29, 30, 31],
		}
	},
	"IVMonMuxTest": {
		"type": "RD53BMonMuxTest",
		"args": {
			"GNDA_REF1": 0.0,
			"GNDA_REF2": 0.0,
			"GNDD_REF": 0.0,
			"voltmeterId": "MyKeithley",
			"voltmeterChannelId": "Front",
			"currentMuxResistance": 5.00e3,
			"vList": [4, 31, 32, 33, 34, 35, 36, 37, 38, 39],
			"iList": [0, 1, 2, 3, 4, 5, 6, 7, 8, 28, 29, 30, 31]
		}
	},
	"ScanChainTest": {
		"type": "RD53BScanChainTestWLT",
		"args": {}
	}
}

#base XML configuration for the DAQ
_BASE_DAQ_CONFIG = b"""<?xml version="1.0" encoding="UTF-8"?>

<HwDescription>
  <BeBoard Id="0" boardType="RD53" eventType="VR">
    <connection id="cmsinnertracker.crate0.slot0" uri="chtcp-2.0://localhost:10203?target=192.168.1.80:50001" address_table="file://${PH2ACF_BASE_DIR}/settings/address_tables/CMSIT_address_table.xml" />
    <!---
        <connection id="cmsinnertracker.crate0.slot0" uri="ipbusudp-2.0://192.168.1.80:50001" address_table="file://${PH2ACF_BASE_DIR}/settings/address_tables/CMSIT_address_table.xml" />
    -->

    <!-- Frontend chip configuration -->
    <OpticalGroup Id="0" FMCId="0">
      <!--
      <lpGBT_Files path="./" />
      <lpGBT Id="0" configfile="CMSIT_LpGBT.txt" ChipAddress="0x70" RxDataRate="1280" RxHSLPolarity="0" TxDataRate="160" TxHSLPolarity="1">
        <Settings
            EPRX60ChnCntr_phase = "6"
            />
      </lpGBT>
      -->
      <Hybrid Id="0" Status="1">
        <RD53_Files path="./" />

        <CROC Id="15" Lane="0" configfile="CROC.toml" RxGroups="6" RxChannels="0" TxGroups="3" TxChannels="0">
          <!-- Overwrite .toml configuration file settings -->
          <Settings
              
              />
        </CROC>
        <Global/>
      </Hybrid>
    </OpticalGroup>

    <!-- Configuration for backend readout board -->
    <Register name="user">
      <Register name="ctrl_regs">

        <Register name="fast_cmd_reg_2">
          <Register name="trigger_source"> 2 </Register>
          <!-- 1=IPBus, 2=Test-FSM, 3=TTC, 4=TLU, 5=External, 6=Hit-Or, 7=User-defined frequency -->
          <Register name="HitOr_enable_l12"> 0 </Register>
          <!-- Enable HitOr port: set trigger_source to proper value then this register, 0b0001 enable HitOr from left-most connector, 0b1000 enable HitOr from right-most connector -->
        </Register>

        <Register name="ext_tlu_reg1">
          <Register name="dio5_ch1_thr"> 128 </Register>
          <Register name="dio5_ch2_thr"> 128 </Register>
        </Register>

        <Register name="ext_tlu_reg2">
          <Register name="dio5_ch3_thr"> 128 </Register>
          <Register name="dio5_ch4_thr"> 128 </Register>
          <Register name="dio5_ch5_thr"> 128 </Register>

          <Register name="ext_clk_en"> 0 </Register>
        </Register>

        <!--
        <Register name="tlu_delay"> 1 </Register>
        Set delay on TLU trigger line
        -->

        <Register name="fast_cmd_reg_3">
          <Register name="triggers_to_accept"> 10 </Register>
        </Register>

        <Register name="gtx_drp">
          <Register name="aurora_speed"> 0 </Register> <!-- Aurora Speed configuration. '0'=1.28Gbps, '1'=640Mbps -->
        </Register>

      </Register>
    </Register>

  </BeBoard>

  <Settings>
    <Setting name="placeholder">            100 </Setting>
  </Settings>

  <!-- === Monitoring parameters ===
       MonitoringSleepTime: sleep for monitoring thread in milliseconds
  -->
  <MonitoringSettings>
    <Monitoring type="RD53" enable="0">
      <MonitoringSleepTime> 1000 </MonitoringSleepTime>
      <MonitoringElements
        VIN_ana_ShuLDO  = "0"
        VOUT_ana_ShuLDO = "1"
        VIN_dig_ShuLDO  = "0"
        VOUT_dig_ShuLDO = "1"
        ADCbandgap      = "1"
        Iref            = "1"
        TEMPSENS_1      = "1"
        TEMPSENS_4      = "1"
      />
    </Monitoring>
  </MonitoringSettings>

</HwDescription>

"""

#############
# CROC chip #
#############

#configuration dictionary for chip registers
DAQ_CONFIGS[CROC_CHIP]["chip"] = dict(_BASE_CHIP_CONFIG)
DAQ_CONFIGS[CROC_CHIP]["chip"]["Registers"].update(
	{
		"DAC_PREAMP_L_LIN": 300,
		"DAC_PREAMP_R_LIN": 300,
		"DAC_PREAMP_TL_LIN": 300,
		"DAC_PREAMP_TR_LIN": 300,
		"DAC_PREAMP_T_LIN": 300,
		"DAC_PREAMP_M_LIN": 300,
		"DAC_FC_LIN": 20,
		"DAC_KRUM_CURR_LIN": 70,
		"DAC_REF_KRUM_LIN": 360,
		"DAC_COMP_LIN": 110,
		"DAC_COMP_TA_LIN": 110,
		"DAC_GDAC_L_LIN": 465,
		"DAC_GDAC_R_LIN": 465,
		"DAC_GDAC_M_LIN": 465,
		"DAC_LDAC_LIN": 140,
		"LEACKAGE_FEEDBACK": 0,
		"AnalogInjectionMode": 0,
		"VCAL_HIGH": 1400,
		"VCAL_MED": 300,
		"PIX_MODE": 10
	}
)

#configuration dictionary for DAQ tools
DAQ_CONFIGS[CROC_CHIP]["tools"] = dict(_BASE_TOOLS_CONFIG)

#DAQ configuration for ITkPix
DAQ_CONFIGS[CROC_CHIP]["daq"] = copy.deepcopy(_BASE_DAQ_CONFIG)

###############
# ITkPix chip #
###############

#configuration dictionary for chip registers. To be filled with relevant data
#for ITkPix
DAQ_CONFIGS[ITKPIX_CHIP]["chip"] = dict(_BASE_CHIP_CONFIG)

#configuration dictionary for DAQ tools for ITkPix
DAQ_CONFIGS[ITKPIX_CHIP]["tools"] = dict(_BASE_TOOLS_CONFIG)

#DAQ configuration for ITkPix
DAQ_CONFIGS[ITKPIX_CHIP]["daq"] = copy.deepcopy(_BASE_DAQ_CONFIG)
