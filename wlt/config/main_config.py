"""
Main configuration module for the wlt package.

It defines the settings for the WPAC, ChipTester, WaferTester, the instruments 
and more.
"""

import os
import logging

from wlt.duts.chips import ITKPIX_CHIP
from wlt.duts.chips import CROC_CHIP

from wlt.hardware.probe_cards import CROC_PROBE_CARD_V1P0
from wlt.hardware.probe_cards import CROC_PROBE_CARD_V1P1

from wlt import LDO_POWERING
from wlt import SHUNT_POWERING
from wlt import DIRECT_POWERING

###############
# directories #
###############

#directory settings taken from the environment variables exported by the
#setup.sh Bash script

#root folder of the package
WLT_DIR = os.getenv("WLT_DIR")
#directory in which the DAQ software can be found
ACF_DIR = os.getenv("ACF_DIR")
#directory in which the DAQ executables are run
DAQ_DIR = os.getenv("DAQ_DIR")
#root folder in which the results will be saved
DATA_DIR = os.getenv("DATA_DIR")

#########
# chips #
#########

#global configuration for the type of the chip under test. Using the chip
#constants is better than typing the value manually
CHIP_TYPE = CROC_CHIP

##############
# probe card #
##############

PROBE_CARD = CROC_PROBE_CARD_V1P1

########
# WPAC #
########

#settings used by ChipTester and by the wpac subpackage
WPAC_CONF = {
	#device file for the serial communication with the WPAC
	"port": "/dev/wpac-serial",
	#baud rate for the communication
	"baud_rate": 230400,
	#termination character for the communication
	"termination": '\n',
	#communication timeout: if no answer is read within that interval, the
	#communication is stopped
	"timeout": 1,
	#type of the chip under test
	"probe_card": PROBE_CARD,
	#list of available temperature sensors
	"temp_sens_ids": [3, 4]
}


##############
# ChipTester #
##############

#list of testing routines to be performed by ChipTester
TESTS_LIST = {
	#perform various measurements related to the power drawn by the chip
	"power_ldo": True,
	#perform the IREF trimming routine
	"iref_trimming": False,
	#check whether communicating with the chip is possible
	"communication": False,
	#test all communication lanes which can be used by the chip
	"lanes_test": False,
	#test that all CHIP_ID values work
	"chip_id": False,
	#efuses programming
	"efuses": False,
	#perform the trimming of the analog and digital VDDs of the chip
	"vdd_trimming": False,
	#perform a measurement of the power drawn by the chips
	"pixel_power": False,
	#perform a R&W test on global chip registers
	"global_registers_test": False,
	#perform a R&W test on pixel chip registers
	"pixel_registers_test": False,
	#perform a data merging test to check data aggregation
	"data_merging": False,
	#perform a digital scan using the DAQ
	"digital_scan": False,
	#perform an analog scan with the DAQ
	"analog_scan": False,
	#perform a threshold scan with the DAQ
	"threshold_scan": False,
	#perform a threshold trimming procedure
	"threshold_trimming": False,
	#measure the injection capacitance
	"injection_capacitance": True,
	#perform the ADC calibration procedure
	"adc_calibration": False,
	#perform the DAC calibration procedure
	"dac_calibration": False,
	#perform the ring oscillator calibration procedure
	"ringosc_calibration": False,
	#perform a calibration of the on-chip temperature sensors
	"temp_calibration": False,
	#power the chip in shunt mode and measure the power consumption
	"power_shunt": True,
	#perform an IV curve in the shunt powering mode using the low shunt slope
	"shunt_iv_low_slope": True,
	#perform an IV curve in the shunt powering mode using the high shunt slope
	"shunt_iv_high_slope": False,
	#perform an IV curve with shorted VINA and VIND
	"shunt_iv_shorted": False,
	#perform an IV curve in LDO powering mode
	"ldo_iv": False,
	#turn the chip on in direct power
	"power_direct": False,
	#test the digital chip bottom using the scan chain test
	"scan_chain": False
	#measure signals from the monitoring mux of the chip
	"chip_mux_monitoring": False
}

#configuration for the testing routines
TESTS_CONF = {
	#configuration for powering up the chip in LDO mode
	"init_power_ldo": {
		"mode": LDO_POWERING,
		"vin": 1.6,
		"iin": 2,
		"ovp": 2.1,
		"ocp": 2.1,
	},
	#configuration for the measurements performed at chip startup in LDO
	"init_power_meas_ldo":{
		#cut on contact resistance
		"r_gnd_cut": 0.020,
	},
	#configuration for powering up the chip in shunt LDO mode
	"init_power_shunt": {
		"mode": SHUNT_POWERING,
		"vin": 4,
		"iin": 1,
		"ovp": 4.1,
		"ocp": 4.1,
	},
	#configuration for the measurements performed at chip startup in shunt LDO
	"init_power_meas_shunt": {
		#cut on contact resistance
		"r_gnd_cut": 3
	},
	#configuration for powering up the chip in direct powering mode
	"init_power_direct": {
		"mode": DIRECT_POWERING,
		"vdda": 1.35,
		"vddd": 1.35,
		"iin": 4,
		"ovp": 1.6,
		"ocp": 4.1,
	},
	#configuration for IREF trimming
	"iref_trim": {
		#target value of the 4 μA reference current
		"target": 0.5,
		#maximum allowed deviation from the target value
		"tolerance": 0.05,
		#which mux output to use for the trimming
		"mux": "VOFS",
		#value of the resistor
		"resistance": 24.9e3,
		#scale factor
		"scale": 5
	},
	#configuration for the communication routine
	"communication": {
		"n_tries": 3,
		"timeout": 10
	},
	#configuration for the VDD trimming routine
	"vdd_trim": {
		#target value of the trimming for VDD (in volt)
		"target": 1.2,
		#maximum allowed different between the trimmed value and `target`. If
		#this difference is greater than `tolerance`, the trimming is considered
		#as failed
		"tolerance": 0.05,
		#if the measured VDD is above `threshold`, the loop is interrupted.
		#Given that one trim code corresponds to approximately 0,02 V, the
		#threshold should not be set to 1,3 V or above
		"threshold": 1.29
	},
	#configuration for testing the 4 chip lanes
	"lanes_test": {
		#number of times to try to communicate with the chip for each lane
		"n_tries": 3
	},
	#configuration for testing CHIP_ID
	"chip_id": {
		"n_extra_reads": 9
	},
	#configuration for the IV curve in shunt-LDO powering using the default
	#(low) shunt slope
	"shunt_iv_low_slope": {
		#currents at which the measurements must be performed
		"currents": [0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1],
		#currents at which temperature measurements must be performed
		"currents_temp": [],
		#currents at which monitoring multiplexer measurements must be performed
		"currents_mux": [],
		#sets a high shunt slope for the IV curve 
		"use_high_slope": False,
		#value of the resistor which gives the shunt-LDO slope
		"rext": 597.35,
		#if set to True, stop the IV curve if a trip condition is detected
		"quit_on_trip": True,
		#run with shorted VINA and VIND?
		"is_shorted": False
	},
	#configuration for the IV curve in shunt-LDO powering using the high slope
	"shunt_iv_high_slope": {
		#currents at which the measurements must be performed
		"currents": [0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7],
		#currents at which temperature measurements must be performed
		"currents_temp": [],
		#sets a high shunt slope for the IV curve 
		"use_high_slope": True,
		#value of the resistor which gives the shunt-LDO slope
		"rext": 820.00,
		#if set to True, stop the IV curve if a trip condition is detected
		"quit_on_trip": True,
		#run with shorted VINA and VIND?
		"is_shorted": False
	},
	#configuration for the IV curve in SLDO powering and shorting VINA and VIND
	#together
	"shunt_iv_shorted": {
		#currents at which the measurements must be performed
		"currents": [2.0, 2.25, 2.5, 2.75, 3, 3.25, 3.5, 3.75, 4],
		#currents at which temperature measurements must be performed
		"currents_temp": [4],
		#currents at which monitoring multiplexer measurements must be performed
		"currents_mux": [],
		#sets a high shunt slope for the IV curve
		"use_high_slope": False,
		#value of the resistor which gives the shunt-LDO slope
		"rext": 597.35,
		#if set to True, stop the IV curve if a trip condition is detected
		"quit_on_trip": True,
		#run with shorted VINA and VIND?
		"is_shorted": True
	},
	#configuration for the IV curve in LDO powering
	"ldo_iv": {
		"voltages": [1.6, 1.5, 1.45, 1.4, 1.35]
	},
	#configuration for the default (coarse) threshold scan
	"threshold_scan": {
		"vcal_med": 300,
		"vcal_high_range": (100, 1100),
		"vcal_high_step": 30
	},
	#threshold equalization configuration
	"threshold_trimming": {
		#number of steps to perform: this parameter has a big effect on the runtime
		"n_steps": 7
	},
	#configuration for the calibration of the DACs
	"dac_calibration": {
		#"debug": False,
		#"gndsel": 19,
		"targets": {},
		"update_dacs": True
	},
	#configuration for the calibration of the on-chip temperature sensors
	"temp_calibration": {
		"temp_sens_id": 3,
		"adc_samples": 10
	}
	#TODO: add configuration for remaining tests
}

#settings used for ChipTester. The settings marked with * are only used when
#running the chiptester module directly and not through WaferTester
#TODO: move this configuration to command-line parameters
CHIPTESTER_CONF = {
	#chip column: goes from 0x0 to 0xF
	"chip_col": 0x0000,
	#chip row: goes from 0x0 to 0xF
	"chip_row": 0x0000,
	#wafer batch identifer obtained from the foundry
	"batch_id": "UNDEF",
	#wafer identifer obtained from the foundry
	"wafer_id": "UNDEF",
	#directory in which the results will be saved (*)
	"data_dir": os.path.join(DATA_DIR, "chips"),
	#verbosity of the logging information
	"log_level": logging.DEBUG,
	#type of the chip under test
	"chip_type": CHIP_TYPE,
	#list of tests to be performed by ChipTester
	"enabled_tests": TESTS_LIST,
	#configuration dictionary for the various tests
	"config_tests": TESTS_CONF
}

###############
# WaferTester #
###############

#waferprobing metadata, such as the operator, the hardware used and so on
METADATA = {
	"probe_card_id": "1",
	"probe_card_version": "1.1",
	"operator": None,
	"enabled_tests": TESTS_LIST,
	"overtravel": None,
	#additional information for the run
	"notes": ""
}

#settings used when running the main of the wafertester module
#TODO: move this configuration to command-line parameters or separate config
WAFERTESTER_CONF = {
	#batch identifier from foundry
	"batch_id": "UNDEF",
	#wafer identifier from foundry
	"wafer_id": "UNDEF",
	#wafer type
	"chip_type": CHIP_TYPE,
	#number of chips on which WaferTester will travel to. This does not
	#necessarily mean that they will be tested: this depends on the test_chips
	#attribute of the WaferTester class
	"n_chips_test": 1, #132 for RD53B-ATLAS, 138 for CROC
	#column index of the first die to test
	"start_column": 2,
	#row index of the first die to test
	"start_row": 9,
	#GPIB address used for communicating with the probe station
	"gpib_address": 22,
	#time to wait for the completion of wafer chuck movements
	"mov_wait_time": 5,
	#verbosity of the logging information
	"log_level": logging.DEBUG,
	#this WaferTester parameter defines whether the chips will be tested or not.
	#If it is set to True, the chips will be tested, otherwise WaferTester will
	#perform the so-called wafer travel
	"test_chips": False,
	#parameter which sets whether the probe station will go to separation height
	#or not after completing the testing of all the chips
	"separate_after_test": False,
	#waferprobing metadata to be saved in the WLT JSON
	"metadata": METADATA,
	#contact the wafer two times before starting the testing. This can reduce
	#the contact resistance
	"double_contact": False
}

##################
# power supplies #
##################

#TODO: use this configuration dictionary instead of specifying the startup power
#in the testing
#configuration dictionary for chip powering in shunt and LDO modes
POWER_CONF = {
	LDO_POWERING: {
		"vin": 1.7,
		"iin": 3,
		"ovp": 2.1,
		"ocp": 2.9,
		"vdd_pll": 1.3,
		"vdd_cml": 1.3
	},
	SHUNT_POWERING: {
		"vin": 2.1,
		"iin": 1,
		"ovp": 2.2,
		"ocp": 3,
		"vaux": 1.2
	}
}

#configuration for the different instruments
DEVICES = {
	#probe card power supply
	"PC": {
		"device_file": "/dev/tti-pc",
		"baud_rate": 9600,
		"device_name": "PC",
		"device_vendor": "TTi",
		"device_model": "QL355TP",
		"read_termination": "\r\n",
		"write_termination": "\n",
		"timeout": 1,
		"xonxoff": 1
	},
	#power supply for direct powering
	"VDD": {
		"device_file": "/dev/tti-aux",
		"baud_rate": 9600,
		"device_name": "VDD",
		"device_vendor": "TTi",
		"device_model": "QL355TP",
		"read_termination": "\r\n",
		"write_termination": "\n",
		"timeout": 1,
		"xonxoff": 1
	},
	#source-meter unit
	"METER": {
		"device_file": "/dev/keithley",
		"baud_rate": 19200,
		"device_name": "METER",
		"device_vendor": "Keithley",
		"device_model": "2400",
		"read_termination": "\r",
		"write_termination": "\r",
		"timeout": 1,
		"xonxoff": 0
	}
}

#mapping for the instrument channels. The 'VINA', 'VIND', 'AUX1' and 'AUX2' keys
#must be specified
SIGNALS = {
	"VINA": {
		"power_supply": "PC",
		"channel": "1"
	},
	"VIND": {
		"power_supply": "PC",
		"channel": "2"
	},
	 "VDDA": {
		"power_supply": "VDD",
		"channel": "1"
	},
	"VDDD": {
		"power_supply": "VDD",
		"channel": "2"
	}
}
