"""
Subpackage for interacting with the CMS DAQ.
"""

import re
import os
import csv
import glob
import copy
import json
import shutil
import time
import subprocess

import toml
from lxml import etree

from wlt.config.main_config import DAQ_DIR
from wlt.config.main_config import CHIP_TYPE
from wlt.config.daq_config import DAQ_CONFIGS

from wlt.duts.chips import ITKPIX_CHIP
from wlt.duts.chips import CROC_CHIP
from wlt.duts.chips import CHIP_DATA

#TODO: should _run* methods only return the exit code?
#TODO: add global variable to set the output folder and the base name to use for
#      the output files
#TODO: define for each test what the outputs (files) of the DAQ are
#TODO: raise IOError if copying fails instead of printing (include JSON)
#TODO: create a timeout dictionary instead of specifying it as a parameter to
#      the called function
#TODO: each DAQ test should be a class

#DAQ configuration files for the different chips
_CONFIG_FILES = {
	CROC_CHIP: {
		"xml": os.path.join(DAQ_DIR, "CROC.xml"),
		"toml": os.path.join(DAQ_DIR, "CROC.toml"),
		"tools": os.path.join(DAQ_DIR, "CROCTools.toml")
	},
	ITKPIX_CHIP: {
		"xml": os.path.join(DAQ_DIR, "RD53B.xml"),
		"toml": os.path.join(DAQ_DIR, "RD53B.toml"),
		"tools": os.path.join(DAQ_DIR, "RD53BTools.toml")
	}
}

#setting the global variables for the DAQ configuration files
try:
	_XML_CONFIG_FILE = _CONFIG_FILES[CHIP_TYPE]["xml"]
	_TOML_CONFIG_FILE = _CONFIG_FILES[CHIP_TYPE]["toml"]
	_TOOLS_CONFIG_FILE = _CONFIG_FILES[CHIP_TYPE]["tools"]
except KeyError:
	raise RuntimeError("Could not set the DAQ configuration files for chip "
		f"type: {CHIP_TYPE}") from None

#load the default chip and DAQ configurations
try:
	CHIP_CONFIG = copy.deepcopy(DAQ_CONFIGS[CHIP_TYPE]["chip"])
	TOOLS_CONFIG = copy.deepcopy(DAQ_CONFIGS[CHIP_TYPE]["tools"])
	DAQ_CONFIG = copy.deepcopy(DAQ_CONFIGS[CHIP_TYPE]["daq"])
except KeyError:
	raise RuntimeError("Failed to load default DAQ configuration!")

#constants used to refer to different DAQ procedures
PROGRAMMING = 0
READ_REGS = 5
TEST_REGS = 10
COMMUNICATION = 15
DIGITAL_SCAN = 20
ANALOG_SCAN = 25
THRESHOLD_SCAN = 30
THRESHOLD_EQUAL = 35
ADC_CALIB = 40
DAC_CALIB = 45
RINGOSC_CALIB = 50
DATA_MERGING = 55
CHIP_ID = 60
EFUSE_PROG = 65
TEMP_CALIB = 70
TEMP_MEAS = 75
CAPINJ_MEAS = 80
MON_MUX = 85
IV_MON_MUX = 90
SCAN_CHAIN = 95

#constants used for setting what registers to print
SEL_ALL_REGS = 0
SEL_DEFAULT_REGS = 1
SEL_NONDEFAULT_REGS = 2

#executable to run for everything
_PH2_ACF_EXECUTABLE = "RD53BminiDAQ"

#base command which must be executed each time
_PH2_ACF_BASE_COMMAND = [
	_PH2_ACF_EXECUTABLE,
	"-h", #(h)ide the ROOT plots
	"-s", #(s)ave results
	"-f",
	f"{_XML_CONFIG_FILE}",
	"-t",
	f"{_TOOLS_CONFIG_FILE}"
]

#mapping between constants used to define DAQ procedures and Ph2_ACF tools
_PH2_ACF_TOOLS = {
	PROGRAMMING: None,
	COMMUNICATION: None,
	READ_REGS: "RegReader",
	TEST_REGS: "RegTest",
	DIGITAL_SCAN: "DigitalScan",
	ANALOG_SCAN: "AnalogScan",
	THRESHOLD_SCAN: "ThresholdScan",
	THRESHOLD_EQUAL: "ThresholdEqualization",
	ADC_CALIB: "ADCCalib",
	DAC_CALIB: "DACTest",
	RINGOSC_CALIB: "RingOsc",
	DATA_MERGING: "DataMergingTest",
	CHIP_ID: "ChipId",
	EFUSE_PROG: "Efuse",
	CAPINJ_MEAS: "capMeasure",
	MON_MUX: "MonMuxTest",
	IV_MON_MUX: "IVMonMuxTest",
	TEMP_CALIB: "TempCalib",
	TEMP_MEAS: "TempMeas",
	SCAN_CHAIN: "ScanChainTest"
}

#dictionary for parsing the results of Ph2_ACF. Each key corresponds to a
#Ph2_ACF tool
_TEST_RESULTS_REGEXES = {
	COMMUNICATION: {
		"init_sequences": {
			"type": int,
			"count": True,
			"regex": r"Trying initialization sequence number: (\d+)"
		},
		"init_tries": {
			"type": int,
			"count": True,
			"regex": r"Total number of active data lanes for tentative n. (\d+): "
		},
		"are_all_lanes_active": {
			"type": bool,
			"count": True,
			"regex": "All enabled data lanes are active"
		}
	},
	ANALOG_SCAN: {
		"n_enabled": {
			"type": int,
			"count": False,
			"regex": "Number of enabled pixels: ([0-9]+)"
		},
		"occupancy_enabled": {
			"type": float,
			"count": False,
			"regex": "Mean occupancy for enabled pixels: ([0-9.]+)"
		},
		"occupancy_disabled": {
			"type": float,
			"count": False,
			"regex": "Mean occupancy for disabled pixels: ([0-9.]+)"
		},
		"n_missing_events": {
			"type": int,
			"count": True,
			"regex": r"Expected \d+ events but received \d+"
		},
		"n_timeouts": {
			"type": int,
			"count": True,
			"regex": "Timed out"
		}
	},
	DIGITAL_SCAN: {
		"n_enabled": {
			"type": int,
			"count": False,
			"regex": "Number of enabled pixels: ([0-9]+)"
		},
		"occupancy_enabled": {
			"type": float,
			"count": False,
			"regex": "Mean occupancy for enabled pixels: ([0-9.]+)"
		},
		"occupancy_disabled": {
			"type": float,
			"count": False,
			"regex": "Mean occupancy for disabled pixels: ([0-9.]+)"
		},
		"n_missing_events": {
			"type": int,
			"count": True,
			"regex": r"Expected \d+ events but received \d+"
		},
		"n_timeouts": {
			"type": int,
			"count": True,
			"regex": "Timed out"
		}
	},
	#the values can also be `nan`, so everything in the results part must be
	#parsed. This happens only if something went wrong during the analysis on
	#the DAQ side
	THRESHOLD_SCAN: {
		"stuck_pixels": {
			"type": float,
			"count": False,
			"regex": r"Stuck pixels: ([-\w.]+)"
		},
		"not_stuck_pixels": {
			"type": int,
			"count": False,
			"regex": r"Succesfully computed threshold and noise for ([-\w.]+) pixels"
		},
		"mean_threshold": {
			"type": float,
			"count": False,
			"regex": r"Mean threshold: ([-\w.]+)"
		},
		"threshold_stddev": {
			"type": float,
			"count": False,
			"regex": r"Threshold stddev: ([-\w.]+)"
		},
		"min_threshold": {
			"type": float,
			"count": False,
			"regex": r"Min threshold: ([-\w.]+)"
		},
		"max_threshold": {
			"type": float,
			"count": False,
			"regex": r"Max threshold: ([-\w.]+)"
		},
		"mean_noise": {
			"type": float,
			"count": False,
			"regex": r"Mean noise: ([-\w.]+)"
		},
		"noise_stddev": {
			"type": float,
			"count": False,
			"regex": r"Noise stddev: ([-\w.]+)"
		},
		"n_missing_events": {
			"type": int,
			"count": True,
			"regex": r"Expected \d+ events but received \d+"
		},
		"n_timeouts": {
			"type": int,
			"count": True,
			"regex": "Timed out"
		}
	},
	DATA_MERGING: {
		"success": {
			"type": bool,
			"count": True,
			"regex": "Data merging test successful"
		},
	},
	EFUSE_PROG: {
		#value to write to the efuses
		"value_to_write": {
			"type": int,
			"count": False,
			"regex": r"Efuses word to be written: (\d+) \([\dA-Fa-fXx]+\)"
		},
		#efuses status **before** the programming procedure
		"value_before_prog": {
			"type": int,
			"count": False,
			"regex": r"Efuses word before programming: (\d+) \([\dA-Fa-fXx]+\)"
		},
		#efuses status **after** the programming procedure
		"value_after_prog": {
			"type": int,
			"count": False,
			"regex": r"Efuses word after programming: (\d+) \([\dA-Fa-fXx]+\)"
		},
		"success": {
			"type": bool,
			"count": True,
			"regex": "Efuses programming completed"
		}
	},
	DAC_CALIB: {
		"best_values": {
			"type": [str, int],
			"count": False,
			"regex": r"Best value for (\w+) is (\d+)"
		}
	},
	TEMP_MEAS: {
		"TEMPSENS_Analog_SLDO": {
			"type": float,
			"count": False,
			"regex": r"Temperature for TEMPSENS_SLDOA: -?(\d+.\d+)"
		},
		"TEMPSENS_Digital_SLDO": {
			"type": float,
			"count": False,
			"regex": r"Temperature for TEMPSENS_SLDOD: -?(\d+.\d+)"
		},
		"TEMPSENS_center": {
			"type": float,
			"count": False,
			"regex": r"Temperature for TEMPSENS_ACB: -?(\d+.\d+)"
		}
	},
	MON_MUX: {
		"VMUX": {
			"type": {str: float},
			"count": False,
			"regex": r'"(.*)" -> ([+-]?\d+.?\d*[eE][+-]?\d*) V'
		},
		"IMUX": {
			"type": {str: float},
			"count": False,
			"regex": r'"(.*)" -> ([+-]?\d+.?\d*[eE][+-]?\d*) A'
		}
	},
	SCAN_CHAIN: {
		"success": {
			"type": bool,
			"count": True,
			"regex": "Scan chain test completed successfully"
		},
		"n_errors": {
			"type": int,
			"count": False,
			"regex": "Number of errors: ([0-9]+)"
		},
		"errors": {
			"type": [int, str, int, int],
			"count": False,
			"regex": r'Number: (\d+); Type: (.+); Step: (\d+); Bit: (\d+)'
		}
	}
}

#configuration dictionary containing the paths to various results
_TEST_RESULTS_JSON = {
	ADC_CALIB: {},
	DAC_CALIB: {},
	RINGOSC_CALIB: {}
}

#compile regex for removing ANSI color codes
_ANSI_PATTERN = re.compile(r'\x1B\[\d+(;\d+){0,2}m')

#file that stores TDAC data
_TDAC_FILE = os.path.join(DAQ_DIR, "tdac.csv")

#data for temperature sensors calibration
_TEMP_DAT_FILE = os.path.join(DAQ_DIR, "tempCalibWLT.dat")

#folder containing the results
RESULTS_DIR = os.path.join(DAQ_DIR, "Results")

#Ph2_ACF log file
LOG_FILE = os.path.join(DAQ_DIR, "myeasylog.log")


class DAQException(Exception):
	"""Base exception for the `daq` subpackage in case of problems when running 
	the DAQ."""


class DAQTimeout(DAQException):
	"""Exception raised when a Ph2_ACF run times out."""


class DAQError(DAQException):
	"""Exception raised when a Ph2_ACF run returns a non-zero exit code."""


def _load_toml(file_):
	"""
	Loads a TOML configuration file.

	Parameters
	----------
	file_name : str
		The input file name.

	Returns
	-------
	dict
		The TOML configuration.

	Raises
	------
	RuntimeError
		If the loading of the TOML fails.
	"""

	try:
		config = toml.load(file_)
	except Exception as e:
		raise RuntimeError(e) from None

	return dict(config)


def _dump_toml(dict_, file_):
	"""
	Dumps a dictionary as a TOML file.

	If the file doesn't exist, it is created, otherwise it is overwritten.

	Parameters
	----------
	dict_ : dict
		The dictionary to be dumped.
	file_ : str or path
		Path to the file in which the dictionary should be dumped as TOML, 
		relative to the DAQ folder.

	Raises
	------
	RuntimeError
		If the procedure fails.
	"""

	try:
		with open(file_, "w+") as file_desc:
			try:
				toml.dump(dict_, file_desc)
			except Exception as e:
				raise RuntimeError("Could not dump TOML!") from e
	except IOError as e:
		raise RuntimeError("Caught exception while trying to open file!") from e


#TODO: add/edit method in order to clear the DAQ configuration as well
def _clear_toml():
	"""
	Clears the TOML configuration file.

	The file to clear is defined by the `_TOML_CONFIG_FILE` constant.

	Raises
	------
	RuntimeError
		If the procedure fails.
	"""

	try:
		_dump_toml(CHIP_CONFIG, _TOML_CONFIG_FILE)
	except RuntimeError as e:
		raise RuntimeError("Failed to dump default TOML configuration!") from e


def _load_xml(file_):
	"""Loads an XML configuration file."""

	try:
		with open(file_, "rb") as file_object:
			xml_data = file_object.read()
	except IOError:
		raise RuntimeError("Could not open the XML file!")

	#convert to XML
	root = etree.XML(xml_data)
	tree = etree.ElementTree(root)
	return tree


def _dump_xml_from_tree(xml_tree, file_):
	"""
	Dumps XML tree data to a file.

	Parameters
	----------
	xml_tree : lxml.etree.ElementTree
		ElementTree object with the XML data.
	file_ : str
		Name of the file to be used for output.
	"""

	try:
		with open(file_, "wb") as file_object:
			xml_tree.write(file_object, pretty_print=True)
	except IOError:
		raise RuntimeError("Could not open the XML file!")


def _dump_xml_from_str(xml_bytes, file_):
	"""
	Dumps XML bytes data to a file.

	Parameters
	----------
	xml_bytes : bytes
		`bytes` object with the XML data.
	file_ : str
		Name of the file to be used for output.
	"""

	#getting XML data
	root = etree.fromstring(xml_bytes)
	tree = etree.ElementTree(root)

	try:
		with open(file_, "wb") as file_object:
			tree.write(file_object, pretty_print=True)
	except IOError:
		raise RuntimeError("Could not open the XML file!")


def _generate_toml_configuration():
	"""Generates the TOML files needed to run the DAQ."""

	#chip configuration
	try:
		_dump_toml(CHIP_CONFIG, _TOML_CONFIG_FILE)
	except RuntimeError as e:
		raise RuntimeError("Could not generate chip TOML configuration!") from e

	#tools configuration
	try:
		_dump_toml(TOOLS_CONFIG, _TOOLS_CONFIG_FILE)
	except RuntimeError as e:
		raise RuntimeError("Could not generate DAQ TOML configuration!") from e


def _generate_xml_configuration():
	"""Generates the XML file needed to run the DAQ."""

	#DAQ configuration
	try:
		_dump_xml_from_str(DAQ_CONFIG, _XML_CONFIG_FILE)
	except RuntimeError as e:
		raise RuntimeError("Could not generate DAQ XML configuration!") from e


def copy_logs(destination):
	"""
	Copies the DAQ log file to the selected destination.

	Parameters
	----------
	destination : str
		Destination file for the copy of the DAQ log file.

	Raises
	------
	RuntimeError
		If the copying of the DAQ log file fails.
	"""

	try:
		shutil.copy(LOG_FILE, destination)
	except Exception as e:
		raise RuntimeError("Could not copy DAQ log file!") from e


def clear_logs():
	"""
	Clears the DAQ log file.

	Raises
	------
	RuntimeError
		If the removal of the logs fails.
	"""

	if os.path.exists(LOG_FILE):
		try:
			os.remove(LOG_FILE)
		except Exception:
			raise RuntimeError("Could not clear the DAQ log file!")


def clear_results():
	"""
	Clears the DAQ results folder.

	Raises
	------
	RuntimeError
		If the removal of the folder fails.
	"""

	if os.path.isdir(RESULTS_DIR):
		try:
			shutil.rmtree(RESULTS_DIR)
		except Exception:
			raise RuntimeError("Could not clear the DAQ results!")


def clear_tdac_data():
	"""
	Clears the TDAC configuration.

	Raises
	------
	RuntimeError
		If the removal fo the TDAC file fails.
	"""

	if os.path.exists(_TDAC_FILE):
		try:
			os.remove(_TDAC_FILE)
		except OSError as e:
			raise RuntimeError from e


def clear_dat_data():
	"""
	Clears the TDAC configuration.

	Raises
	------
	RuntimeError
		If the removal fo the TDAC file fails.
	"""

	if os.path.exists(_TEMP_DAT_FILE):
		try:
			os.remove(_TEMP_DAT_FILE)
		except OSError as e:
			raise RuntimeError from e


def clear_chip_config():
	"""
	Clears all the DAQ configurations related to the chip registers.

	Raises
	------
	RuntimeError
		If the clearing of TOML or TDAC data fails.
	"""

	#write the default values for chip registers on the TOML
	try:
		_clear_toml()
	except Exception as e:
		raise RuntimeError("Clearing of chip TOML configuration failed!") from e

	#clear TDAC data
	try:
		clear_tdac_data()
	except RuntimeError as e:
		raise RuntimeError("Clearing of chip TDAC data failed!") from e


def init(use_default_config = True, remove_logs = True, remove_results = False,
		remove_dat = False):
	"""
	Initializes the DAQ.

	Checks that the required configuration files are there, otherwise it 
	generates it.

	Parameters
	----------
	use_default_config : bool, optional
		Sets the chip configuration to the default values if True.
	remove_logs : bool, optional
		Removes the log files if True.
	remove_results : bool, optional
		Removes the Results directory if True.
	remove_dat : bool, optional
		Remove temperature sensor calibration data.

	Raises
	------
	RuntimeError
		If the initialization fails.
	"""

	global CHIP_CONFIG

	#set the chip configuration back to the default
	if use_default_config is True:
		try:
			CHIP_CONFIG = copy.deepcopy(DAQ_CONFIGS[CHIP_TYPE]["chip"])
		except RuntimeError:
			raise RuntimeError("Could not load default chip configuration!")

	#clear log file
	if remove_logs is True:
		try:
			clear_logs()
		except RuntimeError:
			pass

	#clear results
	if remove_results is True:
		try:
			clear_results()
		except RuntimeError:
			pass

	#clear TDAC
	try:
		clear_tdac_data()
	except RuntimeError:
		raise RuntimeError("Could not clear the TDAC data!")

	#clear DAT for temperature sensors
	if remove_dat is True:
		try:
			clear_dat_data()
		except RuntimeError:
			raise RuntimeError("Could not clear the DATA data!")

	#generate TOML configuration for chip and DAQ
	try:
		_generate_toml_configuration()
	except RuntimeError:
		raise RuntimeError("The TOML initialization process could not be "
			"completed!")

	#generate XML configuration for DAQ
	try:
		_generate_xml_configuration()
	except RuntimeError:
		raise RuntimeError("The XML initialization process could not be "
			"completed!")


#function used to parse the standard output of the DAQ executable
def _parse_stdout(stdout, procedure):
	results = {}
	for result, data in _TEST_RESULTS_REGEXES[procedure].items():
		#getting the regex data
		try:
			type_ = data["type"]
			regex = data["regex"]
			is_counted = data["count"]
		except KeyError:
			raise RuntimeError("No parsing data available for procedure: "
				f"{procedure}")

		#parsing the results
		results[result] = None

		#count all occurrences
		if is_counted:
			matches_list = re.findall(regex, stdout)
			len_list = len(matches_list)
			#if the requested type is `bool`, convert the number of occurrences
			#to `bool`, otherwise keep it an `int`
			if type_ == bool:
				results[result] = bool(len_list)
			else:
				results[result] = len_list
			continue

		#handle the case in which the type is composite but with no counting
		if isinstance(type_, list):
			matches = []
			#performing the conversions
			for item in re.findall(regex, stdout):
				#convert the tuple to a list for easier handling
				item = list(item)
				#looping on all subtypes
				for i, subtype_ in enumerate(type_):
					try:
						#convert the item
						item[i] = subtype_(item[i])
					except ValueError:
						item[i] = None
				#convert it back to a tuple
				item = tuple(item)
				matches.append(item)
			results[result] = matches
			continue

		#handle the case in which the type is a dictionary
		if isinstance(type_, dict):
			matches = {}
			#performing the conversions
			for item in re.findall(regex, stdout):
				#convert the tuple to a list for easier handling
				key = item[0]
				val = item[1]
				values = [key, val]
				#getting the subtypes
				key_type = list(type_.keys())[0]
				item_type = list(type_.values())[0]
				subtypes_ = [key_type, item_type]
				#converting the values
				for i, val in enumerate(values):
					try:
						#convert the item
						val = subtypes_[i](val)
					except ValueError:
						val = None
				#adding to the dictionary
				matches[key] = val
			results[result] = matches
			continue

		#handle the usual case in which there is only one value to parse
		match = re.search(regex, stdout)
		if match is not None:
			parsed = match.group(1)
			#try to typecast the result. If it fails, set None
			try:
				results[result] = type_(parsed)
			except ValueError:
				results[result] = None

	return results


def _get_latest_file(tool, extension = ".root"):
	#get the list of folders
	dir_path_glob = os.path.join(RESULTS_DIR, f"{tool}*/")
	list_of_dirs = glob.glob(dir_path_glob)

	#get the latest directory
	if len(list_of_dirs) == 0:
		return None
	latest_dir = max(list_of_dirs, key=os.path.getmtime)

	#check that it's really a directory
	if os.path.isdir(latest_dir) is False:
		return None

	#get the list of files
	file_path_glob = os.path.join(latest_dir, f"*{extension}")
	list_of_files = glob.glob(file_path_glob)

	#get the latest file
	if len(list_of_files) == 0:
		return None
	latest_file = max(list_of_files, key=os.path.getmtime)
	return latest_file


def _get_latest_results(procedure, extension = ".root"):
	#get the tool and construct the filename for the results
	tool = _PH2_ACF_TOOLS[procedure] #get tool name
	filename = _get_latest_file(tool, extension)
	return filename


def _copy_latest_results(procedure, destination, extension = ".root"):
	#get the tool and construct the filename for the results
	tool = _PH2_ACF_TOOLS[procedure] #get tool name
	filename_in = _get_latest_file(tool, extension)

	if filename_in is None:
		raise ValueError("File not found!")

	try:
		shutil.copy(filename_in, destination)
	except Exception as e:
		raise RuntimeError("Could not copy file!") from e


def _read_json_data(file_, is_nested=True):
	try:
		with open(file_, "r") as json_file:
			raw_results = json.load(json_file)

			#extract chip data and remove unneeded fields if nested
			if is_nested:
				try:
					results = dict(raw_results["chips"][0])
				except IndexError as e:
					raise RuntimeError("No JSON data found!") from e
				for key in ["board", "hybrid", "id"]:
					try:
						results.pop(key)
					except KeyError:
						continue
			else:
				results = dict(raw_results)
	except FileNotFoundError as e:
		raise RuntimeError("File not found!") from e

	return results


def _run(*args, procedure = None, print_ = False, timeout = 30):
	"""
	Runs the RD53miniDAQ executable from Ph2_ACF and reads the output line by 
	line by polling the standard output of the process.

	Parameters
	----------
	*args
		The parameters to pass to the DAQ executable.
	procedure : str, optional
		DAQ procedure to run.
	print_ : bool, optional
		Print the standard output of the DAQ run.
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.

	Returns
	-------
	str
		The standard output from the DAQ run.

	Raises
	------
	DAQError
		If the DAQ executable call returns a non-zero exit code.
	DAQTimeout
		 If the DAQ executable call times out.
	"""

	#putting together the command to run. The global variable is a **list** and
	#**MUST** be copied! If this is not performed, the append method changes the
	#value of the command
	command = copy.deepcopy(_PH2_ACF_BASE_COMMAND)
	for arg in args:
		command.append(arg)

	#add the tool related to the procedure if it's not None
	if procedure:
		tool = _PH2_ACF_TOOLS[procedure]
		if tool:
			command.append(tool)

	#running process
	try:
		process = subprocess.run(command, stdout=subprocess.PIPE,
			stderr=subprocess.PIPE, cwd=DAQ_DIR, bufsize=1, text=True,
			timeout=timeout, check=True)
	except subprocess.TimeoutExpired as e:
		raise DAQTimeout("The DAQ executable timed out!") from e
	except subprocess.CalledProcessError as e:
		raise DAQError("The DAQ executable returned a non-zero exit "
			"code!") from e

	#strip ANSI color codes
	stdout_clean = _ANSI_PATTERN.sub('', process.stdout)

	if print_ is True:
		print(stdout_clean.strip())

	return stdout_clean


def _parse_registers(stdout):
	"""
	Parses the output of RD53BminiDAQ and gets all register values.

	Parameters
	----------
	stdout : str
		The output of a RD53BminiDAQ run.

	Returns
	-------
	dict
		A dictionary containing all the parsed registers and their value.
	"""

	#regular expression for parsing the registers from the RD53BminiDAQ output
	regex = (
		r"INFO \[default\] "             #log entry type
		r"\s+"                           #variable number of spaces
		r"(?P<reg>\w+) = (?P<val>\d+)"   #key = val pair
		r"( \(default: (?P<def>\d+)\))?" #optional default
	)
	pattern = re.compile(regex)

	registers = {}
	for match in pattern.finditer(stdout):
		reg_dict = match.groupdict() #get a dictionary from the regex match
		reg_name = reg_dict["reg"]  #register name
		reg_val = reg_dict["val"]   #register value
		reg_def = reg_dict["def"]   #default value
		is_def = (reg_def is None)  #is the value the default one?
		registers[reg_name] = {"value": int(reg_val), "is_default": is_def}

	return dict(registers)


def _print_toml():
	"""Print the TOML config file."""

	with open(_TOML_CONFIG_FILE, "r") as config_file:
		print(config_file.read())


def _print_registers(registers, flag = SEL_NONDEFAULT_REGS,
		separator_char = "-"):
	"""
	Print all the registers taken from the output of the DAQ executable.

	Parameters
	----------
	registers : dict
		Registers to be printed.
	flag : int, optional
		What to print: SEL_ALL_REGS, SEL_DEFAULT_REGS, SEL_NONDEFAULT_REGS.
	separator_char : str, optional
		Separator character to use for the banner.
	"""

	width = 45
	separator = separator_char * width

	#printing the header
	print(separator)
	print("{'Register':<25}{'Value':^10}{'Default?':>10}")
	print(separator)

	#printing the registers
	for reg in registers.keys():
		value = registers[reg]["value"]
		is_default = registers[reg]["is_default"]
		output = f"{reg:<25}{value:^10}{is_default:>10}"

		#list containing boolean conditions for printing
		bool_print_values = []

		#choose whether to print or not depending on flag and `is_default`
		bool_print_values.append(flag == SEL_ALL_REGS)
		bool_print_values.append(flag == SEL_DEFAULT_REGS and is_default)
		bool_print_values.append(flag == SEL_NONDEFAULT_REGS and not is_default)

		#if any of the above conditions for printing apply, print the `output`
		#variable
		do_print = any(bool_print_values)

		if do_print:
			print(output)

	#printing the separator at the end
	print(separator)


def _dump_regs():
	"""Prints the registers to stdout."""

	#run with the defined tool
	try:
		stdout = _run(procedure=READ_REGS)
	except DAQException:
		print("No data")
		return
	regs = _parse_registers(stdout)

	#printing a `REGS` dictionary
	output = "REGS = {"
	for reg, val in regs.items():
		output += f"\n\t\"{reg}\": {val},"
	output += "\n}"

	print(output)


def program(timeout = 3):
	"""
	Program the chip.

	Parameters
	----------
	n_tries : int, optional
		Number of tries to perform to program the chip.
	timeout : int or float, optional
		DAQ timeout time.

	Raises
	------
	DAQError
		If the programming of the chip fails.
	DAQTimeout
		If the programming of the chip goes to timeout (failed communication).
	"""

	#running the DAQ
	try:
		_run(procedure=PROGRAMMING, timeout=timeout)
	except DAQException:
		raise


def write_register(reg, value, timeout = 5):
	"""
	Write a chip register.

	Parameters
	----------
	reg : str
		The name of the register.
	value : int
		The register value.
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.

	Raises
	------
	RuntimeError
		When the writing fails due to a timeout or due to a readback value 
		which is different from the written value.
	"""

	config = _load_toml(_TOML_CONFIG_FILE)
	try:
		config["Registers"][reg] = value
	except KeyError:
		config["Registers"] = {}
		config["Registers"][reg] = value
	_dump_toml(config, _TOML_CONFIG_FILE)

	#running the DAQ
	try:
		stdout = _run(procedure=READ_REGS, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("DAQ error while trying to "
			f"write {value} to register {reg}") from e

	registers = _parse_registers(stdout)

	#checking that the register is there
	try:
		readback_value = registers[reg]["value"]
	except KeyError:
		raise RuntimeError(f"Register {reg} not found!") from None

	#checking the readback value
	if readback_value != value:
		raise RuntimeError(f"Writing {value} to register {reg} failed! "
			f"Readback value: {readback_value}")


def write_vdd_trim_bits(vrefa_trim = 8, vrefd_trim = 8, en_ilim_vrefa = False,
		en_ilim_vrefd = False, timeout = 5):
	"""
	Method to write the VDD trim bits to the chip.

	Parameters
	----------
	vrefa_trim, vrefd_trim : int, optional
		The trim values to write.
	en_ilim_vrefa, en_ilim_vrefd : bool, optional
		Enable or disable undershunt protection.
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.

	Raises
	------
	ValueError
		If a wrong value for the parameters is detected.
	RuntimeError
		If writing to the VOLTAGE_TRIM global register fails.
	"""

	#checking the input trim codes
	range_vref_trim = range(0, 16)
	is_vrefa_good = (vrefa_trim in range_vref_trim)
	is_vrefd_good = (vrefd_trim in range_vref_trim)
	if (is_vrefa_good is False) or (is_vrefd_good is False):
		raise ValueError("Wrong trim code(s)! VDD trim codes must go from 0 to "
			"15")

	#checking enables
	range_en_ilim = range(0,2)
	is_ilim_vrefa_good = en_ilim_vrefa in range_en_ilim
	is_ilim_vrefd_good = en_ilim_vrefd in range_en_ilim
	if (is_ilim_vrefa_good is False) or (is_ilim_vrefd_good is False):
		raise ValueError("Wrong value(s) for undershunt protection enable!")

	#getting the value of the VOLTAGE_TRIM register
	voltage_trim = 0
	voltage_trim += (en_ilim_vrefa << 9)
	voltage_trim += (en_ilim_vrefd << 8)
	voltage_trim += (vrefa_trim << 4)
	voltage_trim += (vrefd_trim)

	#trying to write the value
	try:
		write_register("VOLTAGE_TRIM", voltage_trim, timeout=timeout)
	except RuntimeError as e:
		raise RuntimeError("Writing of the VDD trim bits failed!") from e


def write_registers(dict_, timeout = 5):
	"""
	Write multiple chip registers.

	Parameters
	----------
	dict_ : dict
		Dictionary containing the registers to edit and their value.
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.

	Returns
	-------
	dict
		Dictionary with the status of all parsed chip registers.

	Raises
	------
	RuntimeError
		When the writing fails due to a timeout or due to readback values which
		are is different from the written values.
	"""

	config = _load_toml(_TOML_CONFIG_FILE)

	#checking that the `Registers` key is there. If it's not, add it
	try:
		config["Registers"]
	except KeyError:
		config["Registers"] = {}

	#writing the values to the configuration dictionary
	for reg, val in dict_.items():
		config["Registers"][reg] = val

	#save on file
	_dump_toml(config, _TOML_CONFIG_FILE)

	#running the DAQ
	try:
		stdout = _run(procedure=READ_REGS, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("DAQ error while trying to write "
			"registers") from e

	registers = _parse_registers(stdout)

	#counter for the number of failed writes
	errors_cnt = 0

	#checking the readback value for the written registers
	for reg, val in dict_.items():
		readback_value = registers[reg]["value"]
		if readback_value != val:
			errors_cnt += 1

	#raise exception if anything failed
	if errors_cnt > 0:
		raise RuntimeError("Error while writing multiple chip registers! "
			f"Number of failed writes: {errors_cnt}")

	return registers


def write_register_defaults(timeout = 5):
	"""
	Sets the default configuration of the chip in the TOML and runs the
	DAQ to apply it.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.

	Returns
	-------
	dict
		Dictionary with all readback values of the configured registers.
	"""

	#removing all TOML configuration
	_clear_toml()

	#running the DAQ
	try:
		_run(procedure=PROGRAMMING, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("Couldn't write the default chip configuration: "
		"DAQ executable timed out!") from e


def communication_test(timeout = 30):
	"""
	Perform a communication test with the chip and get the number of failed
	tries before establishing the communication.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.

	Returns
	-------
	tuple of int
		Tuple with the number of failed sequences and the total number of 
		failed tries.

	Raises
	------
	RuntimeError
		If the DAQ executable did not run successfully.
	"""

	#set the default chip configuration
	#write_register_defaults()

	#run the DAQ
	try:
		stdout = _run(procedure=COMMUNICATION, timeout=timeout)
	except DAQException as e:
		#TODO: get output even after timeouts
		#TODO: do not raise RuntimeError but return all measured values anyway
		raise RuntimeError("An error occurred during a communication "
			"test!") from e

	#parsing the standard output of the executable
	try:
		results = _parse_stdout(stdout, COMMUNICATION)
	except RuntimeError as e:
		raise RuntimeError("Failed to parse the results of the test!") from e

	n_sequences = results["init_sequences"]
	n_tentatives = results["init_tries"]
	are_all_lanes_active = results["are_all_lanes_active"]

	#Getting the number of **failed** tries. The parsing returns **all** tries,
	#so 1 must be subtracted but **only if it communicates at all**. If it does
	#not communicate, a RuntimeError is raised and this part of the code is not
	#reached at all, so nothing is returned. Also, the case in which there is
	#no need to try any initialisation sequences must be considered. In that
	#case, nothing has to be subtracted at all
	if are_all_lanes_active is False:
		n_failed_sequences = n_sequences - 1
		n_failed_tentatives = n_tentatives - 1
	else:
		n_failed_sequences = n_sequences
		n_failed_tentatives = n_tentatives

	#return a dictionary with the results
	return n_failed_sequences, n_failed_tentatives


def register_reading_test(timeout = 20):
	"""
	Runs a test on the chip registers.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.

	Raises
	------
	RuntimeError
		If the DAQ executable did not run successfully.
	"""

	#set everything to default
	config = {"Registers": {}}
	_dump_toml(config, _TOML_CONFIG_FILE)

	#run the DAQ
	try:
		stdout = _run(procedure=READ_REGS, timeout=timeout)
	except RuntimeError as e:
		raise e

	#parse the registers and get the number of initialization sequences and the
	#total number of communication tries
	try:
		registers = _parse_registers(stdout)
	except RuntimeError as e:
		raise e

	print(registers)


def _registers_test(timeout = 80, test_global_registers = True,
		test_pixel_registers = False, save_successes = False,
		output_path = None):
	"""
	Private method for testing both global and pixel registers.

	Parameters
	----------
	test_global_registers, test_pixel_registers : bool, optional
		Select which registers to test.
	save_successes : bool, optional
		If True, registers which passed a test will be saved to a CSV file by 
		the DAQ. If it's False, only registers which **failed** a test will be 
		saved.
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.
	output_path : string or path-like object, optional
		Copy the test results to the desired location.

	Returns
	-------
	dict
		Dictionary containing the data for the registers that failed a test.

	Raises
	------
	RuntimeError
		If something goes wrong during the testing.
	"""

	#setting the `testPixels` parameter of the test
	tool = _PH2_ACF_TOOLS[TEST_REGS]
	args = TOOLS_CONFIG[tool]["args"] #default configuration
	args["testRegs"] = test_global_registers
	args["testPixels"] = test_pixel_registers
	args["saveSuccesses"] = save_successes
	try:
		config = _load_toml(_TOOLS_CONFIG_FILE)
	except RuntimeError as e:
		raise RuntimeError("Could not load TOML configuration!") from e
	config[tool]["args"].update(args)
	try:
		_dump_toml(config, _TOOLS_CONFIG_FILE)
	except RuntimeError as e:
		raise RuntimeError("Could not write TOML configuration!") from e

	#running Ph2_ACF
	try:
		_run(procedure=TEST_REGS, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("Error while running the register test!") from e

	#reading the results
	failed_regs = {}
	csv_filename = _get_latest_results(procedure=TEST_REGS, extension=".csv")
	if csv_filename is None:
		raise RuntimeError("Could not open register test results!")
	with open(csv_filename, "r") as csv_file:
		csv_data = csv.DictReader(csv_file, delimiter=";", skipinitialspace=True)
		for row in csv_data:
			if row["Result"] == "ERR":
				reg_name = row["Register"]
				expected = row["Expected"]
				readback = row["Readback"]
				result = row["Result"]

				#if this register has no data, add it
				if reg_name not in failed_regs.keys():
					failed_regs[reg_name] = []

				#handle a no readback entry. A missing entry is stored in the
				#CSV as '-' * 16. 16 represents the information stored in two
				#pixels (8 bits for each one)
				if readback == "-" * 16:
					readback = None

				#adding the failed try to the list
				failed_regs[reg_name].append(
					{
						"expected": expected,
						"readback": readback,
						"passed": (result == "OK")
					}
				)

	#copying the results
	if output_path is not None:
		try:
			_copy_latest_results(procedure=TEST_REGS, destination=output_path,
				extension=".csv")
		except Exception:
			print("Could not copy data to the required location!")

	return failed_regs


def global_registers_test(timeout = 20, output_path = None):
	"""
	Runs a test of the global chip registers.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.
	output_path : string or path-like object, optional
		Copy the test results to the desired location.

	Returns
	-------
	dict
		Dictionary containing the data for the registers that failed a test.

	Raises
	------
	RuntimeError
		If something goes wrong during the test.
	"""

	try:
		failed_regs = _registers_test(
			test_global_registers=True,
			test_pixel_registers=False,
			timeout=timeout,
			output_path=output_path
		)
	except RuntimeError as e:
		raise RuntimeError("Could not execute the global registers test!") from e
	return failed_regs


def pixel_registers_test(timeout = 320, output_path = None):
	"""
	Runs a test of the pixel chip registers.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.
	output_path : string or path-like object, optional
		Copy the test results to the desired location.

	Returns
	-------
	dict
		Dictionary containing the data for the registers that failed a test.

	Raises
	------
	RuntimeError
		If something goes wrong during the test.
	"""

	try:
		failed_regs = _registers_test(
			test_global_registers=False,
			test_pixel_registers=True,
			timeout=timeout,
			output_path=output_path
		)
	except RuntimeError as e:
		raise RuntimeError("Could not execute the pixel registers test!") from e
	return failed_regs


def _scan_frontend(scan_type = DIGITAL_SCAN, print_ = False, output_path = None,
		timeout = 30):
	"""
	Performs an analog or digital frontend scan.

	Parameters
	---------
	scan_type : int
		The type of the scan to run. Must be one of the module constants: 
		`ANALOG_SCAN`, `DIGITAL_SCAN`.
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.
	print_ : bool, optional
		The function prints to stdout if this parameter is True.
	output_path : str, optional
		If it's not None, the results of the tests are copied in the required 
		location.

	Returns
	-------
	dict
		Dictionary with the results.

	Raises
	------
	DAQError
		If the DAQ run terminates unexpectedly.
	DAQTimeout
		If a DAQ timeout is detected.
	ValueError
		If a wrong scan type is detected.
	RuntimeError
		If another error happens (e.g., failed results parsing).
	"""

	#check the `scan_type` parameter
	if scan_type not in [ANALOG_SCAN, DIGITAL_SCAN]:
		raise ValueError(f"Invalid frontend scan type: {scan_type}")

	#running process
	try:
		stdout = _run(procedure=scan_type, print_=print_, timeout=timeout)
	except DAQError as e:
		raise DAQError("DAQ run failed for frontend scan!") from e
	except DAQTimeout as e:
		raise DAQTimeout("DAQ timeout during frontend scan!") from e

	#analyzing the lines
	try:
		results = _parse_stdout(stdout=stdout, procedure=scan_type)
	except RuntimeError as e:
		raise RuntimeError("Could not parse the results of the frontend "
			"scan!") from e

	#copying data in `output_path`
	if output_path is not None:
		try:
			_copy_latest_results(procedure=scan_type, destination=output_path)
		except Exception:
			print("Could not copy data to the required location!")

	return results


def digital_scan(timeout = 30, print_ = False, output_path = None):
	"""
	Performs a scan on the digital chip front-end.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.
	print_ : bool, optional
		The function prints to stdout if this parameter is True.
	output_path : str, optional
		If it's not None, the results of the tests are copied in the required 
		location.

	Returns
	-------
	dict
		Dictionary with the results.

	Raises
	------
	DAQError
		If the DAQ run terminates unexpectedly.
	DAQTimeout
		If a DAQ timeout is detected.
	"""

	try:
		results = _scan_frontend(DIGITAL_SCAN, print_=print_,
			output_path=output_path, timeout=timeout)
	except DAQError as e:
		raise DAQError("Could not complete the digital scan due to an "
			"unexpected DAQ failure!") from e
	except DAQTimeout as e:
		raise DAQTimeout("Could not complete the digital scan due to an "
			"unexpected DAQ timeout!") from e

	return results


def analog_scan(timeout = 30, print_ = False, output_path = None):
	"""
	Performs a scan on the analog chip front-end.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.
	print_ : bool, optional
		The function prints to stdout if this parameter is True.
	output_path : str, optional
		If it's not None, the results of the tests are copied in the required 
		location.

	Returns
	-------
	dict
		Dictionary with the results.

	Raises
	------
	DAQError
		If the DAQ run terminates unexpectedly.
	DAQTimeout
		If a DAQ timeout is detected.
	"""

	try:
		results = _scan_frontend(ANALOG_SCAN, print_=print_,
		output_path=output_path, timeout=timeout)
	except DAQError as e:
		raise DAQError("Could not complete the analog scan due to an "
			"unexpected DAQ failure!") from e
	except DAQTimeout as e:
		raise DAQTimeout("Could not complete the analog scan due to an "
			"unexpected DAQ timeout!") from e
	return results


def threshold_scan(vcal_med = None, vcal_high_range = None,
		vcal_high_step = None, timeout = 60, print_ = False,
		output_path = None):
	"""
	Performs a threshold scan on the analog chip front-end.

	If the `vcal_med`, `vcal_high_range` and `vcal_high_step` parameters are 
	None, the default configuration for these values is used.

	Parameters
	----------
	vcal_med : int, optional
		Fixed value of the `VCAL_MED` DAC.
	vcal_high_range : tuple of int, optional
		Range of values for the `VCAL_HIGH` register.
	vcal_high_step : int, optional
		Step size for increasing `VCAL_HIGH`.
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.
	print_ : bool, optional
		The function prints to stdout if this parameter is True.
	output_path : str, optional
		If it's not None, the results of the tests are copied in the required 
		location.

	Returns
	-------
	dict
		Dictionary with the results.

	Raises
	------
	RuntimeError
		If the threshold scan fails due to a DAQ runtime error.
	"""

	#getting the default values
	tool = _PH2_ACF_TOOLS[THRESHOLD_SCAN]
	args = TOOLS_CONFIG[tool]["args"] #default configuration

	#updating the default configuration with the parameters that are not None
	if vcal_med is not None:
		args["vcalMed"] = vcal_med
	if vcal_high_range is not None:
		args["vcalRange"] = [vcal_high_range[0], vcal_high_range[1]]
	if vcal_high_step is not None:
		args["vcalStep"] = vcal_high_step

	#setting the DAQ configuration in the config file
	config = _load_toml(_TOOLS_CONFIG_FILE)
	config[tool]["args"].update(args)
	_dump_toml(config, _TOOLS_CONFIG_FILE)

	#running process
	try:
		stdout = _run(procedure=THRESHOLD_SCAN, print_=print_, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("Could not complete the threshold scan!") from e

	#analyzing the lines
	try:
		results = _parse_stdout(stdout=stdout, procedure=THRESHOLD_SCAN)
	except RuntimeError as e:
		raise RuntimeError("Could not parse the results of the threshold "
			"scan!") from e

	#calculating the number of stuck pixels
	if results["stuck_pixels"] is None:
		n_pixels = CHIP_DATA[CROC_CHIP]["pixel_count"]
		try:
			n_stuck = n_pixels - results["not_stuck_pixels"]
			frac_stuck = n_stuck / n_pixels
			results["stuck_pixels"] = frac_stuck
		except Exception:
			pass

	#copying data in `output_path`
	if output_path is not None:
		try:
			_copy_latest_results(procedure=THRESHOLD_SCAN,
				destination=output_path)
		except Exception:
			print("Could not copy data to the required location!")

	return results


def threshold_equalization(target_threshold = None, timeout = 80,
		print_ = False, output_path_tdac = None, n_steps = None,
		output_path_root = None):
	"""
	Performs a trimming of the chip trimming DACs for the threshold.

	Parameters
	----------
	target_threshold : int, optional
		`VCAL_HIGH` - `VCAL_MED` target for the threshold equalization.
	n_steps : int, optional
		Number of steps to perform during the threshold equalization.
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.
	print_ : bool, optional
		Defines whether the function will print the DAQ logging information to 
		stdout or not.
	output_path_root, output_path_tdac : str, optional
		If they are not None, the results of the tests are copied in the 
		required location.

	RuntimeError
		If the threshold equalization fails due to a DAQ runtime error.
	"""

	#getting the default values
	tool = _PH2_ACF_TOOLS[THRESHOLD_EQUAL]
	args = TOOLS_CONFIG[tool]["args"] #default configuration

	#updating the default configuration with the parameters that are not None
	if target_threshold is not None:
		args["targetThreshold"] = target_threshold
	if n_steps is not None:
		args["nSteps"] = n_steps

	#setting the DAQ configuration in the config file
	config = _load_toml(_TOOLS_CONFIG_FILE)
	config[tool]["args"].update(args)
	_dump_toml(config, _TOOLS_CONFIG_FILE)

	#running process
	try:
		_run(procedure=THRESHOLD_EQUAL, print_=print_, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("Could not complete the threshold equalization!") from e

	#copying ROOT file
	if output_path_root is not None:
		try:
			_copy_latest_results(procedure=THRESHOLD_EQUAL,
				destination=output_path_root)
		except Exception:
			print("Could not copy data to the required location!")

	#copying TDAC file
	if output_path_tdac is not None:
		try:
			shutil.copy(_TDAC_FILE, output_path_tdac)
		except Exception as e:
			raise RuntimeError("Could not copy file!") from e

def injection_capacitance_measurement(timeout = 40, output_path_json = None,
		print_ = False):
	"""
	Runs the unjection capacitance measurement procedure.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.
	print_ : bool, optional
		Defines whether the function will print the DAQ logging information to 
		stdout or not.
	output_path_json : str, optional
		If they are not None, the results of the tests are copied in the 
		required location.

	Raises
	------
	RuntimeError
		If the injection capacitance measurement fails.
	"""

	#running process
	try:
		_run(procedure=CAPINJ_MEAS, print_=print_, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("Injection capacitance measurement failed!") from e

	#reading the results
	json_file = _get_latest_results(procedure=CAPINJ_MEAS, extension=".json")
	if json_file is not None:
		try:
			results = _read_json_data(json_file, is_nested=False)
		except RuntimeError:
			results = {}

	#copying JSON file
	if output_path_json is not None:
		try:
			_copy_latest_results(procedure=CAPINJ_MEAS,
				destination=output_path_json, extension=".json")
		except Exception as e:
			raise RuntimeError("Could not copy file!") from e

	return results


def adc_calibration(timeout = 40, output_path_root = None,
		output_path_json = None, print_ = False):
	"""
	Runs the ADC calibration procedure.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.
	print_ : bool, optional
		Defines whether the function will print the DAQ logging information to 
		stdout or not.
	output_path_root, output_path_json : str, optional
		If they are not None, the results of the tests are copied in the 
		required location.

	Raises
	------
	RuntimeError
		If the ADC calibration fails.
	"""

	#running process
	try:
		_run(procedure=ADC_CALIB, print_=print_, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("ADC calibration failed!") from e

	#reading the results
	json_file = _get_latest_results(procedure=ADC_CALIB, extension=".json")
	if json_file is not None:
		try:
			results = _read_json_data(json_file)
		except RuntimeError:
			results = {}

	#copying ROOT file
	if output_path_root is not None:
		try:
			_copy_latest_results(procedure=ADC_CALIB,
				destination=output_path_root)
		except Exception as e:
			raise RuntimeError("Could not copy data to the required "
				"location!") from e

	#copying JSON file
	if output_path_json is not None:
		try:
			_copy_latest_results(procedure=ADC_CALIB,
				destination=output_path_json, extension=".json")
			#shutil.copy(adc_calib_xml, output_path_xml)
		except Exception as e:
			raise RuntimeError("Could not copy file!") from e

	return results


def dac_calibration(timeout = 60, output_path_root = None,
		output_path_json = None, print_ = False):
	"""
	Runs the DACs calibration procedure.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.
	print_ : bool, optional
		Defines whether the function will print the DAQ logging information to 
		stdout or not.
	output_path_root, output_path_json : str, optional
		If they are not None, the results of the tests are copied in the 
		required location.

	Raises
	------
	RuntimeError
		If the DAC calibration fails.
	"""

	#running process
	try:
		stdout = _run(procedure=DAC_CALIB, print_=print_, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("DACs calibration failed!") from e

	#reading the results
	json_file = _get_latest_results(procedure=DAC_CALIB, extension=".json")
	if json_file is not None:
		try:
			results = _read_json_data(json_file)
		except RuntimeError:
			results = {}

	#also reading the standard output
	try:
		stdout_results = _parse_stdout(stdout=stdout, procedure=DAC_CALIB)
		#converting to a dictionary
		stdout_results_dict = {key:val for key, val in stdout_results["best_values"]}
		#adding to the global results
		results["best_values"] = stdout_results_dict
	except RuntimeError as e:
		raise RuntimeError("Could not parse the results of the DAC "
			"calibration!") from e

	#copying ROOT file
	if output_path_root is not None:
		try:
			_copy_latest_results(procedure=DAC_CALIB,
				destination=output_path_root)
		except Exception as e:
			raise RuntimeError("Could not copy data to the required "
				"location!") from e

	#copying JSON file
	if output_path_json is not None:
		try:
			_copy_latest_results(procedure=DAC_CALIB,
				destination=output_path_json, extension=".json")
			#shutil.copy(adc_calib_xml, output_path_xml)
		except Exception as e:
			raise RuntimeError("Could not copy file!") from e

	return results


def ringosc_calibration(timeout = 40, output_path_root = None,
		output_path_json = None, print_ = False):
	"""
	Runs the ring oscillators calibration procedure.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.
	print_ : bool, optional
		Defines whether the function will print the DAQ logging information to 
		stdout or not.
	output_path_root, output_path_json : str, optional
		If they are not Nones, the results of the tests are copied in the 
		required location.

	Raises
	------
	RuntimeError
		If the DAC calibration fails.
	"""

	#running process
	try:
		_run(procedure=RINGOSC_CALIB, print_=print_, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("Ring oscillators calibration failed!") from e

	#reading the results
	json_file = _get_latest_results(procedure=RINGOSC_CALIB, extension=".json")
	if json_file is not None:
		try:
			results = _read_json_data(json_file)
		except RuntimeError:
			results = {}

	#copying ROOT file
	if output_path_root is not None:
		try:
			_copy_latest_results(procedure=RINGOSC_CALIB,
				destination=output_path_root)
		except Exception as e:
			raise RuntimeError("Could not copy data to the required "
				"location!") from e

	#copying JSON file
	if output_path_json is not None:
		try:
			_copy_latest_results(procedure=RINGOSC_CALIB,
				destination=output_path_json, extension=".json")
			#shutil.copy(adc_calib_xml, output_path_xml)
		except Exception as e:
			raise RuntimeError("Could not copy file!") from e

	return results


def temperature_calibration(wpac_port, temp_sens_id = 4, adc_samples = 10,
		timeout = 70, output_path_json = None):
	"""
	Runs the temperature calibration routine.

	Parameters
	----------
	wpac_port : str or path-like
		Device file used to communicate with the WPAC.
	temp_sens_id : int, optional
		ID of the temperature sensor to use for the calibration.
	adc_samples : int, optional
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.
	output_path_json : str, optional
			If not Nones, the file containing the test results is copied in  
			the required location.

	Returns
	-------
	dict
		Dictionary with the results.
	"""

	#getting the default values
	tool = _PH2_ACF_TOOLS[TEMP_CALIB]
	args = TOOLS_CONFIG[tool]["args"] #default configuration

	#checking input value
	temp_sens_ids = [1, 2, 3, 4]
	if temp_sens_id not in temp_sens_ids:
		raise ValueError(f"Invalid temperature sensor ID: {temp_sens_id}")

	#updating the default configuration
	args["wpacPort"] = wpac_port
	if temp_sens_id is not None:
		args["wpacTempSens"] = temp_sens_id
	if adc_samples is not None:
		args["adcSamples"] = adc_samples

	#setting the DAQ configuration in the config file
	config = _load_toml(_TOOLS_CONFIG_FILE)
	config[tool]["args"].update(args)
	_dump_toml(config, _TOOLS_CONFIG_FILE)

	#running the test
	try:
		_run(procedure=TEMP_CALIB, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("An error occurred during temperature calibration!") from e

	#reading the results
	json_file = _get_latest_results(procedure=TEMP_CALIB, extension=".json")
	if json_file is not None:
		try:
			results = _read_json_data(json_file)
		except RuntimeError:
			results = {}

	#copying JSON file
	if output_path_json is not None:
		try:
			_copy_latest_results(procedure=TEMP_CALIB,
				destination=output_path_json, extension=".json")
		except Exception as e:
			raise RuntimeError("Could not copy file!") from e

	return results


def read_temperature(timeout = 6):
	"""
	Reads the on-chip temperature sensors.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.

	Returns
	-------
	dict
		Dictionary with the results.
	"""

	#running the test
	try:
		stdout = _run(procedure=TEMP_MEAS, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("An error occurred during temperature measurement!") from e

	#parsing the results
	try:
		results = _parse_stdout(stdout=stdout, procedure=TEMP_MEAS)
	except RuntimeError as e:
		raise RuntimeError("Could not parse the results of the threshold "
			"scan!") from e

	return results


def reset(n_tries = 3):
	"""
	Sends the reset command to the DAQ board.

	Parameters
	----------
	n_tries : int, optional
		How many times the reset procedure should be tried.

	Raises
	------
	RuntimeError
		If the reset procedure fails for `n_tries` times.
	"""

	#try a fixed number of times to reset the DAQ board
	is_reset_complete = False
	for _ in range(n_tries):
		#reset the DAQ board
		try:
			_run("-r")
			is_reset_complete = True
			break
		except DAQException:
			continue

	#if all tries failed, raise an exception
	if not is_reset_complete:
		raise RuntimeError(f"Could not reset the DAQ board after {n_tries} "
			"tries!") from None

	#wait for the chip to stabilize
	time.sleep(0.5)


def enable_core_columns():
	"""
	Enables the clock to the pixel matrix by setting the `EnCoreCol` 
	registers.

	Raises
	------
	RuntimeError
		If the procedure fails.
	"""

	#get TOML configuration
	config = _load_toml(_TOML_CONFIG_FILE)
	try:
		config["CoreColumns"]["disable"] = []
		config["CoreColumns"]["disableInjections"] = []
	except KeyError:
		config["CoreColumns"] = {}
		config["CoreColumns"]["disable"] = []
		config["CoreColumns"]["disableInjections"] = []
	_dump_toml(config, _TOML_CONFIG_FILE)

	#running the DAQ
	try:
		_run(procedure=PROGRAMMING)
	except DAQException as e:
		raise RuntimeError("Failed to enable pixel matrix!") from e

	#waiting for the power to stabilize
	time.sleep(1.5)


def disable_core_columns():
	"""
	Disables the clock to the pixel matrix by setting the `EnCoreCol` 
	registers.

	Raises
	------
	RuntimeError
		If the procedure fails.
	"""

	#number of core columns in the chip
	n_core_cols = CHIP_DATA[CHIP_TYPE]["core_cols"]

	#get TOML configuration
	config = _load_toml(_TOML_CONFIG_FILE)
	columns_list = [[0, n_core_cols - 1]]
	try:
		config["CoreColumns"]["disable"] = columns_list
		config["CoreColumns"]["disableInjections"] = columns_list
	except KeyError:
		config["CoreColumns"] = {}
		config["CoreColumns"]["disable"] = columns_list
		config["CoreColumns"]["disableInjections"] = columns_list
	_dump_toml(config, _TOML_CONFIG_FILE)

	#running the DAQ
	try:
		_run(procedure=PROGRAMMING)
	except DAQException as e:
		raise RuntimeError("Failed to disable pixel matrix!") from e

	#waiting for the power to stabilize
	time.sleep(1.5)


def set_low_power_analog():
	"""
	Sets the analog front-end of the chip to the low power configuration.

	Raises
	------
	RuntimeError
		If the procedure fails.
	"""

	#setting the values for the chip registers related to analog power
	registers = {
		"DAC_PREAMP_M_LIN": 0,
		"DAC_PREAMP_R_LIN": 0,
		"DAC_PREAMP_L_LIN": 0,
		"DAC_PREAMP_T_LIN": 0,
		"DAC_PREAMP_TL_LIN": 0,
		"DAC_PREAMP_TR_LIN": 0,
		"DAC_GDAC_M_LIN": 0,
		"DAC_GDAC_R_LIN": 0,
		"DAC_GDAC_L_LIN": 0,
		"DAC_LDAC_LIN": 0,
		"DAC_FC_LIN": 0,
		"DAC_COMP_LIN": 0,
		"DAC_COMP_TA_LIN": 0,
		"DAC_KRUM_CURR_LIN": 0,
		"DAC_REF_KRUM_LIN": 0
	}

	try:
		write_registers(registers)
	except RuntimeError as e:
		raise RuntimeError("Could not set a low analog power configuration "
		"chip!") from e


def set_standard_power_analog():
	"""
	Sets the analog front-end of the chip to the standard power configuration.

	Raises
	------
	RuntimeError
		If the procedure fails.
	"""

	#getting the default values for the chip registers related to analog power
	registers = {
		"DAC_PREAMP_M_LIN": CHIP_CONFIG["Registers"]["DAC_PREAMP_M_LIN"],
		"DAC_PREAMP_R_LIN": CHIP_CONFIG["Registers"]["DAC_PREAMP_R_LIN"],
		"DAC_PREAMP_L_LIN": CHIP_CONFIG["Registers"]["DAC_PREAMP_L_LIN"],
		"DAC_PREAMP_T_LIN": CHIP_CONFIG["Registers"]["DAC_PREAMP_T_LIN"],
		"DAC_PREAMP_TL_LIN": CHIP_CONFIG["Registers"]["DAC_PREAMP_TL_LIN"],
		"DAC_PREAMP_TR_LIN": CHIP_CONFIG["Registers"]["DAC_PREAMP_TR_LIN"],
		"DAC_GDAC_M_LIN": CHIP_CONFIG["Registers"]["DAC_GDAC_M_LIN"],
		"DAC_GDAC_R_LIN": CHIP_CONFIG["Registers"]["DAC_GDAC_R_LIN"],
		"DAC_GDAC_L_LIN": CHIP_CONFIG["Registers"]["DAC_GDAC_L_LIN"],
		"DAC_LDAC_LIN": CHIP_CONFIG["Registers"]["DAC_LDAC_LIN"],
		"DAC_FC_LIN": CHIP_CONFIG["Registers"]["DAC_FC_LIN"],
		"DAC_COMP_LIN": CHIP_CONFIG["Registers"]["DAC_COMP_LIN"],
		"DAC_COMP_TA_LIN": CHIP_CONFIG["Registers"]["DAC_COMP_TA_LIN"],
		"DAC_KRUM_CURR_LIN": CHIP_CONFIG["Registers"]["DAC_KRUM_CURR_LIN"],
		"DAC_REF_KRUM_LIN": CHIP_CONFIG["Registers"]["DAC_REF_KRUM_LIN"]
	}

	try:
		write_registers(registers)
	except RuntimeError as e:
		raise RuntimeError("Could not set the standard analog power "
		"configuration for the chip!") from e


def set_low_chip_power():
	"""
	Sets the chip to the low power configuration.

	Raises
	------
	RuntimeError
		If the procedure fails.
	"""

	#setting the values for the chip registers related to analog power
	registers = {
		"DAC_PREAMP_M_LIN": 0,
		"DAC_PREAMP_R_LIN": 0,
		"DAC_PREAMP_L_LIN": 0,
		"DAC_PREAMP_T_LIN": 0,
		"DAC_PREAMP_TL_LIN": 0,
		"DAC_PREAMP_TR_LIN": 0,
		"DAC_GDAC_M_LIN": 0,
		"DAC_GDAC_R_LIN": 0,
		"DAC_GDAC_L_LIN": 0,
		"DAC_LDAC_LIN": 0,
		"DAC_FC_LIN": 0,
		"DAC_COMP_LIN": 0,
		"DAC_COMP_TA_LIN": 0,
		"DAC_KRUM_CURR_LIN": 0,
		"DAC_REF_KRUM_LIN": 0
	}

	#setting the values for the chip registers related to digital power
	n_core_cols = CHIP_DATA[CHIP_TYPE]["core_cols"]
	config = _load_toml(_TOML_CONFIG_FILE)
	columns_list = [[0, n_core_cols - 1]]
	try:
		config["CoreColumns"]["disable"] = columns_list
		config["CoreColumns"]["disableInjections"] = columns_list
	except KeyError:
		config["CoreColumns"] = {}
		config["CoreColumns"]["disable"] = columns_list
		config["CoreColumns"]["disableInjections"] = columns_list
	_dump_toml(config, _TOML_CONFIG_FILE)

	#running the configuration
	try:
		write_registers(registers)
	except RuntimeError as e:
		raise RuntimeError("Could not set the low power configuration for the "
		"chip!") from e

	#waiting for the chip to stabilize
	time.sleep(0.5)


def set_standard_chip_power():
	"""
	Sets the chip to the standard power configuration.

	Raises
	------
	RuntimeError
		If the procedure fails.
	"""

	#setting the default values for the chip registers related to analog power
	registers = {
		"DAC_PREAMP_M_LIN": CHIP_CONFIG["Registers"]["DAC_PREAMP_M_LIN"],
		"DAC_PREAMP_R_LIN": CHIP_CONFIG["Registers"]["DAC_PREAMP_R_LIN"],
		"DAC_PREAMP_L_LIN": CHIP_CONFIG["Registers"]["DAC_PREAMP_L_LIN"],
		"DAC_PREAMP_T_LIN": CHIP_CONFIG["Registers"]["DAC_PREAMP_T_LIN"],
		"DAC_PREAMP_TL_LIN": CHIP_CONFIG["Registers"]["DAC_PREAMP_TL_LIN"],
		"DAC_PREAMP_TR_LIN": CHIP_CONFIG["Registers"]["DAC_PREAMP_TR_LIN"],
		"DAC_GDAC_M_LIN": CHIP_CONFIG["Registers"]["DAC_GDAC_M_LIN"],
		"DAC_GDAC_R_LIN": CHIP_CONFIG["Registers"]["DAC_GDAC_R_LIN"],
		"DAC_GDAC_L_LIN": CHIP_CONFIG["Registers"]["DAC_GDAC_L_LIN"],
		"DAC_LDAC_LIN": CHIP_CONFIG["Registers"]["DAC_LDAC_LIN"],
		"DAC_FC_LIN": CHIP_CONFIG["Registers"]["DAC_FC_LIN"],
		"DAC_COMP_LIN": CHIP_CONFIG["Registers"]["DAC_COMP_LIN"],
		"DAC_COMP_TA_LIN": CHIP_CONFIG["Registers"]["DAC_COMP_TA_LIN"],
		"DAC_KRUM_CURR_LIN": CHIP_CONFIG["Registers"]["DAC_KRUM_CURR_LIN"],
		"DAC_REF_KRUM_LIN": CHIP_CONFIG["Registers"]["DAC_REF_KRUM_LIN"]
	}

	#setting the default values for the chip registers related to digital power
	config = _load_toml(_TOML_CONFIG_FILE)
	try:
		config["CoreColumns"]["disable"] = []
		config["CoreColumns"]["disableInjections"] = []
	except KeyError:
		config["CoreColumns"] = {}
		config["CoreColumns"]["disable"] = []
		config["CoreColumns"]["disableInjections"] = []
	_dump_toml(config, _TOML_CONFIG_FILE)

	#running the configuration
	try:
		write_registers(registers)
	except RuntimeError as e:
		raise RuntimeError("Could not set the default power configuration for "
		"the chip!") from e

	#waiting for the chip to stabilize
	time.sleep(0.5)


def _set_lane(lane):
	"""
	Sets the lane to be used by the chip for communication.
	"""

	tree = _load_xml(_XML_CONFIG_FILE)
	croc_xpath = "/HwDescription/BeBoard/OpticalGroup/Hybrid/CROC"
	croc_elem = tree.xpath(croc_xpath)[0]
	croc_elem.attrib["Lane"] = str(lane)
	_dump_xml_from_tree(tree, _XML_CONFIG_FILE)


def lanes_test(n_tries = 3, timeout = 5):
	"""
	Tests the communication on all four chip lanes.

	Parameters
	----------
	n_tries : int
		Number of tries to perform for each lane.
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.

	Returns
	-------
	tuple of int
		Tuple with the number of failed sequences and the total number of 
		failed tries.

	Raises
	------
	ValueError
		If invalid parameter types are detected.
	RuntimeError
		If the test fails.
	"""

	if isinstance(n_tries, int) is False:
		raise ValueError("n_tries must be an integer!")

	#test data
	lanes = [3, 2, 1, 0] #lanes to test
	results = { #testing results to return at the end
		"n_tries": n_tries,
		"tested_lanes": lanes,
		"lanes": {}
	}

	for lane in lanes:
		results["lanes"][str(lane)] = {}
		for i in range(n_tries):
			#setting the XML configuration
			_set_lane(lane)

			#testing the communication
			is_test_ok = False
			try:
				n_failed_seq, n_failed_tries = communication_test(
					timeout=timeout)
				is_test_ok = True
			except RuntimeError:
				is_test_ok = False
				n_failed_seq = None
				n_failed_tries = None

			results["lanes"][str(lane)][f"try_{i}"] = {}
			results["lanes"][str(lane)][f"try_{i}"]["success"] = is_test_ok
			results["lanes"][str(lane)][f"try_{i}"]["n_failed_daq_sequences"] = n_failed_seq
			results["lanes"][str(lane)][f"try_{i}"]["n_failed_daq_tries"] = n_failed_tries

	#go back to the default value
	#_set_lane(0)

	return results


def data_merging_test(timeout = 3):
	"""
	Performs the data merging test.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.

	Raises
	------
	RuntimeError
		If something goes wrong during the test.
	"""

	#running the test
	try:
		stdout = _run(procedure=DATA_MERGING, timeout=timeout)
	except (DAQError, DAQTimeout) as e:
		raise RuntimeError("An error occurred during the data merging "
			"test!") from e

	#getting the result
	results = {}
	try:
		results = _parse_stdout(stdout, DATA_MERGING)
	except RuntimeError as e:
		raise RuntimeError("Failed to parse the results of the test!") from e

	#waiting for the chip to stabilize
	time.sleep(1)

	return results


def chip_id_test(timeout = 10, extra_reads = 9, wpac_port = None,
		output_path_json = None):
	"""
	Performs a CHIP_ID test with the CMS DAQ.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.
	extra_reads : int, optional
		Number of extra read procedures to perform.
	wpac_port : str or path-like
		Device file used to communicate with the WPAC.

	Returns
	-------
	dict
		Dictionary with the results.

	Raises
	------
	RuntimeError
		If something goes wrong during the test.
	"""

	#getting the default values
	tool = _PH2_ACF_TOOLS[CHIP_ID]
	args = TOOLS_CONFIG[tool]["args"] #default configuration

	#updating the default configuration with the parameters that are not None
	if extra_reads is not None:
		args["extraReads"] = extra_reads
	if wpac_port is not None:
		args["wpacPort"] = wpac_port

	#setting the DAQ configuration in the config file
	config = _load_toml(_TOOLS_CONFIG_FILE)
	config[tool]["args"].update(args)
	_dump_toml(config, _TOOLS_CONFIG_FILE)

	#running the test
	try:
		_run(procedure=CHIP_ID, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("An error occurred during the CHIP_ID test!") from e

	#reading the results
	json_file = _get_latest_results(procedure=CHIP_ID, extension=".json")
	with open(json_file, "r") as json_fdesc:
		try:
			results = json.load(json_fdesc)
		except Exception:
			results = {}

	#copying JSON file
	if output_path_json is not None:
		try:
			_copy_latest_results(procedure=CHIP_ID,
				destination=output_path_json, extension=".json")
		except Exception as e:
			raise RuntimeError("Could not copy file!") from e

	return results


def write_efuses(value, wpac_port, timeout = 10):
	"""
	Burns the efuses on the chip, writing `value`.

	Parameters
	----------
	value : int
		Value to write in the chip efuses. Must be writable in a 32 bit 
		register.
	wpac_port : str or path-like
		Device file used to communicate with the WPAC.
	Returns
	-------
	dict
		Dictionary with the results.

	Raises
	------
	ValueError
		If `value` needs more than 32 bits to be written or is otherwise 
		invalid.
	"""

	#size of the efuses register
	regsize = 32

	#getting the default values
	tool = _PH2_ACF_TOOLS[EFUSE_PROG]
	args = TOOLS_CONFIG[tool]["args"] #default configuration

	#checking input value
	if (value < 0) or (value > 2 ** regsize - 1):
		raise ValueError(f"Cannot write {value:#x} to efuses!")

	#updating the value to be written
	args["word"] = value

	#updating the default configuration with the parameters that are not None
	if wpac_port is not None:
		args["wpacPort"] = wpac_port

	#setting the DAQ configuration in the config file
	config = _load_toml(_TOOLS_CONFIG_FILE)
	config[tool]["args"].update(args)
	_dump_toml(config, _TOOLS_CONFIG_FILE)

	#running the test
	try:
		stdout = _run(procedure=EFUSE_PROG, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("An error occurred during efuse programming!") from e

	#parsing the results
	results = {}
	try:
		results = _parse_stdout(stdout, EFUSE_PROG)
	except RuntimeError as e:
		raise RuntimeError("Failed to parse the results of the test!") from e

	return results


def probe_monitoring_mux(gnda1 = None, gnda2 = None, gndd = None,
		type_ = "normal", timeout = 15):
	"""
	Measures chip signals using the internal voltage and current multiplexers.

	Parameters
	----------
	type_ : str
		Type of monitoring to perform. Can be either 'normal' or 'iv'.
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.

	Returns
	-------
	dict
		Dictionary with the results.

	Raises
	------
	RuntimeError
	"""
	
	#checking the measurement type
	procedure = None
	if type_ == "normal":
		procedure = MON_MUX
	elif type_ == "iv":
		procedure = IV_MON_MUX

	#getting the default values
	tool = _PH2_ACF_TOOLS[procedure]
	args = TOOLS_CONFIG[tool]["args"] #default configuration

	#updating the default configuration with the parameters that are not None
	if gnda1 is not None:
		args["GNDA_REF1"] = gnda1
	if gnda2 is not None:
		args["GNDA_REF2"] = gnda2
	if gndd is not None:
		args["GNDD_REF"] = gndd

	#setting the DAQ configuration in the config file
	config = _load_toml(_TOOLS_CONFIG_FILE)
	config[tool]["args"].update(args)
	_dump_toml(config, _TOOLS_CONFIG_FILE)

	#running process
	try:
		stdout = _run(procedure=procedure, timeout=timeout)
	except DAQException as e:
		raise RuntimeError("Failed to measure the signals from the chip internal multiplexer!") from e

	#parsing the standard output of the executable
	try:
		results = _parse_stdout(stdout, MON_MUX)
	except RuntimeError as e:
		raise RuntimeError("Failed to parse the results of the test!") from e

	return results

def scan_chain_test(timeout = 120):
	"""
	Performs the scan chain test.

	Parameters
	----------
	timeout : int or float, optional
		Elapsed time before the DAQ executable is terminated.

	Raises
	------
	RuntimeError
		If something goes wrong during the test.
	"""

	#running the test
	try:
		stdout = _run(procedure=SCAN_CHAIN, timeout=timeout)
	except (DAQError, DAQTimeout) as e:
		raise RuntimeError("An error occurred during the scan chain "
			"test!") from e

	#getting the result
	results = {}
	try:
		results = _parse_stdout(stdout, SCAN_CHAIN)
	except RuntimeError as e:
		raise RuntimeError("Failed to parse the results of the test!") from e

	#waiting for the chip to stabilize
	#time.sleep(5)

	return results
