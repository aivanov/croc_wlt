"""
Subpackage with definitions related to the Devices Under Test (DUTs) for 
wafer-level testing.

Modules:
	chips: contains chip-related constant

	wafer: contains the wafer maps and additional wafer-related data
"""

import os
import csv
from dataclasses import dataclass
import importlib.resources as pkg_resources


#Python data class used to store information about wafers, such as the foundry
#batch or the identifier
@dataclass
class Wafer:
	"""Dataclass used for storing wafer data."""

	#wafer attributes
	#the wafer map should also be added here
	chip_type: str
	foundry_batch: str
	foundry_batch_number: int
	foundry_id: str
	waferprobing_id: int


#file containing wafer data (batch, serial and waferprobing identifier)
WAFER_DATA_FILE = "wafers.csv"

#list of all wafers
WAFERS = []

#reading the CSV
#note: the use of open_text will be deprecated in Python 3.11
with pkg_resources.open_text(__package__, WAFER_DATA_FILE) as csv_file:
	csv_data = csv.DictReader(csv_file, delimiter=";", skipinitialspace=True)
	for row in csv_data:
		wafer = Wafer(
			chip_type=row["Chip type"],
			foundry_batch=row["Foundry batch"],
			foundry_batch_number=row["Foundry batch number"],
			foundry_id=row["Foundry identifier"],
			waferprobing_id=row["Waferprobing identifier"]
		)
		WAFERS.append(wafer)


def get_wafer(batch, id_):
	"""
	Returns the Wafer object from the selected batch and with the required 
	id.

	Parameters
	----------
	batch, id_ : str
		Foundry batch and ID for the wafer.

	Returns
	-------
	Wafer or None
		Wafer data class or None if no matching wafers have been found.
	"""

	#get the wafer with the required batch and id; if no wafer matches
	try:
		wafer = [wafer for wafer in WAFERS if wafer.foundry_batch == batch and
			wafer.foundry_id == id_][0]
	except IndexError:
		wafer = None

	return wafer


def get_waferprobing_id(batch, id_):
	"""
	Returns the waferprobing ID of the wafer with the requested batch and id.

	Parameters
	----------
	batch, id_ : str
		Foundry batch and ID for the wafer.

	Returns
	-------
	int or None
		The waferprobing ID or None if no matching wafers have been found.
	"""

	#getting the waferprobing ID from the CSV data and converting it to an
	#integer before returning it
	wafer = get_wafer(batch, id_)
	return int(wafer.waferprobing_id, 16) if wafer is not None else None
