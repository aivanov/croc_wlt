"""
Module which contains definitions related to the RD53 chips.
"""

#constants that define chip types
ITKPIX_CHIP = "ITkPix"
CROC_CHIP = "CROCv1"

#constant list with the two RD53B flavours. Useful for checking if a test 
#should be run on a specific DUT
RD53B_CHIPS = [CROC_CHIP, ITKPIX_CHIP]

#constant list of valid chip types
CHIPS = [ITKPIX_CHIP, CROC_CHIP]

#dictionary containing data related to the chips, such as the number of
#pixel/core columns and rows
CHIP_DATA = {
	ITKPIX_CHIP: {
		"pixel_cols": 400,
		"pixel_rows": 384,
		"pixel_count": 153600,
		"core_cols": 50,
		"core_rows": 48,
		"core_count": 2400
	},
	CROC_CHIP: {
		"pixel_cols": 432,
		"pixel_rows": 336,
		"pixel_count": 145152,
		"core_cols": 54,
		"core_rows": 42,
		"core_count": 2268
	}
}
