"""
Module used for interfacing with serial instruments employed for WLT.

The instruments are controlled using SCPI commands via serial communications.

The module has been developed and tested with a TTi QL355TP and Keithley
2400/2401.
"""

import re
import time
import serial

#SCPI commands are sent as ASCII strings
ENCODING = "ascii"


#function used to parse instrument replies
def _parse(input_string, expression_string):
	"""
	Private function used to parse the answers of the instrument.

	Parameters
	----------
	input_string : str
		String which has to be searched with a particular regex.
	expression_string : str
		Regular expression which has to be used on input_string. It must 
		contain one regex group named "target" for the _parse method to 
		work.

	Returns
	-------
	str
		The matching string contained in the "target" regex group.

	Raises
	------
	RuntimeError
		When the parsing of the instrument answer fails.
	"""

	match = re.search(expression_string, input_string)

	if match is None:
		raise RuntimeError("Parsing of instrument answer failed. Could not "
			f"parse '{input_string}' with regular expression "
			f"'{expression_string}'")

	return match.group("target")


class Instrument:
	"""Base class from which the other power supply classes inherit."""

	#list of valid channels
	channels = None

	def __init__(
		self,
		device_file,
		baud_rate,
		device_name = "",
		device_model = "",
		write_termination = "\n",
		read_termination = "\n",
		xonxoff = 0,
		timeout = 1 ):

		self.device_name = device_name
		self.device_model = device_model
		self.device_file = device_file
		self.baud_rate = baud_rate
		self.write_termination = write_termination
		self.read_termination = read_termination
		self.xonxoff = xonxoff
		self.timeout = timeout
		self.connection = None

	def open(self):
		"""
		Opens the device file for the instrument with the configured baud
		rate, timeout and flow control.

		Raises
		------
		IOError
			If the port has already been opened or if a serial exception is 
			catched while trying to initialize the communication.
		ValueError
			When a wrong communication parameter has been passed and the 
			initialization of the device cannot proceed.
		"""

		#raise exception if the port is already open
		if self.is_open():
			raise IOError("Port already open!")

		try:
			self.connection = serial.Serial(
				self.device_file,
				self.baud_rate,
				timeout=self.timeout,
				xonxoff=self.xonxoff,
				write_timeout=5,
				exclusive=True
			)

		except serial.SerialException as e:
			self.connection = None
			raise IOError(f"Error while trying to open {self.device_file}") from e

		except ValueError as e:
			self.connection = None
			raise ValueError("Wrong parameter(s) for initializing the "
				"communication") from e

	def write(self, message):
		"""
		Low-level method to send a generic command to the instrument.

		Parameters
		----------
		message : str
			Command to send to the instrument, without the termination 
			character.

		Raises
		------
		IOError
			When the port has not been opened before calling this method.
		"""

		if not self.is_open():
			raise IOError(f"Port is not open, cannot write command: {message}")

		#add write termination
		term_message = f"{message}{self.write_termination}"

		#encode the terminated message using ASCII
		#TO DO: check for exceptions
		enc_message = term_message.encode(ENCODING)

		#write the ASCII-encoded command to the instrument
		self.connection.write(enc_message)

	def read(self):
		"""
		Low-level method to read a reply of the instrument.

		Returns
		-------
		str
			The decoded output of the instrument, without XON/XOFF characters 
			or the termination character.

		Raises
		------
		IOError
			When the port has not been opened before calling this method.
		"""

		if not self.is_open():
			raise IOError("Port is not open, cannot perform a reading")

		#encode the termination character using ASCII
		enc_termination = self.read_termination.encode(ENCODING)

		#read the message from the instrument using the encoded termination
		message = self.connection.read_until(enc_termination)

		#decode the instrument message from ASCII
		decoded_message = message.decode(ENCODING)

		#remove XON/XOFF characters, the termination character and return
		return decoded_message.strip("\x11\x13" + self.read_termination)

	def query(self, message):
		"""
		Low-level method to perform a query operation (write + read).

		Parameters
		----------
		message : str
			Command to send to the instrument, without the termination 
			character.

		Returns
		-------
		str
			The decoded output of the instrument, without XON/XOFF characters 
			or the termination character.

		Raises
		------
		IOError
			When the port has not been opened before calling this method.
		"""

		if not self.is_open():
			raise IOError(f"Port is not open, cannot perform query: {message}")

		self.write(message)
		return self.read()

	def is_open(self):
		"""
		Method to check whether the device file has been opened or not.

		Returns
		-------
		bool
			True if the device file for the instrument has been opened, False 
			otherwise.
		"""

		#first check if the connection attribute is None
		if self.connection is None:
			return False

		#return the status
		return self.connection.is_open

	def is_channel(self, channel):
		"""
		Method to check whether a string represents a valid channel name.

		Parameters
		----------
		channel : str
			The string with the channel name to be checked.

		Returns
		-------
		bool
			True if the channel parameter is a valid channel name for the 
			instrument, False otherwise.
		"""

		#list of valid channel names. Check all spellings
		channel_names = [val for sublist in self.channels for val in sublist]

		#check if the channel name is in the previous list
		is_channel_valid = (channel.upper() in channel_names)
		return is_channel_valid

	def _check_channel(self, channel):
		"""
		Private function for checking a channel name and raising an exception
		if the name is not valid.

		Parameters
		----------
		channel : str
			The string with the channel name to be checked.

		Raises
		------
		ValueError
			When the given string does not represent a valid instrument channel.
		"""

		if not self.is_channel(channel):
			raise ValueError(f"Invalid channel name: {channel}")

	def close(self):
		"""Closes the device file for the instrument."""

		if self.is_open():
			self.connection.close()
			self.connection = None


class Keithley(Instrument):
	"""
	Class used to interface with Keithley source-meters.

	It has been developed and tested using Keithley 2400/2401 source-meters.
	"""

	#list of valid channel names. Each entry is itself a list in order to allow
	#for variations
	channels = [["FRON", "FRONT"], ["REAR"]]

	def query_idn(self):
		"""
		Queries the instrument for the identification string.

		Returns
		-------
		str
			The identification string of the instrument.
		"""

		return self.query("*IDN?")

	def query_esr(self):
		"""
		Queries and clears the Event Status Register.

		Returns
		-------
		str
			The value of the Event Status Register.
		"""

		return self.query("*ESR?")

	def query_error_queue(self):
		"""
		Queries the instrument for the Error Queue. The register is cleared
		afterwards.

		Returns
		-------
		str
			The error queue of the instrument.
		"""

		return self.query("SYST:ERR:ALL?")

	def set_channel(self, channel):
		"""Sets the active channel of the source-meter."""

		self._check_channel(channel)
		self.write(f"ROUT:TERM {channel}")

	def query_channel(self):
		"""
		Queries the instrument for the active channel.

		Returns
		-------
		str
			The active channel of the instrument.
		"""

		return self.query("ROUT:TERM?")

	def query_output(self):
		"""
		Queries the instrument for the status of the output for the active
		channel.

		Returns
		-------
		str
			The status of the output of the instrument.
		"""

		return self.query("OUTP?")

	def clear_status(self):
		"""
		Clears various instrument registers.

		The Standard Event Register, Operation Event Register, Measurement Event
		Register and Questionable Event Register are cleared.
		"""

		self.write("*CLS")

	def enable_output(self):
		"""Turns on the output of the instrument."""

		self.write("OUTP ON")

	def disable_output(self):
		"""Turns off the output of the instrument."""

		self.write("OUTP OFF")

	def read_output(self):
		"""
		Triggers and acquires readings from the instrument.

		The command performs an INITiate and then a FETCh. The INITiate command
		triggers a measurement cycle and stores the data in the Sample Buffer.
		This buffer is then read by sending the FETCh command.

		Returns
		-------
		str
			The results of the measurement.
		"""

		return self.query("READ?")

	def enable_beeper(self):
		"""Enables the instrument beeper."""

		self.write("SYST:BEEP:STAT 1")

	def disable_beeper(self):
		"""Disables the instrument beeper."""

		self.write("SYST:BEEP:STAT 0")

	def reset(self):
		"""
		Resets the instrument to the default settings, except for the remote 
		interface settings.

		After the reset, a small delay is set (100 ms) in order to wait for the 
		operation to complete.
		"""

		self.write("*RST")
		time.sleep(0.1)


class TTi(Instrument):
	"""
	Class used to interface with TTi power supplies of the QL series.

	It has been developed and tested considering a TTi QL355TP power supply.
	"""

	#list of valid channel names. Each entry is itself a list in order to allow
	#for variations. "3" is the auxiliary channel
	channels = [["1"], ["2"], ["3"]]

	#structure of the Limit Event Status Registers
	_lsr_dict = {
		#Limit Event Status Register 1
		"1": {
			"voltage_limit_ch1": {
				"channel": "1",
				"value": 2**0
			},
			"current_limit_ch1": {
				"channel": "1",
				"value": 2**1
			},
			"overvoltage_trip_ch1": {
				"channel": "1",
				"value": 2**2
			},
			"overcurrent_trip_ch1": {
				"channel": "1",
				"value": 2**3
			},
			"thermal_trip_ch1": {
				"channel": "1",
				"value": 2**4
			},
			"sense_trip_ch1": {
				"channel": "1",
				"value": 2**5
			},
		},
		#Limit Event Status Register 2
		"2": {
			"voltage_limit_ch2": {
				"channel": "2",
				"value": 2**0
			},
			"current_limit_ch2": {
				"channel": "2",
				"value": 2**1
			},
			"overvoltage_trip_ch2": {
				"channel": "2",
				"value": 2**2
			},
			"overcurrent_trip_ch2": {
				"channel": "2",
				"value": 2**3
			},
			"thermal_trip_ch2": {
				"channel": "2",
				"value": 2**4
			},
			"sense_trip_ch2": {
				"channel": "2",
				"value": 2**5
			},
			"current_limit_ch3": {
				"channel": "3",
				"value": 2**6
			}
		}
	}

	#mapping of channels to LSR1/LSR2
	_lsr_channel_map = {"1": "1", "2": "2", "3": "2"}

	def query_idn(self):
		"""
		Queries the instrument for the identification string.

		Returns
		-------
		str
			The identification string of the instrument.
		"""

		return self.query("*IDN?")

	def query_esr(self):
		"""
		Queries and clears the Event Status Register.

		Returns
		-------
		str
			The value of the Event Status Register.
		"""

		return self.query("*ESR?")

	def query_eer(self):
		"""
		Queries the instrument for the Execution Error Register. The register 
		is cleared afterwards.

		Returns
		-------
		str
			The value of the Execution Error Register.
		"""

		return self.query("EER?")

	def query_qer(self):
		"""
		Queries the instrument for the Query Error Register. The register is
		cleared afterwards.

		Returns
		-------
		str
			The value of the Query Error Register.
		"""

		return self.query("QER?")

	def query_lse(self, channel):
		"""
		Queries the instrument for the Limit Status Event Enable Register.
		The register is cleared afterwards.

		Parameters
		----------
		channel : str
			The name of the channel for which the LSE must be queried. It can 
			only be queried for channels 1 and 2.

		Returns
		-------
		str
			The value of the LSE register.

		Raises
		------
		ValueError
			When trying to query the LSE for the auxiliary output.
		"""

		self._check_channel(channel)

		if channel == "3":
			raise ValueError("The LSE register cannot be queried for auxiliary "
			"output")

		return self.query(f"LSE{channel}?")

	def query_lsr(self, index):
		"""
		Queries the instrument for the Limit Event Status Register. The
		register is cleared afterwards.

		Parameters
		----------
		index : str
			The index parameter can be 1 or 2, given that there are only two 
			Limit Event Status Registers.

		Returns
		-------
		int
			The value of the Limit Event Status Register 1 or 2.

		Raises
		------
		ValueError
			In case the index parameter is incorrect.
		RuntimeError
			If something goes wrong during the reading.
		"""

		#checking the index type
		try:
			int_index = int(index)
		except ValueError:
			raise ValueError("Invalid value for index parameter. Valid values: "
				"1, 2") from None

		#check the index
		if int_index not in [1, 2]:
			raise ValueError(f"Invalid index for LSR register: {int_index}")

		#query the status
		status_lsr = self.query(f"LSR{int_index}?")

		try:
			status_lsr_int = int(status_lsr)
		except ValueError as e:
			raise RuntimeError("Something went wrong while reading the status "
				"of the LSR register!") from e

		return status_lsr_int

	def get_trip_status(self, channel):
		"""
		Checks whether any trip event occurred for the provided channel.

		The Limit Event Status Register (LSR) for the relevant channel is
		queried and the answer is translated to human-readable format and
		returned.

		Parameters
		----------
		channel : str
			The name of the channel for which the trip status must be checked.

		Returns
		-------
		dict
			Dictionary containing the trip status. The keys are the trip names 
			and the values are booleans (True for occurred trip).
		"""

		self._check_channel(channel)

		#getting the index of the LSR associated to the channel
		index = TTi._lsr_channel_map[channel]

		#query the relevant LSR
		register_value = self.query_lsr(index)

		#get all register fields
		all_fields = TTi._lsr_dict[index]

		#select only the ones related to the selected channel
		channel_fields = [
			field
			for field in all_fields
			if all_fields[field]["channel"] == channel
		]

		#select only trips for the given channel
		channel_trip_fields = [
			field
			for field in channel_fields
			if "trip" in field
		]

		#status dictionary
		status_dict = dict()

		#loop on all fields
		for field in channel_trip_fields:
			#remove the channel suffix
			field_stripped = field.replace("_ch" + str(channel), "")

			#get the status as a bool
			bit_value = all_fields[field]["value"]
			bit_status = (register_value & bit_value) / bit_value
			bit_status_bool = bool(bit_status)

			#update the status dictionary
			status_dict[field_stripped] = bit_status_bool

		return dict(status_dict)

	def reset_trip(self):
		"""Tries to clear the trip condition from all outputs."""

		#remove the trip condition
		self.write("TRIPRST")

		#clear the LSR1 and LSR2 by reading them
		self.query_lsr("1")
		self.query_lsr("2")

	def set_voltage(self, channel, voltage):
		"""
		Sets the voltage limit for the selected channel.

		When this value is reached, the instrument switches to constant voltage
		mode.

		Parameters
		----------
		channel : str
			The name of the channel for which the voltage limit must be set.
		voltage : float or int
			Voltage limit to set on the chosen channel.
		"""

		self._check_channel(channel)
		self.write(f"V{channel} {voltage}")

	def query_voltage(self, channel):
		"""
		Queries the voltage limit for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the voltage limit must be queried.

		Returns
		-------
		float
			The set voltage limit for the given channel.
		"""

		self._check_channel(channel)
		answer = self.query(f"V{channel}?")
		regex = fr"V{channel} (?P<target>\d+.\d+)"
		voltage = _parse(answer, regex)

		return float(voltage)

	def query_output_voltage(self, channel):
		"""
		Queries the output voltage for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the output voltage must be 
			queried.

		Returns
		-------
		float
			The output voltage for the given channel.
		"""

		self._check_channel(channel)
		answer = self.query(f"V{channel}O?")
		regex = r"(?P<target>\d+.\d+)V"
		voltage = _parse(answer, regex)

		return float(voltage)

	def set_ovp(self, channel, ovp):
		"""
		Sets the overvoltage protection for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the OVP must be set.
		ovp : float or int
			The overvoltage value to set.
		"""

		self._check_channel(channel)
		self.write(f"OVP{channel} {ovp}")

	def query_ovp(self, channel):
		"""
		Queries the overvoltage protection setting for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the OVP must be queried.
		"""

		self._check_channel(channel)
		answer = self.query(f"OVP{channel}?")
		regex = r"VP[12] (?P<target>\d+.\d+)"
		voltage = _parse(answer, regex)

		return float(voltage)

	def set_current(self, channel, current):
		"""
		Sets the current limit for the selected channel.

		When this value is reached, the instrument switches to constant current
		mode.

		Parameters
		----------
		channel : str
			The name of the channel for which the current limit must be set.
		current: float or int
			The current limit to set.

		Raises
		------
		ValueError
			If trying to set the current of the auxiliary output.
		"""

		self._check_channel(channel)

		if channel == "3":
			raise ValueError("Cannot set current for auxiliary output")

		self.write(f"I{channel} {current}")

	def query_current(self, channel):
		"""
		Queries the current limit for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the current limit must be queried.

		Returns
		-------
		float
			The set current limit for the given channel.

		Raises
		------
		ValueError
			If trying to query the current of the auxiliary output.
		"""

		self._check_channel(channel)

		if channel == "3":
			raise ValueError("Cannot query current for auxiliary output")

		answer = self.query(f"I{channel}?")
		regex = fr"I{channel} (?P<target>\d+.\d+)"
		current = _parse(answer, regex)

		return float(current)

	def query_output_current(self, channel):
		"""
		Queries the output voltage for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the output current must be 
			queried.

		Returns
		-------
		float
			The output current for the given channel.
		"""

		self._check_channel(channel)

		answer = self.query(f"I{channel}O?")
		regex = r"(?P<target>\d+.\d+)A"
		current = _parse(answer, regex)

		return float(current)

	def set_ocp(self, channel, ocp):
		"""
		Sets the overcurrent protection for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the OCP must be set.
		ocp : float or int
			The overcurrent value to set.
		"""

		self._check_channel(channel)
		self.write(f"OCP{channel} {ocp}")

	def query_ocp(self, channel):
		"""
		Queries the overcurrent protection setting for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel for which the OCP must be queried.
		"""

		self._check_channel(channel)

		answer = self.query(f"OCP{channel}?")
		regex = r"CP[12] (?P<target>\d+.\d+)"
		voltage = _parse(answer, regex)

		return float(voltage)

	def enable_output(self, channel):
		"""
		Sets the output of the selected channel on.

		Parameters
		----------
		channel : str
			The name of the channel.
		"""

		self._check_channel(channel)
		self.write(f"OP{channel} 1")

	def disable_output(self, channel):
		"""
		Sets the output of the selected channel off.

		Parameters
		----------
		channel : str
			The name of the channel.
		"""

		self._check_channel(channel)
		self.write(f"OP{channel} 0")

	def query_output(self, channel):
		"""
		Queries the status of the output for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel.

		Returns
		-------
		str
			The status of the selected channel: '0' if the channel is off and 
			'1' if it is on.
		"""

		self._check_channel(channel)
		answer = self.query(f"OP{channel}?")

		return answer

	def toggle_output(self, channel):
		"""
		Toggles the status of the output for the selected channel.

		Parameters
		----------
		channel : str
			The name of the channel.
		"""

		self._check_channel(channel)

		current_status_string = self.query_output(channel)
		is_on = (current_status_string == "1")

		if is_on:
			self.disable_output(channel)
		else:
			self.enable_output(channel)

	def set_range(self, channel, range_):
		"""
		Sets the voltage range for the selected output.

		The valid range values are:
			0: 15 V (5 A)
			1: 35 V (3 A)
			2: 35 V (500 mA)

		Parameters
		----------
		channel : str
			The name of the channel.
		range_ : int
			The voltage range to set. The possible values are: 0, 1, 2.

		Raises
		------
		ValueError
			If the range_ parameter is not one of 0, 1, 2.
		"""

		self._check_channel(channel)

		if range_ not in range(0, 3):
			raise ValueError("Wrong value supplied for the voltage range: "
			f"{range_}")

		self.write(f"RANGE{channel} {range_}")

	def query_range(self, channel):
		"""
		Gets the voltage range for the selected output.

		The valid range values are:
			0: 15 V (5 A)
			1: 35 V (3 A)
			2: 35 V (500 mA)
		"""

		self._check_channel(channel)

		answer = self.query(f"RANGE{channel}?")
		regex = fr"R{channel} (?P<target>\d)"
		range_val = _parse(answer, regex)

		return int(range_val)

	def enable_all_outputs(self):
		"""Sets the output of all instrument channels on."""

		self.write("OPALL 1")

	def disable_all_outputs(self):
		"""Sets the output of all instrument channels off."""

		self.write("OPALL 0")

	def clear_status(self):
		"""
		Clears various instrument registers.

		The Standard Event Status Register, Query Error Register and Execution
		Error Register are cleared. This also clears the Status Byte Register
		indirectly.
		"""

		self.write("*CLS")

	def set_link_mode(self, mode):
		"""
		Sets the link mode for the instrument.

		The valid values for link mode can be 0 (linked), 1 (control to channel
		1) or 2 (control to channel 2).

		Parameters
		----------
		mode : int
			Value to set for link mode.

		Raises
		------
		TypeError
			If the 'mode' parameter is not an integer.
		ValueError
			If 'mode' has a value that is not one of: [0, 1, 2].
		"""

		#checking type of `mode` parameter
		if not isinstance(mode, int):
			raise TypeError("The 'mode' parameter must be an integer!")

		#checking the value of the `mode` parameter
		if mode not in [0, 1, 2]:
			raise ValueError("Invalid value for parameter 'mode'!")

		self.write(f"MODE {mode}")

	def set_sense_mode(self, channel, status):
		"""
		Sets the sensing mode of the power supply.

		Parameters
		----------
		channel : str
			The name of the power supply channel to configure.
		status : int
			The status of the sense mode to apply. Valid values are 0 for 
			setting local sensing and 1 for remote sensing.

		Raises
		------
		TypeError
			If the `status` parameter is not an integer.
		ValueError
			If the `status` is neither 0 nor 1.
		"""

		#checking the `channel` parameter. Reraise the `ValueError` exception
		#raised by the private method `_check_channel`
		try:
			self._check_channel(channel)
		except ValueError:
			raise

		#checking the type of the `status` parameter
		if isinstance(status, int) is False:
			raise TypeError("The status parameter must be an integer!")

		#checking the value of the `status` parameter
		if status not in [0, 1]:
			raise ValueError(f"Invalid value for status parameter: {status}." 
				"Must be either 0 or 1")

		self.write(f"SENSE{channel} {status}")

	def reset(self):
		"""
		Resets the instrument to the default settings, except for the remote 
		interface settings.

		After the reset, a small delay is set (500 ms) in order to wait for the 
		operation to complete.
		"""

		self.write("*RST")
		time.sleep(0.5)
