#!/usr/bin/env python3

import wlt.hardware.instruments as instr

SEPARATOR = "-" * 96

#initializing the instrument
keithley = instr.utils.get_instrument("METER")
keithley.open()

print(SEPARATOR)
print( "Identification:", keithley.query_idn() )
print(SEPARATOR)
print( "Event status register:", keithley.query_esr() )
print( "Error queue:", keithley.query_error_queue() )
print("Status byte:", keithley.query("*STB?"))
print( "Selected channel:", keithley.query("ROUT:TERM?") )
print(SEPARATOR)

keithley.close()
