#!/usr/bin/env python3

import wlt.hardware.instruments as instr

SEPARATOR = "-" * 70


def print_status(tti):
	print(SEPARATOR)
	print("Identification:", tti.query_idn())
	print(SEPARATOR)
	print("Event status register:", tti.query_esr())
	print("Execution error register:", tti.query_eer())
	print("Query error register:", tti.query_qer())
	print(SEPARATOR)
	print(
		"Set voltage [V]:"
		f"\t{tti.query_voltage('1')}"
		f"\t{tti.query_voltage('2')}"
		f"\t{tti.query_voltage('3')}"
	)
	print(
		"Set current [A]:"
		f"\t{tti.query_current('1')}"
		f"\t{tti.query_current('2')}"
	)
	print(SEPARATOR)
	print(
		"Output voltage [V]:"
		f"\t{tti.query_output_voltage('1')}"
		f"\t{tti.query_output_voltage('2')}"
		f"\t{tti.query_output_voltage('3')}"
	)
	print(
		"Output current [A]:"
		f"\t{tti.query_output_current('1')}"
		f"\t{tti.query_output_current('2')}"
	)
	print(SEPARATOR)
	print(f"Range:\t\t\t{tti.query_range('1')}\t{tti.query_range('2')}")
	print(SEPARATOR)
	print(f"OVP [V]:\t\t{tti.query_ovp('1')}\t{tti.query_ovp('2')}")
	print(f"OCP [A]:\t\t{tti.query_ocp('1')}\t{tti.query_ocp('2')}")
	print(SEPARATOR)


def print_status_short(tti):
	print(SEPARATOR)
	print(
		"Set voltage [V]:"
		f"\t{tti.query_voltage('1')}"
		f"\t{tti.query_voltage('2')}"
		f"\t{tti.query_voltage('3')}"
	)
	print(
		"Set current [A]:"
		f"\t{tti.query_current('1')}"
		f"\t{tti.query_current('2')}"
	)
	print(
		"Output voltage [V]:"
		f"\t{tti.query_output_voltage('1')}"
		f"\t{tti.query_output_voltage('2')}"
		f"\t{tti.query_output_voltage('3')}"
		)
	print(
		"Output current [A]:"
		f"\t{tti.query_output_current('1')}"
		f"\t{tti.query_output_current('2')}"
	)
	print(SEPARATOR)


if __name__ == "__main__":
	#get instrument from instruments.utils
	tti = instr.utils.get_instrument("PC")
	tti.open()
	print_status(tti)
	tti.close()
