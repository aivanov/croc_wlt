"""
Module used for interfacing with the instruments used for wafer probing.

This module manages the configuration of the instruments, the powering of the
chip and the electrical measurements (e.g., current drawn by chip under test).
"""

import time
import copy

from wlt.config import main_config
from wlt.hardware.instruments import instrument

DEVICES = dict()
SIGNALS = copy.deepcopy(main_config.SIGNALS)

#TO DO:
# - add method for resetting all the instruments
# - use the DEVICES global variable for getting the instruments

#constants and variable to store the configuration of the SMU
VOLTAGE_MEASUREMENT = 1
CURRENT_MEASUREMENT = 2
smu_measurement_type = None


class TripError(Exception):
	"""Exception raised in case of a trip condition."""

	pass


def _init_devices():
	"""
	Loads the settings from the Python configuration module and initializes 
	the device objects.

	Raises
	------
	ValueError
		Exception raised in case of an invalid device vendor.
	"""

	#getting the settings
	settings = copy.deepcopy(main_config.DEVICES)

	for device_key in settings.keys():
		#pop the device vendor
		device_vendor = settings[device_key].pop("device_vendor")

		#check for invalid device vendor. The valid values can be put in the
		#__init__.py of the instruments module
		if device_vendor not in ["TTi", "Keithley"]:
			raise ValueError(f"Unrecognized device vendor: {device_vendor}")

		#initializing the device object
		if device_vendor == "TTi":
			device_object = instrument.TTi(**settings[device_key])
		elif device_vendor == "Keithley":
			device_object = instrument.Keithley(**settings[device_key])

		#add the object to the DEVICES dictionary
		DEVICES[device_key] = device_object


def _check_wiring():
	"""
	Check that the power supplies are wired correctly for the testing.

	VIN and VDD must be on the same power supply.

	Raises
	------
	RuntimeError
		If the read configuration has VIN or VDD signals on different power 
		supplies.
	"""

	#get the power supplies
	vina_power_supply = SIGNALS["VINA"]["power_supply"]
	vind_power_supply = SIGNALS["VIND"]["power_supply"]
	vdda_power_supply = SIGNALS["VDDA"]["power_supply"]
	vddd_power_supply = SIGNALS["VDDD"]["power_supply"]

	#booleans to check whether the power supplies are different
	is_vin_ps_diff = (vina_power_supply != vind_power_supply)
	is_vdd_ps_diff = (vdda_power_supply != vddd_power_supply)

	if is_vin_ps_diff or is_vdd_ps_diff:
		raise RuntimeError("Wrong wiring for power supplies detected from "
			"software configuration. VIN and VDD must be on dedicated power "
			"supplies.")


def open_instruments():
	"""
	Reads the configuration and opens the connection to the instruments.

	Raises
	------
	RuntimeError
		Exception raised if unable to open a device file for an instrument.
	"""

	#initialize the communication
	for instr_key, instr in DEVICES.items():
		try:
			instr.open()
		except Exception:
			raise RuntimeError(f"Failed to open device {instr_key}") from None


def close_instruments():
	"""Closes the connection to the instruments."""

	for instr in DEVICES.values():
		instr.close()


def get_instrument(name):
	"""
	Returns the instrument with the given name from the available devices.

	Parameters
	----------
	name : str
		Name of the instrument to obtain as defined in the Python configuration 
		module.

	Raises
	------
	ValueError
		Raised if a non-existent device name is passed as parameter.

	Returns
	-------
	Instrument
		Returns the Instrument object with the associated name in the Python 
		configuration module.
	"""

	device = None

	try:
		device = DEVICES[name]
	except KeyError:
		raise ValueError(f"No device named {name} in initialized devices") from None

	return device


def get_instrument_from_signal(signal):
	"""
	Returns the Instrument object associated to the given signal and the 
	channel at which it can be found.

	Parameters
	----------
	signal : str
		Name of the signal to obtain, as defined in the Python configuration 
		module.

	Returns
	-------
	Instrument, str
		Returns the Instrument object and the channel name.
	"""

	try:
		power_supply_name = SIGNALS[signal]["power_supply"]
		channel = SIGNALS[signal]["channel"]
		power_supply = DEVICES[power_supply_name]
		return power_supply, channel
	except Exception:
		return (None, None)


def setup_power_supplies():
	"""
	Sets up the power supplies in the wafer-level testing hardware.

	Both the direct powering instrument and the S/LDO one are reset and
	reconfigured with an initial, default configuration, even if different 
	from the one in the configuration file.
	"""

	#get the instruments
	instr_vin, _ = get_instrument_from_signal("VINA")
	instr_vdd, _ = get_instrument_from_signal("VDDA")

	#reset them
	instr_vin.reset()
	instr_vdd.reset()

	#set the lowest range possible for both power supplies. Range 0 means that
	#the voltage can go up to 15 V and the current up to 5 A
	instr_vin.set_range("1", 0)
	instr_vin.set_range("2", 0)
	instr_vdd.set_range("1", 0)
	instr_vdd.set_range("2", 0)

	#configure voltages, currents and limits for both power supplies
	set_ldo_mode(vin=1.6, iin=2, ovp=2.1, ocp=2.1)
	set_direct_mode(vdda=1.35, vddd=1.35, iin=4, ovp=1.6, ocp=4.1)


def setup_meter():
	"""
	Sets up the source-meter unit in the wafer-level testing hardware.

	The instrument is configured to perform voltage measurements and the beeper 
	is disabled.

	Raises
	------
	RuntimeError
		If the setting up of the instrument fails.
	"""

	meter = DEVICES["METER"]
	try:
		meter.reset()
		meter.disable_beeper()
		setup_voltage_measurement()
	except Exception as e:
		raise RuntimeError("Could not setup the source-meter!") from e


def setup_instruments():
	"""
	Setup the instruments before using them.

	Performs a reset and an initial default configuration of the power supplies 
	and of the SMU.

	Raises
	------
	RuntimeError
		If the setting up of the instruments fails.
	"""

	#configure TTi
	try:
		setup_power_supplies()
	except RuntimeError:
		raise

	#configure Keithley
	try:
		setup_meter()
	except RuntimeError:
		raise


def check_instruments():
	"""
	Checks that the serial communication with the instruments works.

	Raises
	------
	RuntimeError
		If an instrument doesn't answer with the identification string.
	"""

	for key, device in DEVICES.items():
		#checking failed write/read
		try:
			idn_string = device.query_idn()
		except Exception as e:
			raise RuntimeError(f"Cannot communicate with device {key}!") from e

		#checking for empty identification string
		if idn_string == "":
			raise RuntimeError(f"Empty identification string from device {key}!")


def enable_chip_power():
	"""
	Enables power to the chip (VINA/D and VDDA/D).

	The selection between VINA/D and VDDA/D is performed by the Power Board, 
	so both power supplies can be turned on. MUST NOT BE USED WITHOUT THE 
	POWER BOARD!

	Due to a Power Board bug, VDD must also be turned on if VIN is on, otherwise 
	a current of approximately 10-20 mA flows from the VIN power supply to the 
	VDD one.

	The VINA/D signals and the VDDA/D ones **must** be on the same power 
	supply for this method to work, as it is safer to enable and disable all 
	power supply outputs together, given that there are no delays from one 
	channel to the other.

	Raises
	------
	RuntimeError
		When a wrong wiring of the power supplies, preventing the usage of this 
		method, is detected.
	"""

	#check the wiring before doing anything. VINA and VIND must be provided from
	#the same power supply in order for this method to work!
	try:
		_check_wiring()
	except RuntimeError as e:
		raise e

	#get the needed power supply and turn the chip on by enabling both power
	#supply outputs simultaneously (VINA/D)
	power_supply_vin, _ = get_instrument_from_signal("VINA")
	power_supply_vin.enable_all_outputs()

	#due to a power board bug, VDD must also be turned on, otherwise a current
	#of approximately 10-20 mA flows from the VIN power supply to the VDD one
	power_supply_vdd, _ = get_instrument_from_signal("VDDA")
	power_supply_vdd.enable_all_outputs()

	#waiting for the output to stabilise
	time.sleep(0.5)


def disable_chip_power():
	"""
	Disables power to the chip (VINA/D **and** VDDA/D).

	For safety reasons, the power-off command is sent to both power supplies 
	(VIN and VDD).

	The VINA/D signals and the VDDA/D ones **must** be on the same power 
	supply for this method to work, as it is safer to enable and disable all 
	power supply outputs together, given that there are no delays from one 
	channel to the other.

	Raises
	------
	RuntimeError
		When a wrong wiring of the power supplies, preventing the usage of this 
		method, is detected.
	"""

	#check the wiring before doing anything. VINA and VIND must be provided from
	#the same power supply in order for this method to work!
	try:
		_check_wiring()
	except RuntimeError as e:
		raise e from None

	#get the needed power supply
	power_supply_vin, _ = get_instrument_from_signal("VINA")
	power_supply_vdd, _ = get_instrument_from_signal("VDDA")

	#turn the chip off by disabling both power supply outputs simultaneously
	power_supply_vin.disable_all_outputs()
	power_supply_vdd.disable_all_outputs()

	#waiting for the output to stabilise
	time.sleep(0.5)


def measure_chip_power_vin():
	"""
	Measures power drawn by chip in S/LDO using 4-wire sensing.

	Returns
	-------
	dict
		Dictionary containing voltage and current measurements for both power 
		domains.
	"""

	instr_ana, channel_ana = get_instrument_from_signal("VINA")
	instr_dig, channel_dig = get_instrument_from_signal("VIND")

	#measurements
	v_ana = instr_ana.query_output_voltage(channel_ana)
	v_dig = instr_dig.query_output_voltage(channel_dig)
	i_ana = instr_ana.query_output_current(channel_ana)
	i_dig = instr_dig.query_output_current(channel_dig)

	#results dictionary
	chip_power = {
		"VINA": v_ana,
		"IINA": i_ana,
		"VIND": v_dig,
		"IIND": i_dig
	}

	return chip_power


def measure_chip_power_vdd():
	"""
	Measures power drawn by chip in direct powering using 4-wire sensing.

	Returns
	-------
	dict
		Dictionary containing voltage and current measurements for both power 
		domains.
	"""

	instr_ana, channel_ana = get_instrument_from_signal("VDDA")
	instr_dig, channel_dig = get_instrument_from_signal("VDDD")

	#measurements
	v_ana = instr_ana.query_output_voltage(channel_ana)
	v_dig = instr_dig.query_output_voltage(channel_dig)
	i_ana = instr_ana.query_output_current(channel_ana)
	i_dig = instr_dig.query_output_current(channel_dig)

	#results dictionary
	chip_power = {
		"VDDA": v_ana,
		"IINA": i_ana,
		"VDDD": v_dig,
		"IIND": i_dig
	}

	return chip_power


def setup_voltage_measurement():
	"""Sets up the SMU in order to acquire voltage readings."""

	#TO DO: remove hard-coded values
	#TO DO: sanitize input values

	#needed in order to edit the global variable
	global smu_measurement_type

	#getting the device
	meter = DEVICES["METER"]

	#set measurement speed and number of digits
	meter.write("SENS:VOLT:NPLC 1")
	meter.write("DISP:DIG 6")

	#setting up source mode
	#TODO: Move to dedicated function
	meter.write("SOUR:FUNC CURR")
	meter.write("SOUR:CURR:MODE FIXED")
	meter.write("SOUR:CURR:RANG MIN")
	meter.write("SOUR:CURR:LEV 0")

	#setting up meter mode
	meter.write("SENS:FUNC:OFF:ALL")
	meter.write("SENS:FUNC:ON \"VOLT\"")
	meter.write("FORM:ELEM VOLT")

	#safety
	meter.write("SENS:VOLT:PROT 3")
	meter.write("SENS:VOLT:RANG 3")

	#setting the measurement type
	smu_measurement_type = VOLTAGE_MEASUREMENT


def get_voltage_measurement():
	"""
	Uses the source-meter to perform a voltage measurement from the probe 
	card LEMO connector.

	Returns
	-------
	float
		The voltage (in volts) measured with the source-meter unit.
	"""

	#getting the device
	meter = DEVICES["METER"]

	#check that the instrument is configured for getting a voltage measurement
	if smu_measurement_type != VOLTAGE_MEASUREMENT:
		setup_voltage_measurement()

	#getting a measurement
	meter.enable_output()
	meas_str = meter.read_output()
	meter.disable_output()

	#trying to typecast the answer
	try:
		meas = float(meas_str)
	except ValueError:
		meas = None

	return meas


def get_current_measurement(reference_voltage = 0):
	"""
	Uses the source-meter to perform a current measurement from the probe 
	card LEMO connector.

	Parameters
	----------
	reference_voltage : float, optional
		The voltage (measured in volts) to apply to the node from which the 
		current is extracted.

	Returns
	-------
	float
		The current (in amperes) measured with the source-meter unit.
	"""

	#TO DO: sanitize input values
	#TO DO: remove hard-coded values

	#getting the device
	meter = DEVICES["METER"]

	#setting up source mode
	meter.write("SOUR:FUNC VOLT")
	meter.write("SOUR:VOLT:MODE FIXED")
	meter.write("SOUR:VOLT:RANG 2")
	meter.write(f"SOUR:VOLT:LEV {reference_voltage:1.2f}")

	#setting up meter mode
	meter.write("SENS:FUNC:OFF:ALL")
	meter.write("SENS:FUNC:ON \"CURR\"")
	meter.write("FORM:ELEM CURR")

	#safety: these go to separate commands
	meter.write("SENS:CURR:RANG 200E-6")
	meter.write("SENS:CURR:PROT 200E-6")

	#turning on output
	meter.enable_output()

	#measuring
	meas = meter.read_output()

	#turning off output
	meter.disable_output()

	return float(meas)


def set_ldo_mode(vin = 1.7, iin = 3, ovp = 2, ocp = 3):
	"""
	Programs the power supply in order to go to LDO mode.

	The configuration is the same for both the analog and the digital channels.

	Parameters
	----------
	vin : float, optional
		Voltage limit for LDO mode in volts.
	iin : float, optional
		Current limit for LDO mode in amperes.
	ovp : float, optional
		Overvoltage protection for LDO mode in volts.
	ocp : float, optional
		Overcurrent protection for LDO mode in amperes.

	Raises
	------
	ValueError
		If the vin parameter is not in the (0, 2] V interval.
	"""

	#checking VIN
	is_vin_in_range = (0 < vin <= 2)
	if not is_vin_in_range:
		raise ValueError("Invalid value for VIN! Must be in (0, 2] V.")

	for signal in ["VINA", "VIND"]:
		power_supply, channel = get_instrument_from_signal(signal)

		#setting all remaining parameters
		power_supply.set_voltage(channel, vin)
		power_supply.set_current(channel, iin)
		power_supply.set_ovp(channel, ovp)
		power_supply.set_ocp(channel, ocp)
	time.sleep(0.5)


def set_ldo_voltage(vin = 1.6):
	"""
	Method used to set the voltage limit of the chip during IV curves in 
	LDO powering mode.

	Parameters
	----------
	vin : float or int, optional
		The voltage limit to set.	

	Raises
	------
	ValueError
		If VIN is not in the valid range.
	"""

	#check LDO voltage VIN
	is_vin_in_range = (0 < vin <= 2)
	if not is_vin_in_range:
		raise ValueError("Invalid LDO voltage! Must be in (0, 2] V")

	#setting the voltage limit
	power_supply, channel_ana = get_instrument_from_signal("VINA")

	#configuring the instrument with link mode
	power_supply.set_link_mode(0)
	power_supply.set_voltage(channel_ana, vin)
	power_supply.set_link_mode(1)

	#let the chip power stabilize
	time.sleep(0.1)


def set_shunt_mode(iin = 0.6, vin = 2, ovp = 2.1, ocp = 1.1):
	"""
	Programs the power supply in order to go to shunt mode.

	The configuration is the same for both the analog and the digital channels.

	Parameters
	----------
	iin : float or int, optional
		Shunt current used to power the chip, measured in amperes. Must be in 
		the (0, 3] A range.
	vin : float or int, optional
		Voltage limit, measured in volts. Must be in (0, 3] V.
	ovp : float or int, optional
		Overvoltage limit in volts.
	ocp : float or int, optional
		Overcurrent limit in amperes.

	Raises
	------
	ValueError
		If IIN or VIN are not in the valid range.
	"""

	#booleans to check IIN and VIN
	is_vin_in_range = (0 < vin <= 4)
	is_iin_in_range = (0 < iin <= 4.6)

	#check shunt current IIN
	if not is_iin_in_range:
		raise ValueError("Invalid shunt current! Must be in (0, 4.1] A")

	#check voltage limit VIN
	if not is_vin_in_range:
		raise ValueError("VIN not in range! Must be in (0, 4] V")

	for signal in ["VINA", "VIND"]:
		#getting the power supply and the channel
		power_supply, channel = get_instrument_from_signal(signal)

		#set the remaining parameters
		power_supply.set_current(channel, iin)
		power_supply.set_voltage(channel, vin)
		power_supply.set_ovp(channel, ovp)
		power_supply.set_ocp(channel, ocp)

	#let the chip power stabilize
	time.sleep(0.8)


def set_shunt_current(iin = 0.6):
	"""
	Method used to set the current limit of the chip during IV curves in 
	shunt LDO powering mode.

	Parameters
	----------
	iin : float or int, optional
		The current limit to set.

	Raises
	------
	ValueError
		If IIN is not in the valid range.
	"""

	#check shunt current IIN
	is_iin_in_range = (0 < iin <= 4.6)
	if not is_iin_in_range:
		raise ValueError("Invalid shunt current! Must be in (0, 4.1] A")

	#setting the current limit
	power_supply, channel_ana = get_instrument_from_signal("VINA")
	#_, channel_dig = get_instrument_from_signal("VIND")

	#enabling linking
	power_supply.set_link_mode(0)

	power_supply.set_current(channel_ana, iin)
	#power_supply_dig.set_current(channel_dig, iin)

	#disabling linking
	power_supply.set_link_mode(1)

	#let the chip power stabilize
	time.sleep(0.8)


def set_direct_mode(vdda = 1.2, vddd = 1.2, iin = 2, ovp = 1.32, ocp = 2.5):
	"""
	Configures the power supply for direct powering.

	Parameters
	----------
	vdda, vddd : float, optional
		The values of the VDDA and VDDD voltages.
	iin : int or float, optional
		Current limit for each channel.
	ovp, ocp : int or float, optional
		Overvoltage/overcurrent protection values for each channel.

	Raises
	------
	ValueError
		If one or more of the given values of VDD is incorrect.
	RuntimeError
		If the procedure fails.
	"""

	#checking that VDDA is in range (values a bit above 1,32 V are
	#permitted given that sensing could be disabled)
	if (vdda < 0.95) or (vdda > 1.5):
		raise ValueError("Invalid value for VDDA voltage!")

	#checking that VDDD is in range (values a bit above 1,32 V are
	#permitted given that sensing could be disabled)
	if (vddd < 0.95) or (vddd > 1.5):
		raise ValueError("Invalid value for VDDD voltage!")

	#TODO: check also `iin`, `ovp` and `ocp`

	#check the wiring before doing anything. VINA and VIND must be provided from
	#the same power supply in order for this method to work!
	try:
		_check_wiring()
	except RuntimeError as e:
		raise e

	#getting the power supply and the channels
	power_supply, channel_ana = get_instrument_from_signal("VDDA")
	_, channel_dig = get_instrument_from_signal("VDDD")

	#setting VDD
	power_supply.set_voltage(channel_ana, vdda)
	power_supply.set_voltage(channel_dig, vddd)

	#setting IIN
	power_supply.set_current(channel_ana, iin)
	power_supply.set_current(channel_dig, iin)

	#setting the OVP
	power_supply.set_ovp(channel_ana, ovp)
	power_supply.set_ovp(channel_dig, ovp)

	#setting the OCP
	power_supply.set_ocp(channel_ana, ocp)
	power_supply.set_ocp(channel_dig, ocp)


def set_vdd(vdda = 1.2, vddd = 1.2, do_reset = False):
	"""
	Configures the values of VDDA and VDDD when in direct powering.

	Parameters
	----------
	vdda, vddd : float, optional
		The values of the VDDA and VDDD voltages.
	do_reset : bool, optional
		If True, performs a power cycle while setting the VDD.

	Raises
	------
	ValueError
		If one or more of the given values of VDD is incorrect.
	RuntimeError
		If the procedure fails.
	"""

	#checking that VDDA is in range
	if (vdda < 0.6) or (vdda >= 1.32):
		raise ValueError("Invalid value for VDDA voltage!")

	#checking that VDDD is in range
	if (vddd < 0.6) or (vddd >= 1.32):
		raise ValueError("Invalid value for VDDD voltage!")

	#getting the power supply and the channels
	power_supply, channel_ana = get_instrument_from_signal("VDDA")
	_, channel_dig = get_instrument_from_signal("VDDD")

	#powering off the chip
	if do_reset is True:
		disable_chip_power()

	#setting VDD
	power_supply.set_voltage(channel_ana, vdda)
	power_supply.set_voltage(channel_dig, vddd)

	#turning chip back on
	if do_reset is True:
		enable_chip_power()


def check_chip_trip():
	"""
	Checks whether a trip condition has occurred on the chip power supply.

	Raises
	------
	TripError
		Raised in case a device trip is detected.
	"""

	instr_ana, channel_ana = get_instrument_from_signal("VINA")
	instr_dig, channel_dig = get_instrument_from_signal("VIND")

	#status dictionaries for trips
	status_ana = instr_ana.get_trip_status(channel_ana)
	status_dig = instr_dig.get_trip_status(channel_dig)

	#occurred trips
	trips_ana = [key for (key, val) in status_ana.items() if val is True]
	trips_dig = [key for (key, val) in status_dig.items() if val is True]

	#bools for analog and digital power
	is_tripped_ana = (len(trips_ana) != 0)
	is_tripped_dig = (len(trips_dig) != 0)

	#global bool for instrument
	is_tripped = (is_tripped_ana or is_tripped_dig)

	#raise exception if a trip condition has been detected
	if is_tripped:
		message = "A chip trip condition has been detected!"
		if is_tripped_ana:
			message += f" Analog power: {trips_ana}"
		if is_tripped_dig:
			message += f" Digital power: {trips_dig}"
		raise TripError(message)


def reset_chip_trip():
	"""Resets the trip condition for the chip power supply/supplies."""

	instr_ana, _ = get_instrument_from_signal("VINA")
	instr_dig, _ = get_instrument_from_signal("VIND")

	instr_ana.reset_trip()
	instr_dig.reset_trip()


#read the settings and fill the DEVICES dictionary when the module is loaded
_init_devices()
