"""
Module which defines the different probe cards used in wafer-level testing.
"""

from wlt.duts.chips import ITKPIX_CHIP
from wlt.duts.chips import CROC_CHIP

#dictionary describing the first version of the CROC probe card
CROC_PROBE_CARD_V1P0 = {
	#list of chip types that can be probed with this probe card
	"chip_types": [ITKPIX_CHIP, CROC_CHIP],
	#list of valid I2C addresses
	"i2c_addr": [0x20, 0x21, 0x22],
	#size of probe card registers
	"i2c_reg_size": 8,
	#mapping of the multiplexer on the probe card
	#due to the multiplexer bug on the CROC probe card v1.0, VOFS_LP must be
	#used as a ground
	"mux": {
		"VINA_SNS": {
			"address": 0x0,
			"ground": "VOFS_LP"
		},
		"VIND_SNS": {
			"address": 0x1,
			"ground": "VOFS_LP"
		},
		"VDDA": {
			"address": 0x2,
			"ground": "VOFS_LP"
		},
		"VDDD": {
			"address": 0x3,
			"ground": "VOFS_LP"
		},
		"VDD_CML": {
			"address": 0x4,
			"ground": "VOFS_LP"
		},
		"VDD_PLL": {
			"address": 0x5,
			"ground": "VOFS_LP"
		},
		"VDD_PRE": {
			"address": 0x6,
			"ground": "VOFS_LP"
		},
		"IMUX_OUT": {
			"address": 0x7,
			"ground": "VOFS_LP"
		},
		"VMUX_OUT": {
			"address": 0x8,
			"ground": "VOFS_LP"
		},
		"R_IREF": {
			"address": 0x9,
			"ground": "VOFS_LP"
		},
		"NTC": {
			"address": 0xA,
			"ground": "VOFS_LP"
		},
		"VREF_ADC": {
			"address": 0xB,
			"ground": "VOFS_LP"
		},
		"VOFS": {
			"address": 0xC,
			"ground": "VOFS_LP"
		}, #34.9 kohm resistor to GND
		"VOFS_LP": {
			"address": 0xD,
			"ground": "GND"
		},
		"REXTA": {
			"address": 0xE,
			"ground": "VOFS_LP"
		},
		"REXTD": {
			"address": 0xF,
			"ground": "VOFS_LP"
		},
		#mux channels from 16 to 31 are affected by the multiplexer bug, so they
		#cannot be read on the CROC probe card v1.0. As a precaution, they are
		#set to VOFS_LP
		"VREFA": {
			"address": 0xD,
			"ground": "VOFS_LP"
		},
		"VREFD": {
			"address": 0xD,
			"ground": "VOFS_LP"
		},
		"VREF_OVP": {
			"address": 0xD,
			"ground": "VOFS_LP"
		},
		"GNDA_REF": {
			"address": 0xD,
			"ground": "GND"
		},
		"GNDD_REF": {
			"address": 0xD,
			"ground": "GND"
		},
		"GND": {
			"address": 0xD,
			"ground": "GND"
		},
		#virtual multiplexer outputs used to keep compatibility between probe
		#cards
		"GNDA_REF1": {
			"address": 0xD,
			"ground": "GND"
		},
		"GNDA_REF2": {
			"address": 0xD,
			"ground": "GND"
		},
		#the others are not connected
	},
	#default value to select when the multiplexer is not in use
	"default_mux": "VINA_SNS",
	#GPIO signals on probe card
	"gpio": {
		### 0x20 ###
		"EN_R_IREF": { #0b00000001
			"address": 0x20,
			"offset": 0,
			"size": 1,
			"default": 1
		},
		"EN_R_IMUX": { #0b00000010
			"address": 0x20,
			"offset": 1,
			"size": 1,
			"default": 1
		},
		"MUX_SEL": { #0b01111100
			"address": 0x20,
			"offset": 2,
			"size": 5,
			"default": 2
		},
		"EN_REXT": { #0b10000000
			"address": 0x20,
			"offset": 7,
			"size": 1,
			"default": 1
		},
		### 0x21 ###
		"CHIP_ID": { #0b00001111
			"address": 0x21,
			"offset": 0,
			"size": 4,
			"default": 0
		},
		"BYPASS": { #0b00010000
			"address": 0x21,
			"offset": 4,
			"size": 1,
			"default": 0
		},
		"TEST": { #0b00100000
			"address": 0x21,
			"offset": 5,
			"size": 1,
			"default": 0
		},
		"EN_VDD_SHUNT": { #0b01000000
			"address": 0x21,
			"offset": 6,
			"size": 1,
			"default": 0
		},
		"EN_VDD_EFUSE": { #0b10000000
			"address": 0x21,
			"offset": 7,
			"size": 1,
			"default": 0
		},
		### 0x22 ###
		"IREF_TRIM": { #0b00001111
			"address": 0x22,
			"offset": 0,
			"size": 4,
			"default": 7
		},
		"PLL_VCTRL_RST_B": { #0b00010000
			"address": 0x22,
			"offset": 4,
			"size": 1,
			"default": 1
		},
		"ES": { #0b11000000
			"address": 0x22,
			"offset": 6,
			"size": 2,
			"default": None
		}
	}
}

#CROC probe card v1.1
CROC_PROBE_CARD_V1P1 = {
	#list of chip types that can be probed with this probe card
	"chip_types": [ITKPIX_CHIP, CROC_CHIP],
	#list of valid I2C addresses
	"i2c_addr": [0x20, 0x21, 0x22, 0x24],
	#size of probe card registers
	"i2c_reg_size": 8,
	#mapping of the multiplexer on the probe card
	"mux": {
		"VINA_SNS": {
			"address": 0x0,
			"ground": "GNDA_REF2"
		},
		"VIND_SNS": {
			"address": 0x1,
			"ground": "GNDD_REF",
		},
		"VDDA": {
			"address": 0x2,
			"ground": "GNDA_REF2"
		},
		"VDDD": {
			"address": 0x3,
			"ground": "GNDD_REF"
		},
		"VDD_CML": {
			"address": 0x4,
			"ground": "GNDA_REF2"
		},
		"VDD_PLL": {
			"address": 0x5,
			"ground": "GNDA_REF2"
		},
		"VDD_PRE": {
			"address": 0x6,
			"ground": "GNDA_REF2"
		},
		"IMUX_OUT": {
			"address": 0x7,
			"ground": "GNDA_REF1"
		},
		"VMUX_OUT": {
			"address": 0x8,
			"ground": "GNDA_REF1"
		},
		"R_IREF": {
			"address": 0x9,
			"ground": "GNDA_REF1"
		},
		"NTC": {
			"address": 0xA,
			"ground": "GNDA_REF1"
		},
		"VREF_ADC": {
			"address": 0xB,
			"ground": "GNDA_REF1"
		},
		"VOFS": {
			"address": 0xC,
			"ground": "GNDA_REF2"
		}, #34.9 kohm resistor to GND
		"VOFS_LP": {
			"address": 0xD,
			"ground": "GNDA_REF2"
		},
		"REXTA": {
			"address": 0xE,
			"ground": "GNDA_REF2"
		},
		"REXTD": {
			"address": 0xF,
			"ground": "GNDD_REF"
		},
		"VREFA": {
			"address": 0x10,
			"ground": "GNDA_REF2"
		},
		"VREFD": {
			"address": 0x11,
			"ground": "GNDD_REF"
		},
		"VREF_OVP": {
			"address": 0x12,
			"ground": "GNDD_REF"
		},
		"GNDA_REF1": {
			"address": 0x13,
			"ground": "GND"
		},		
		"GNDD_REF": {
			"address": 0x14,
			"ground": "GND"
		},
		"GND": {
			"address": 0x15,
			"ground": "GND"
		},
		"VDD_EFUSE": {
			"address": 0x16,
			"ground": "GND"
		},
		"GNDA_REF2": {
			"address": 0x17,
			"ground": "GND"
		},
		"VDD_SHUNT": {
			"address": 0x18,
			"ground": "GNDD_REF"
		},
		"ES1": {
			"address": 0x19,
			"ground": "GND"
		},
		"ES2": {
			"address": 0x1A,
			"ground": "GND"
		},
		#multiplexer inputs that are not connected
		"NC28": {
			"address": 0x1B,
			"ground": "GND"
		},
		"VIND_PC": {
			"address": 0x1C,
			"ground": "GNDD_REF"
		},
		"NC30": {
			"address": 0x1D,
			"ground": "GND"
		},
		"VINA_PC": {
			"address": 0x1E,
			"ground": "GNDA_REF2"
		},
		"NC32": {
			"address": 0x1F,
			"ground": "GND"
		},
	},
	#default value to select when the multiplexer is not in use
	"default_mux": "VINA_SNS",
	"gpio": {
		### 0x20 ###
		"EN_R_IREF": { #0b00000001
			"address": 0x20,
			"offset": 0,
			"size": 1,
			"default": 1
		},
		"EN_R_IMUX": { #0b00000010
			"address": 0x20,
			"offset": 1,
			"size": 1,
			"default": 1
		},
		"MUX_SEL": { #0b01111100
			"address": 0x20,
			"offset": 2,
			"size": 5,
			"default": 2
		},
		"EN_REXT": { #0b10000000
			"address": 0x20,
			"offset": 7,
			"size": 1,
			"default": 1
		},
		### 0x21 ###
		"CHIP_ID": { #0b00001111
			"address": 0x21,
			"offset": 0,
			"size": 4,
			"default": 0
		},
		"BYPASS": { #0b00010000
			"address": 0x21,
			"offset": 4,
			"size": 1,
			"default": 0
		},
		"TEST": { #0b00100000
			"address": 0x21,
			"offset": 5,
			"size": 1,
			"default": 0
		},
		"EN_VDD_SHUNT": { #0b01000000
			"address": 0x21,
			"offset": 6,
			"size": 1,
			"default": 0
		},
		"EN_VDD_EFUSE": { #0b10000000
			"address": 0x21,
			"offset": 7,
			"size": 1,
			"default": 0
		},
		### 0x22 ###
		"IREF_TRIM": { #0b00001111
			"address": 0x22,
			"offset": 0,
			"size": 4,
			"default": 7
		},
		"PLL_VCTRL_RST_B": { #0b00010000
			"address": 0x22,
			"offset": 4,
			"size": 1,
			"default": 1
		},
		"SCAN": { #0b00100000
			"address": 0x22,
			"offset": 5,
			"size": 1,
			"default": 0
		},
		"ES": { #0b11000000
			"address": 0x22,
			"offset": 6,
			"size": 2,
			"default": None
		},
		### 0x24 ###
		"EN_MUX_ARDU": { #0b00000001
			"address": 0x24,
			"offset": 0,
			"size": 1,
			"default": 0
		},
		"EN_DAC1/2": { #0b00000010
			"address": 0x24,
			"offset": 1,
			"size": 1,
			"default": 0
		},
		"EN_DAC1/4": {  #0b00000100
			"address": 0x24,
			"offset": 2,
			"size": 1,
			"default": 0
		}
	}
}

#list of all available probe cards
PROBE_CARDS = [CROC_PROBE_CARD_V1P0, CROC_PROBE_CARD_V1P1]
