"""
Module which contains libraries for managing probe stations.

Currently, only the Cascade Microtech CM300xi is supported.
"""


class ProberError(Exception):
	"""Exception to raise when the probe station returns a non-zero exit code."""
