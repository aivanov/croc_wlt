"""
Module which contains the class used for interacting with the CM300xi probe
station.

It relies on the Gpib package provided by Linux GPIB.
"""

import re

try:
	import Gpib
except ModuleNotFoundError:
	raise ModuleNotFoundError("Could not import package `Gpib`. Make sure that "
		"this package is installed in order to interact with the CM300xi probe "
		"station.") from None

from wlt.hardware.probe_station import ProberError

#encoding used for probe station communication
ENCODING = "ASCII"

#regex to parse error messages
_ERROR_REGEX = r"(\d+):"
_COMPILED_ERROR_REGEX = re.compile(_ERROR_REGEX)

#dictionary which stores the fields contained in probe station replies
_REPLY_FIELDS = {
	"StepNextDie": [
		"Column",
		"Row",
		"SubDie",
		"Total SubDies"
	],
	"ReadChuckPosition": [
		"X Value",
		"Y Value",
		"Z Value",
	],
	"ReadMapPosition": [
		"Column",
		"Row",
		"xPosition",
		"yPosition",
		"CurrentSubDie",
		"Total SubDies",
		"CurrentDie",
		"Total Dies",
		"CurrentCluster",
		"Total Clusters"
	],
	"ReadChuckStatus": [
		"IsInitialized",
		"Mode",
		"OnEndLimit",
		"IsMoving",
		"CompMode",
		"Vacuum",
		"Z Height",
		"Load Position",
		"Lift",
		"ChuckCamera"
	],
	"GetDieDataAsColRow": [
		"DieNumber",
		"Column",
		"Row",
		"Bincode",
		"Result"
	]
}


def _decode_reply(reply):
	"""
	Decodes the probe station replies.

	Parameters
	----------
	reply : str
		The probe station reply.

	Returns
	-------
	str
		The decoded reply.

	Raises
	------
	RuntimeError
		If the decoding of the probe station message fails.
	"""

	try:
		return reply.decode(ENCODING)
	except UnicodeDecodeError:
		raise RuntimeError("Could not decode the probe station reply!") from None


def _check_error_code(reply):
	"""
	Checks a probe station reply for errors.

	Raises
	------
	ProberError
		If a probe station error is detected.

	Raises
	------
	RuntimeError
		If something goes wrong when trying to acquire the error code from the 
		probe station reply.
	ProberError
		If a probe station error is detected.
	"""

	#probe station error code to parse and return at the end
	error = None

	#check that the error code is in the reply and strip it from the string
	match = _COMPILED_ERROR_REGEX.search(reply)
	if match is None:
		raise RuntimeError("Could not parse the probe station error!")
	error = match.group(1)

	#typecasting the error code. Should not raise an exception because of the
	#way the error regex is defined but checking anyway
	try:
		error = int(error)
	except ValueError:
		raise RuntimeError("Error code typecasting failed!") from None

	#check if an error has happened
	if error != 0:
		raise ProberError(f"Probe station error detected! Error: '{reply}'")


def _strip_error_code(reply):
	"""
	Parses the error code from probe station replies and returns the 
	stripped message and the error code.

	Parameters
	----------
	reply : str
		Probe station reply to inspect and process.

	Returns
	-------
	tuple
		The error code and the stripped reply.

	Raises
	------
	RuntimeError
		If the typecasting of the error code fails.
	"""

	#probe station error code to parse and return at the end
	error = None

	#check that the error code is in the reply and strip it from the string
	match = _COMPILED_ERROR_REGEX.search(reply)
	if match:
		error = match.group(1)
		stripped = _COMPILED_ERROR_REGEX.sub("", reply)
		stripped = stripped.lstrip() #remove leading space
	else:
		stripped = reply

	#typecasting the error code. Should not raise an exception because of the
	#way the error regex is defined but checking anyway
	if error is not None:
		try:
			error = int(error)
		except ValueError:
			raise RuntimeError("Error code typecasting failed!") from None

	return error, stripped


#function to extract the different fields from a probe station reply
def _get_reply_fields(command, reply):
	"""
	Gets a dictionary with the reply from the probe station.

	Parameters
	----------
	command : str
		Probe station command name.
	reply : str
		Probe station reply to parse.

	Returns
	-------
	dict
		Dictionary with the different fields.

	Raises
	------
	ValueError
		If the `command` is invalid.
	RuntimeError
		If it's not possible to get data from the probe station or if a 
		non-zero error is detected.
	ProberError
		If the error code from the probe station reply is not 0.
	"""

	#try to get the different fields for the given command. If there is nothing
	#in the `_REPLY_FIELDS` dictionary, raise a `ValueError` exception
	try:
		fields = _REPLY_FIELDS[command]
	except KeyError:
		raise ValueError(f"No reply fields data for command: {command}") from None

	#processing the answer. Stripping the error code and splitting the reply
	#at each space
	try:
		error, reply = _strip_error_code(reply)
	except RuntimeError as e:
		raise RuntimeError("Could not get data from the probe station "
			"message") from e

	#check if an error has happened
	if error != 0:
		raise ProberError(f"Probe station error detected! Error: {reply}")

	#splitting the reply
	values = reply.split(" ")
	values_dict = dict(zip(fields, values))

	return values_dict


class CM300xi():
	"""
	Class used for controlling the CM300xi wafer prober with the Python GPIB
	library.

	Dangerous commands like set_position while in contact are ignored
	by the command server.
	"""

	#message returned by the probe station when pattern matching fails
	pattern_matching_error = "Pattern matching could not locate wafer model"

	def __init__(self, port):
		"""
		Initializes the GPIB communication and the reply attribute.

		The primary GPIB address is set to 0, while the secondary GPIB address
		can be passed as a parameter.

		Parameters
		----------
		port : secondary GPIB address.
		"""

		self.wprober = Gpib.Gpib(0, port)

	def _get_chuck_status(self):
		"""
		Gets the status of the chuck from the probe station.

		Returns
		-------
		dict
			A dictionary containing the status of the probe station chuck.
		"""

		#querying the probe station command server. The answer is a string
		command = "ReadChuckStatus"
		reply = self.query(command)
		try:
			status_dict = _get_reply_fields(command, reply)
		except Exception as e:
			raise RuntimeError("Could not get chuck status!") from e

		return status_dict

	def query(self, question):
		"""
		Performs a query operation.

		Parameters
		----------
		question : str
			The command to send to the probe station for the query operation.

		Returns
		-------
		str
			The reply from the probe station.

		Raises
		------
		RuntimeError
			If the probe station reply cannot be decoded.
		"""

		self.wprober.write(question)
		reply = self.wprober.read(100)
		try:
			decoded_reply = _decode_reply(reply)
		except RuntimeError as e:
			raise RuntimeError("Could not complete the query operation!") from e

		return decoded_reply

	def set_position(self, x, y, speed = None):
		"""
		Move chuck to absolute position in um.

		Parameters
		----------
		x, y : float
			Absolute position.
		speed : int or float, optional
			Movement speed as a percentage of full speed.
		"""

		if speed:
			self.wprober.write(f"MoveChuckSubsite {x:1.1f} {y:1.1f} R Y {speed:d}")
		else:
			self.wprober.write(f"MoveChuckSubsite {x:1.1f} {y:1.1f} R Y")

	def move_position(self, dx, dy, speed = None):
		"""
		Move chuck relative to actual position in um.

		Parameters
		----------
		dx, dy : float
			Relative position.
		speed : int or float, optional
			Movement speed as a percentage of full speed.
		"""

		if speed:
			self.wprober.write(f"MoveChuckPosition {dx:1.1f} {dy:1.1f} R Y {speed:d}")
		else:
			self.wprober.write(f"MoveChuckPosition {dx:1.1f} {dy:1.1f} R Y")

	def get_position(self):
		"""Read chuck position (x, y, z)."""

		reply = self.query("ReadChuckPosition Y H")
		values = reply[2:].split(" ")
		return (float(values[1]), float(values[2]), float(values[3][:-1]))

	def is_in_contact(self):
		"""
		Returns the status of the chuck and whether it is at contact/overtravel 
		height or not.

		Returns
		-------
		bool
			The contact status, with True meaning that the probe station chuck 
			is at overtravel or contact height.
		"""

		#use the get_chuck_status method
		reply = self._get_chuck_status()

		#if the chuck height is set to "contact" or "overtravel", return
		#true, otherwise return false
		is_in_contact = (reply["Z Height"] in ["C", "O"])

		return is_in_contact

	def goto_die(self, index_x, index_y):
		"""
		Move chuck to wafer map chip index.

		Parameters
		----------
		index_x : int
			The column index of the die to move to.
		index_y : int
			The row index of the die to move to.

		Raises
		------
		ProberError
			If the movement fails due to a probe station error.
		"""

		reply = self.query(f"StepNextDie {index_x:d} {index_y:d}")
		try:
			_check_error_code(reply)
		except RuntimeError:
			print("Failed to read back probe station error")
		except ProberError:
			raise

	def goto_next_die(self):
		"""Move chuck to next die from wafer map."""

		reply = self.query("StepNextDie")
		try:
			_check_error_code(reply)
		except RuntimeError:
			print("Failed to read back probe station error")
		except ProberError:
			raise

	def get_die_number(self, index_x, index_y):
		"""
		Gets the number of the die located at (index_x, index_y).

		Parameters
		----------
		index_x : int
			The column index of the die to move to.
		index_y : int
			The row index of the die to move to.

		Returns
		-------
		int or None
			The die number.

		Raises
		------
		ProberError
			If the probe station returns an error.
		"""

		#query the probe station
		command = "GetDieDataAsColRow" #there's also GetDieDataAsNum
		reply = self.query(f"{command} {index_x} {index_y}")
		try:
			_check_error_code(reply)
		except RuntimeError:
			print("Failed to read back probe station error")
		except ProberError:
			raise

		#getting the ID
		try:
			response_dict = _get_reply_fields(command, reply)
			die_number_str = response_dict["DieNumber"]
		except Exception:
			die_number_str = None
			#raise RuntimeError(f"Could not get ID for die {index_x} {index_y}!") from e

		#try to convert it to int
		try:
			die_number = int(die_number_str) if die_number_str is not None else None
		except ValueError as e:
			raise RuntimeError("Could not convert die number to int!") from e

		return die_number

	def goto_die_number(self, number):
		"""
		Move chuck to `number`-th die from wafer map.

		Parameters
		----------
		number : int
			Number of the die to which the probe station chuck must move to.

		Raises
		------
		ProberError
			If the movement fails.
		"""

		reply = self.query(f"StepToDie {number}")
		try:
			_check_error_code(reply)
		except RuntimeError:
			print("Failed to read back probe station error")
		except ProberError:
			raise

	def goto_first_die(self):
		"""Move chuck to first die from wafer map."""

		reply = self.query("StepFirstDie")
		try:
			_check_error_code(reply)
		except RuntimeError:
			print("Failed to read back probe station error")
		except ProberError:
			raise

	def read_probe_message(self):
		"""
		Reads a reply from the probe station.

		Returns
		-------
		str or None
			The reply of the probe station or None if something wrong happens 
			during the reading.
		"""

		try:
			reply = self.wprober.read(100)
		except Exception:
			return None
		return reply.decode()

	def get_die(self):
		"""
		Get wafer map chip index.

		Returns
		-------
		tuple of int
			Tuple with the format (column_index, row_index).
		"""

		reply = self.query("ReadMapPosition")
		values = reply[2:].split(" ")
		return (int(values[1]), int(values[2]))

	def contact(self):
		"""Move chuck to contact z position."""

		reply = self.query("MoveChuckContact")
		try:
			_check_error_code(reply)
		except RuntimeError:
			print("Failed to read back probe station error")
		except ProberError:
			raise

	def separate(self):
		"""Move chuck to separation z position."""

		reply = self.query("MoveChuckSeparation")
		try:
			_check_error_code(reply)
		except RuntimeError:
			print("Failed to read back probe station error")
		except ProberError:
			raise

	def load(self):
		"""Move chuck to load z position."""

		reply = self.query("MoveChuckLoad")
		try:
			_check_error_code(reply)
		except RuntimeError:
			print("Failed to read back probe station error")
		except ProberError:
			raise

	def get_id(self):
		"""
		Ask ID of the wafer prober.

		Returns
		-------
		str
			The identification string of the probe station.
		"""

		reply = self.query("*IDN?")
		return reply
