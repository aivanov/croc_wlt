"""Script used for monitoring the status of the edge sensors."""

import time
import curses

from wlt.hardware.wpac import wpac
from wlt.config.main_config import WPAC_CONF

#time to wait between checks on probe card edge sensors
SLEEP = 0.1


def main(screen):
	#initializing WPAC instance and opening the connection
	wpac_inst = wpac.Wpac(**WPAC_CONF)
	wpac_inst.open()

	#remove character echo and using return to send inputs
	curses.noecho()
	curses.cbreak()

	#initializing colors for edge sensor status
	curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
	curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK) 

	try:
		#time loop for monitoring
		while True:
			#getting the edge sensor status
			try:
				es_status = wpac_inst.read_edge_sensors()
			except Exception:
				es_status = [None, None]

			#unpacking the output
			is_es1_on = es_status[0]
			is_es2_on = es_status[1]

			#base options for the edge sensor status text
			es1_print_opts = curses.A_BOLD
			es2_print_opts = curses.A_BOLD

			if is_es1_on:
				#set the green color for the text
				es1_print_opts |= curses.color_pair(2)
			else:
				#set the red color for the text
				es1_print_opts |= curses.color_pair(1)

			if is_es2_on:
				#set the green color for the text
				es2_print_opts |= curses.color_pair(2)
			else:
				#set the red color for the text
				es2_print_opts |= curses.color_pair(1)

			screen.addstr(
				0,
				0,
				"croc_wlt - Edge sensors monitoring tool\n\n",
				curses.A_BOLD
			)
			screen.addstr(f"Monitoring time is set to {SLEEP} s\n\n")

			screen.addstr("    Edge sensor 1: ", curses.A_BOLD)
			screen.addstr(f"{is_es1_on}\n", es1_print_opts)
			screen.addstr("    Edge sensor 2: ", curses.A_BOLD)
			screen.addstr(f"{is_es2_on}\n\n", es2_print_opts)

			screen.addstr("You can stop the execution by sending the CTRL+C ")
			screen.addstr("command\n\n")

			#update the screen in the end
			screen.refresh()

			#sleeping until the next check
			time.sleep(SLEEP)

	#catch a keyboard interrupt to stop the infinite loop
	except KeyboardInterrupt:
		print("Caught a keyboard interrupt. Stopping the execution...")

	#closing the connection
	wpac_inst.close()


if __name__ == "__main__":
	curses.wrapper(main)
