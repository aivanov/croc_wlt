#!/usr/bin/env python3
"""
Module used to communicate with the Wafer Probing Auxiliary Card (WPAC).

This module defines the Wpac class, which is used for interfacing with the WPAC.
Moreover, the WpacError exception is defined, which is raised when the card
answers ERR to a command.
"""

import time
import serial

from wlt.config import main_config

from wlt.hardware.probe_cards import PROBE_CARDS

ARDUINO_REBOOT_TIME = 2 #seconds

#TO DO:
# 1) remove extra IOError exceptions raised when a method uses the methods 
#    to read/write on the serial bus;
# 2) add private method for checking that the WPAC answer is not ERR instead of 
#    repeating the code in each method;
# 3) check the WPAC response in read_edge_sensors instead of taking for granted 
#    that there have been no problems
# 4) improve safety of writing/reading operations
# 5) use global configuration constant `PROBE_CARD` instead of probe card
#    attribute?
# 6) add method to set default value for single register and for all registers!
# 7) add a serial query method


class WpacError(Exception):
	"""Exception raised when the card answers ERR to a command."""


class Wpac:
	"""Class used for interfacing with the Wafer Probing Auxiliary Card (WPAC)."""

	def __init__(self, port, probe_card, baud_rate = 230400, termination = '\n',
			timeout = 1, temp_sens_ids = None):
		"""
		Initializes the Wpac object.

		Parameters
		----------
		port : str
			Path to the device file for the WPAC.
		probe_card : dict
			Probe card description dictionary. The available probe cards can be 
			found in the `wlt.hardware.probe_cards` module.
		baud_rate : int, optional
			Baud rate to be used for the communication.
		termination : str, optional
			Termination character used when writing data to the serial 
			communication and when reading back data from the WPAC.
		timeout : float, optional
			Time to wait (in seconds) for an answer from the WPAC.
		temp_sens_ids : list of int or None, optional
			List of the IDs of the available temperature sensors.

		Raises
		------
		ValueError
			When the `probe_card` parameter is invalid.
		IOError
			If there has been an input-output problem with the interface.
		"""

		self.port = port
		self.baud_rate = baud_rate #Bd
		self.termination = termination
		self.timeout = timeout #seconds
		self.interface = None

		#initialize probe card attribute
		if probe_card not in PROBE_CARDS:
			raise ValueError("Invalid probe card type!")
		self.probe_card = probe_card

		#initialize the list of temperature sensor IDs
		if temp_sens_ids is None:
			temp_sens_ids = [4]
		self.temp_sens_ids = temp_sens_ids

	def open(self):
		"""
		Opens the port used for communicating with the probe card.

		Raises
		------
		IOError
			When opening the device file fails or when it is already open.
		RuntimeError
			When one (or more) wrong parameters have been passed to 
			serial.Serial()
		"""

		#check if port has already been opened
		if self.interface is not None:
			if self.interface.is_open is True:
				raise IOError("Port already open!")

		#trying to open the serial port for communication
		try:
			self.interface = serial.Serial(
				self.port,
				self.baud_rate,
				timeout=self.timeout,
				exclusive=False #prevents simultaneous WPAC usage if True
			)
		#check for pySerial exceptions (port already open, ...)
		except serial.SerialException as e:
			raise IOError(f"Cannot open port {self.port}") from e
		#check for wrong parameters passed to serial.Serial()
		except ValueError as e:
			raise RuntimeError("Wrong parameters for WPAC "
				"communication!") from e

	def _write_serial(self, message):
		"""
		Low-level method to send data to the WPAC.

		Parameters
		----------
		message : str
			The message to write to the serial port. It must have no 
			termination character, as this is added by the method.

		Raises
		------
		IOError
			When the serial port has not been previously opened.
		TypeError
			If the message is not a string.
		RuntimeError
			If encoding the input message using UTF-8 fails.
		"""

		#check if port has been initialized
		if self.interface is None:
			raise IOError("Port is not open!")

		#trying to add the termination character
		try:
			message += self.termination
		except AttributeError:
			raise TypeError("The message parameter should be a string!") from None

		#trying to encode the message using UTF-8
		encoded_mesg = None
		try:
			encoded_mesg = message.encode()
		except UnicodeEncodeError:
			raise RuntimeError("Could not encode the WPAC message!") from None

		#writing
		self.interface.write(encoded_mesg)

	def _read_serial(self):
		"""
		Low-level method to read data from the WPAC.

		Returns
		-------
		str
			The data read from the serial port, decoded and with the 
			termination character removed.

		Raises
		------
		IOError
			When the serial port has not been previously opened.
		RuntimeError
			If the decoding of the input message or the encoding of the 
			termination character fail.
		"""

		#check if port has been initialized
		if self.interface is None:
			raise IOError("Port is not open!")

		#read incoming data
		try:
			encoded_term = self.termination.encode()
		except UnicodeEncodeError:
			raise RuntimeError("Could not encode the termination character!") from None
		data = self.interface.read_until(encoded_term)

		#trying to decode the data
		try:
			decoded_data = data.decode()
		except UnicodeDecodeError:
			raise RuntimeError("Could not decode the WPAC message using UTF-8! "
			f"Raw data: {data}") from None

		#remove the termination character
		decoded_data = decoded_data.strip(self.termination)

		return decoded_data

	def reset(self):
		"""
		Sends the reset signal to the WPAC and checks that the probe card has 
		been initialized correctly.

		When the WPAC is reset, the probe card is power-cycled and 
		reinitialized. This method checks that the probe card registers have 
		their default value (as defined in the global dictionaries for the 
		different chip types).

		Raises
		------
		IOError
			When the serial port has not been previously opened.
		WpacError
			When the WPAC answers with the error message ("ERR").
		RuntimeError
			When the answer to the reset command is neither "OK" nor "ERR". 
			Also raised in case the read-back values of the probe card 
			registers differ from their default values.
		"""

		#check if port has been initialized
		if self.interface is None:
			raise IOError("Port is not open!")

		#writing command
		self._write_serial("RS")

		#checking the answer
		answer = self._read_serial()
		if answer == "OK":
			pass
		elif answer == "ERR":
			raise WpacError("WPAC answered ERR!")
		else:
			raise RuntimeError(f"Unrecognized WPAC response: {answer}")

		#wait until the Due restarts
		time.sleep(ARDUINO_REBOOT_TIME)

		#check that the registers have been initialized correctly
		for register in self.probe_card["gpio"].keys():
			#skip this iteration if no default value is set
			if self.probe_card["gpio"][register]["default"] is None:
				continue

			#get the current value and its default
			value = self.read_register(register)
			default_value = self.probe_card["gpio"][register]["default"]

			#check that the default value and the read one match
			if value != default_value:
				raise RuntimeError(
					f"Wrong value for register {register}: {value}. "
					f"Expected: {default_value}"
				)

	def init(self):
		"""
		Sends the initialization signal to the WPAC and checks that the probe  
		card has been initialized correctly.

		This method checks that the probe card registers have 
		their default value (as defined in the global dictionaries for the 
		different chip types).

		Raises
		------
		IOError
			When the serial port has not been previously opened.
		WpacError
			When the WPAC answers with the error message ("ERR").
		RuntimeError
			When the answer to the reset command is neither "OK" nor "ERR". 
			Also raised in case the read-back values of the probe card 
			registers differ from their default values.
		"""

		#check if port has been initialized
		if self.interface is None:
			raise IOError("Port is not open!")

		#writing command
		self._write_serial("IN")

		#checking the answer
		answer = self._read_serial()
		if answer == "OK":
			pass
		elif answer == "ERR":
			raise WpacError("WPAC answered ERR!")
		else:
			raise RuntimeError(f"Unrecognized WPAC response: {answer}")

		#check that the registers have been initialized correctly
		for register in self.probe_card["gpio"].keys():
			#skip this iteration if no default value is set
			if self.probe_card["gpio"][register]["default"] is None:
				continue

			#get the current value and its default
			value = self.read_register(register)
			default_value = self.probe_card["gpio"][register]["default"]

			#check that the default value and the read one match
			if value != default_value:
				raise RuntimeError(
					f"Wrong value for register {register}: {value}. "
					f"Expected: {default_value}"
				)

	def _read_i2c(self, address):
		"""
		Reads the input register of a PCA9554 on the probe card.

		Parameters
		----------
		address : int
			The address of the I2C expander on the probe card.

		Returns
		-------
		int
			The value of the input register of the I2C expander found at that 
			address.

		Raises
		------
		IOError
			When the serial port has not been previously opened.
		ValueError
			When the I2C address is invalid.
		RuntimeError
			In case the data read from the WPAC is malformed and it's not 
			possible to get the command and the value or to convert the value
			to an integer.
		WpacError
			When the WPAC answers with the error message ("ERR").
		"""

		#check if port has been initialized
		if self.interface is None:
			raise IOError("Port is not open!")

		#checking I2C address
		if address not in self.probe_card["i2c_addr"]:
			raise ValueError(f"No I2C expander at address {address}")

		#writing the I2C read message
		self._write_serial(f"IR,{address:#x}")

		#reading back the value
		data = self._read_serial()

		#checking for errors
		if data == "ERR":
			raise WpacError("WPAC answered ERR!")

		if data == "":
			raise RuntimeError("Empty data from WPAC!")

		#splitting and getting the read value
		try:
			command, value = data.split(",")
		except ValueError:
			raise RuntimeError(f"Error while unpacking data: {data}") from None

		#converting read value from hex to dec
		try:
			value = int(value, 16)
		except ValueError:
			message = "Failed to extract register value from WPAC answer!"
			raise RuntimeError(message) from None

		return value

	def _write_i2c(self, address, value):
		"""
		Writes to the output register of a PCA9554 on the probe card.

		Parameters
		----------
		address : int
			The address of the I2C expander on the probe card.
		value : int
			The value to write to the output register of that I2C expander. It 
			is an 8 bit register, so the value parameter must be in the [0, 32] 
			interval.

		Raises
		------
		IOError
			When the serial port has not been previously opened.
		TypeError
			If address or value are not integers.
		ValueError
			If the I2C address or the value to write have an invalid value.
		WpacError
			When the WPAC answers with the error message ("ERR").
		"""

		#check if port has been initialized
		if self.interface is None:
			raise IOError("Port is not open!")

		#checking address type
		if not isinstance(address, int):
			raise TypeError("The I2C address should be an integer!")

		#checking value type
		if not isinstance(value, int):
			raise TypeError("The I2C value to be written should be an integer!")

		#checking I2C address
		if address not in self.probe_card["i2c_addr"]:
			raise ValueError(f"No I2C expander at address {address}")

		#checking value range
		if value not in range(0, 2 ** self.probe_card["i2c_reg_size"]):
			raise ValueError(f"Can't write {value} to 8 bit register")

		#writing the I2C read message
		self._write_serial(f"IW,{address:#x},{value:#x}")

		#reading WPAC message
		data = self._read_serial()

		#checking that the answer is not ERR
		if data == "OK":
			pass
		elif data == "ERR":
			raise WpacError(f"WPAC answered ERR while trying to write {value} "
				f"to output register of I2C expander at address {address}")
		else:
			raise RuntimeError("Wrong answer from WPAC after I2C write!")

	def _generate_bitmask(self, offset, size):
		"""
		Utility to generate a bitmask from offset and size of a given probe
		card register.

		Parameters
		----------
		offset : int
			The offset of the register from the LSB.
		size : int
			The number of bits in the register.

		Returns
		-------
		int
			The generated bitmask, given the input offset and size.

		Raises
		------
		ValueError
			When the offset and/or the size are invalid.
		"""

		#size of I2C registers on probe card
		register_size = self.probe_card["i2c_reg_size"]

		#checking offset
		if offset not in range(0, register_size):
			raise ValueError("Invalid offset for bitmask generation")

		#checking size
		if size not in range(1, register_size + 1):
			raise ValueError("Invalid size for bitmask generation")

		is_overflowing = offset + size - 1 not in range(0, register_size)

		#checking boundaries
		if is_overflowing:
			raise ValueError("Invalid combination of offset and size for "
				"bitmask generation")

		#bitmask generation
		bitmask = 0
		for bit in range(0, register_size):
			#bool values used to enable the mask bits
			is_bit_greq_offset = (bit >= offset)
			is_bit_in_size = (bit - offset < size)

			if is_bit_greq_offset and is_bit_in_size:
				bitmask += 1 << bit

		return bitmask

	def read_register(self, register):
		"""
		Reads a given register from the probe card.

		Parameters
		----------
		register : str
			The name of the probe card register to read.

		Returns
		-------
		int
			The value of the register.

		Raises
		------
		ValueError
			When an invalid register name is passed to the method.
		"""

		#checking if the register is correct
		if register not in self.probe_card["gpio"].keys():
			raise ValueError(f"No register named {register} in register map")

		#querying the probe card
		address = self.probe_card["gpio"][register]["address"]
		data = self._read_i2c(address)

		#getting the value from the answer byte
		offset = self.probe_card["gpio"][register]["offset"]
		size = self.probe_card["gpio"][register]["size"]
		bitmask = self._generate_bitmask(offset, size)
		value = (data & bitmask) >> offset

		return value

	def write_register(self, register, value):
		"""
		Writes to a given register of the probe card.

		After writing to the register, the new value is read back in order to 
		check that it is the correct one.

		Parameters
		----------
		register : str
			The name of the probe card register to write to.
		value : int
			The value to write to the register.

		Raises
		------
		ValueError
			When an invalid register name or value is passed to the method.
		IOError
			If the read-back value after the writing is different from the 
			expected one.
		"""

		#checking if the register is correct
		if register not in self.probe_card["gpio"].keys():
			raise ValueError(f"No register named {register} in register map")

		#check if trying to change the edge sensors
		if register == "ES":
			raise ValueError("Cannot write to ES, it's read only!")

		#getting register information
		address = self.probe_card["gpio"][register]["address"]
		offset = self.probe_card["gpio"][register]["offset"]
		size = self.probe_card["gpio"][register]["size"]

		#checking value
		if value not in range(0, 2 ** size):
			raise ValueError(f"Can't write {value} to {size} bit register!")

		#check if trying to set the MUX to a wrong value
		valid_mux_values = [data["address"] for data in self.probe_card["mux"].values()]
		if register == "MUX_SEL" and value not in valid_mux_values:
			raise ValueError(f"Invalid setting for register MUX_SEL: {value}")

		#getting the bitmask
		bitmask = self._generate_bitmask(offset, size)

		#getting current status of GPIO register
		input_register = self._read_i2c(address)
		edited_register = input_register

		#clearing edge sensors data (setting to default 0b00)
		if address == self.probe_card["gpio"]["ES"]["address"]:
			#Writing 0 is safer if, for some reason, the pin is set as output.
			#In that case, if an edge sensor is closed, the value remains at GND.
			#If the ES opens (contact), there is a 10 kohm resistor between this
			#node and VCC. If it is set to 1, instead, there is a low impedance
			#path between VCC and GND when the ES is closed.
			es_offset = self.probe_card["gpio"]["ES"]["offset"]
			es_size = self.probe_card["gpio"]["ES"]["size"]
			es_bitmask = self._generate_bitmask(es_offset, es_size)
			edited_register &= (~es_bitmask) #setting edge sensors to 0b00

		#clearing the register
		edited_register &= (~bitmask)

		#setting value
		edited_register |= (value << offset)

		#writing
		self._write_i2c(address, edited_register)

		#check that the new value is correct
		value_after_writing = self.read_register(register)
		if value_after_writing != value:
			raise IOError("The register has not been updated!")

	def write_default(self, register):
		"""
		Writes the default value of a probe card register.

		Parameters
		----------
		register : str
			Register name for which the default value must be set.

		Raises
		------
		ValueError
			If the `register` parameter does not represent a valid probe card 
			register.
		RuntimeError
			If writing the default value to the register fails.
		"""

		#checking the register name
		try:
			default = self.probe_card["gpio"][register]["default"]
		except KeyError:
			raise ValueError(f"Register {register} not found in probe card "
				"register!") from None

		#performing a write
		try:
			self.write_register(register, default)
		except (ValueError, IOError) as e:
			raise RuntimeError("Could not write default register value!") from e

	def read_all_registers(self):
		"""
		Returns a dictionary containing all registers and their values for 
		each I2C expander on the probe card.

		Returns
		-------
		dict
			A register containing all probe card registers and their values.
		"""

		output_dictionary = {}

		#loop on all probe card registers
		for register in self.probe_card["gpio"].keys():
			value = self.read_register(register)
			output_dictionary[register] = value

		return output_dictionary

	def read_edge_sensors(self):
		"""
		Reads the edge sensors of the probe card.

		Returns
		-------
		list
			A list containing the status of the edge sensors. The element with 
			index 0 is ES1, while at index 1 there is ES2.
		"""

		value = self.read_register("ES")
		edge_sensors = []

		#unpacking and converting to boolean values
		for bit in range(0, 2):
			bit_value = 2 ** bit
			is_it_on = bool(value & bit_value)
			edge_sensors.append(is_it_on)

		return edge_sensors

	def select_mux_output(self, value):
		"""
		Sets the MUX output using the mapping dictionary.

		Parameters
		----------
		value : str
			The name of the multiplexer output to select.

		Raises
		------
		ValueError
			If the value to select on the multiplexer is invalid.
		"""

		#checking value
		if value not in self.probe_card["mux"].keys():
			raise ValueError(f"{value} is not a valid output of the multiplexer!")

		#getting value to write from map
		mapped_value = self.probe_card["mux"][value]["address"]
		self.write_register("MUX_SEL", mapped_value)

	def select_mux_default(self):
		"""
		Sets the output of the probe card multiplexer to the default value 
		specified in the probe card description dictionary.
		"""

		#getting the name of the mux to select
		mux_name = self.probe_card["default_mux"]
		default_value = self.probe_card["mux"][mux_name]["address"]
		self.write_register("MUX_SEL", default_value)

	def select_mux_ground(self, value):
		"""
		Sets the output of the probe card multiplexer to the ground associated 
		to signal `value`.

		Parameters
		----------
		value : str
			The name of the multiplexer output whose ground must be selected.

		Raises
		------
		ValueError
			If the value to select on the multiplexer is invalid.
		"""

		#checking value
		if value not in self.probe_card["mux"].keys():
			raise ValueError(f"{value} is not a valid output of the multiplexer!")

		#getting value to write from map
		gnd_value = self.probe_card["mux"][value]["ground"]
		self.select_mux_output(gnd_value)

	def get_mux_ground(self, value):
		"""
		Returns the name of the multiplexer output which is the ground of 
		`value`.

		Returns
		-------
		str or None
			The name of the multiplexer or None if the ground has not been 
			defined.

		Raises
		------
		ValueError
			If the value to select on the multiplexer is invalid.
		"""

		#checking value
		if value not in self.probe_card["mux"].keys():
			raise ValueError(f"{value} is not a valid output of the multiplexer!")

		#getting value to return from map
		gnd_value = self.probe_card["mux"][value]["ground"]

		return gnd_value

	def read_mux_output(self):
		"""
		Gets the status of the multiplexer using the mapping dictionary.

		Returns
		-------
		str
			The name of the selected output of the probe card multiplexer.
		"""

		#reading the multiplexer register and getting the key
		mux_value = self.read_register("MUX_SEL")
		mux_key_list = [key
			for (key, data) in self.probe_card["mux"].items()
			if data["address"] == mux_value]
		mux_key = mux_key_list[0]

		return mux_key

	def read_temperature(self, sensor_id = 4, use_kelvin = False):
		"""
		Reads the built-in temperature sensor in the WPAC.

		The method can read one of the 4 WPAC temperature sensors, given that 
		they have been connected. In the default configuration, only the 
		temperature sensor number 4 is available, because it is on the WPAC 
		itself.

		Parameters
		----------
		sensor_id : int, optional
			The ID of the WPAC temperature sensor to read. The valid IDs are: 
			[1, 2, 3, 4].

		Returns
		-------
		float
			The temperature read from the sensor.

		Raises
		------
		ValueError
			If the temperature sensor ID is invalid.
		RuntimeError
			If the data read from the serial port cannot be unpacked correctly.
		WpacError
			If the WPAC answers with the 'ERR' string, meaning that an error 
			occurred on the microcontroller side.
		"""

		#valid temperature sensor IDs
		if sensor_id not in self.temp_sens_ids:
			raise ValueError("Invalid temperature sensor ID! Valid values "
				f"are: {self.temp_sens_ids}")

		#write serial command
		self._write_serial(f"RT,{sensor_id}")

		#reading WPAC message
		data = self._read_serial()

		#checking for the error message
		if data == "ERR":
			raise WpacError(f"Could not read temperature sensor {sensor_id}!")

		#getting the temperature
		try:
			command, value = data.split(",")
		except ValueError:
			raise RuntimeError(f"Error while unpacking data: {data}") from None

		#convert to kelvin if selected
		temperature = float(value)
		if use_kelvin is True:
			temperature += 273.15

		return temperature

	def disable_muxsel(self):
		"""
		Opens the circuit between VMUXOUT e VAOUT00.

		Raises
		------
		WpacError
			If the WPAC answers with the error string ('ERR').
		"""

		#write serial command
		self._write_serial("SM,0")

		#reading WPAC message
		data = self._read_serial()

		#checking that the answer is correct
		if data == "ERR":
			raise WpacError("Error while trying to set MUXSEL to 0!")

	def enable_muxsel(self):
		"""
		Shorts VMUXOUT e VAOUT00 together.

		Raises
		------
		WpacError
			If the WPAC answers with the error string ('ERR').
		"""

		#write serial command
		self._write_serial("SM,1")

		#reading WPAC message
		data = self._read_serial()

		#checking that the answer is correct
		if data == "ERR":
			raise WpacError("Error while trying to set MUXSEL to 1!")

	def set_dac(self, value):
		"""
		Sets the DAC0 output of the Due on the WPAC to the desired level.

		Parameters
		----------
		value : int
			Value of the DAC output.

		Raises
		------
		WpacError
			If the WPAC answers with the error string ('ERR').
		"""

		#check input value

		#write serial command
		self._write_serial(f"SD,{value}")

		#reading WPAC message
		data = self._read_serial()

		#checking that the answer is correct
		if data == "ERR":
			raise WpacError(f"Error while trying to set DAC to {value}!")

	def read_adc(self):
		"""
		Reads the A0 pin of the Due on the WPAC.

		Returns
		-------
		int
			The ADC code read from the A0 pin of the Arduino Due on the WPAC.

		Raises
		------
		WpacError
			If the WPAC answers with the error string ('ERR').
		RuntimeError
			If the data read from the serial port cannot be unpacked correctly.
		"""

		#write serial command
		self._write_serial("RA,0")

		#reading WPAC message
		data = self._read_serial()

		#checking that the answer is correct
		if data == "ERR":
			raise WpacError("Error while trying to set MUXSEL to 1!")

		#getting the ADC reading
		try:
			command, pin, adc_code = data.split(",")
		except ValueError:
			raise RuntimeError(f"Error while unpacking data: {data}") from None

		return int(adc_code)

	def enable_shunt_mode(self):
		"""Enables the shunt powering mode by setting the EN_VDD_SHUNT register
		of the probe card to 1."""

		#writing probe card register
		self.write_register("EN_VDD_SHUNT", 1)

	def disable_shunt_mode(self):
		"""Disables the shunt powering mode by setting the EN_VDD_SHUNT register
		of the probe card to 0."""

		#writing probe card register
		self.write_register("EN_VDD_SHUNT", 0)

	def enable_iref_chip(self):
		"""
		Method which enables the reference current of the chip.

		For the CROC v1.0 probe card, the `EN_R_IREF` register is set to 
		1: this connects the external R_IREF resistor.
		"""

		self.write_register("EN_R_IREF", 1)

	def disable_iref_chip(self):
		"""
		Method which enables the reference current of the chip.

		For the CROC v1.0 probe card, the IREF_EN register is set to 0: this 
		disconnects the external R_IREF resistor.
		"""

		self.write_register("EN_R_IREF", 0)

	def set_iref_trim(self, value):
		"""Sets the IREF_TRIM register of the probe card."""

		#writing probe card register
		self.write_register("IREF_TRIM", value)

	def read_edge_sensors_uc(self):
		"""
		Reads the edge sensors using the microcontroller on the WPAC instead 
		of using the I2C bus.

		IOError
			When the serial port has not been previously opened.
		WpacError
			When the WPAC answers with the error message ("ERR").
		RuntimeError
			When the answer to the command is neither "OK" nor "ERR". 
			Also raised in case the read-back values of the probe card 
			registers differ from their default values. Also raised when 
			failing while trying to unpack the data read from the WPAC.
		"""

		#writing command
		self._write_serial("ES")

		#reading back the answer
		answer = self._read_serial()

		#checking the answer
		if "ES" in answer:
			pass
		elif answer == "ERR":
			raise WpacError("WPAC answered ERR!")
		else:
			raise RuntimeError("Something went wrong during communication")

		#getting the edge sensor status from the answer
		try:
			command, value = answer.split(",")
		except ValueError:
			raise RuntimeError(f"Error while unpacking data: {answer}") from None

		#typecasting to int
		value = int(value)

		#variables for extracting the edge sensor status and for returning it
		es_status = {"ES1": bool(value & 1), "ES2": bool(value & 2)}

		return dict(es_status)

	def set_high_shunt_slope(self):
		"""Sets the high slope of the IV curve in shunt-LDO mode."""

		self.write_register("EN_REXT", 0)

	def set_low_shunt_slope(self):
		"""Sets the low slope of the IV curve in shunt-LDO mode."""

		self.write_register("EN_REXT", 1)

	def set_default_shunt_slope(self):
		"""
		Sets the slope of the IV curve in shunt-LDO mode to the default 
		value defined in the probe card dictionary.

		Raises
		------
		RuntimeError
			If the setting fails.
		"""

		try:
			self.write_default("EN_REXT")
		except RuntimeError as e:
			raise RuntimeError("Couldn't set the default shunt slope") from e

	def set_power_vin(self):
		"""
		Configures the Power Board to provide VIN to the chip.

		Raises
		------
		WpacError
			If the WPAC answers with the ERR message.
		RuntimeError
			If the configuration fails.
		"""

		#loop to first disable the output and then to re-enable it with VIN
		#selected
		for command in ["PC,3", "PC,2"]:
			#disable the output and select VIN powering
			self._write_serial(command)

			#reading back the answer
			answer = self._read_serial()

			#checking the answer
			if answer == "OK":
				pass
			elif answer == "ERR":
				raise WpacError("WPAC answered ERR!")
			else:
				raise RuntimeError(f"Unrecognized WPAC response: {answer}")

			#waiting for the power to stabilise
			time.sleep(0.1)

	def set_power_vdd(self):
		"""
		Configures the Power Board to provide VDD to the chip.

		Raises
		------
		WpacError
			If the WPAC answers with the ERR message.
		RuntimeError
			If the configuration fails.
		"""

		#loop to first disable the output and then to re-enable it with VIN
		#selected
		for command in ["PC,0", "PC,1"]:
			#disable the output and select VIN powering
			self._write_serial(command)

			#reading back the answer
			answer = self._read_serial()

			#checking the answer
			if answer == "OK":
				pass
			elif answer == "ERR":
				raise WpacError("WPAC answered ERR!")
			else:
				raise RuntimeError(f"Unrecognized WPAC response: {answer}")

			#waiting for the power to stabilise
			time.sleep(0.1)

	def set_power_disabled(self):
		"""
		Configures the Power Board to disable power to the chip.

		The Power Board goes to the 0 state, which means that VDD is selected 
		but not applied.

		Raises
		------
		WpacError
			If the WPAC answers with the ERR message.
		RuntimeError
			If the configuration fails.
		"""

		#disable the output and select VIN powering
		self._write_serial("PC,0")

		#reading back the answer
		answer = self._read_serial()

		#checking the answer
		if answer == "OK":
			pass
		elif answer == "ERR":
			raise WpacError("WPAC answered ERR!")
		else:
			raise RuntimeError(f"Unrecognized WPAC response: {answer}")

		#waiting for the power to stabilise
		time.sleep(0.1)

	def set_vin_short(self, is_shorted):
		"""
		Configures the Power board v1.1 in order to short together VINA and 
		VIND.

		Parameters
		----------
		is_shorted : bool
			VINA and VIND get shorted together if True, otherwise the Power 
			board gets configured in order not to short them.

		Raises
		------
		TypeError
			If `is_shorted` is not a bool.
		WpacError
			If the WPAC answers with the ERR message.
		RuntimeError
			If the configuration fails.
		"""

		#setting the PC2 signal in order to short (or not) VINA with VIND
		if isinstance(is_shorted, bool):
			int_value = int(is_shorted)
		else:
			raise TypeError("The `is_shorted` parameter should be a bool!")

		self._write_serial(f"PS,{int_value}")

		#reading back the answer
		answer = self._read_serial()

		#checking the answer
		if answer == "OK":
			pass
		elif answer == "ERR":
			raise WpacError("WPAC answered ERR!")
		else:
			raise RuntimeError(f"Unrecognized WPAC response: {answer}")

		#waiting for the power to stabilise
		time.sleep(0.1)

	def set_i2c_wpac(self):
		"""Sets the WPAC as the main device of the probe card I2C bus."""

		#serial communication
		self._write_serial("PB,1")
		answer = self._read_serial()

		#checking the answer
		if answer == "OK":
			pass
		elif answer == "ERR":
			raise WpacError("WPAC answered ERR!")
		else:
			raise RuntimeError(f"Unrecognized WPAC response: {answer}")

	def set_i2c_fc7(self):
		"""Sets the FC7 as the main device of the probe card I2C bus."""

		#serial communication
		self._write_serial("PB,0")
		answer = self._read_serial()

		#checking the answer
		if answer == "OK":
			pass
		elif answer == "ERR":
			raise WpacError("WPAC answered ERR!")
		else:
			raise RuntimeError(f"Unrecognized WPAC response: {answer}")

	def close(self):
		"""Closes the connection to the WPAC."""

		if self.interface is None:
			return

		if self.interface.is_open:
			self.interface.close()


if __name__ == "__main__":
	wpac = Wpac(**main_config.WPAC_CONF)

	wpac.open()

	print("--- Probe card status ---")
	for key, value in wpac.read_all_registers().items():
		print(f"{key}: {value}")

	print("--- Edge sensors status ---")
	print(wpac.read_edge_sensors_uc())

	print("--- Temperature measurement ---")
	print(wpac.read_temperature())

	wpac.close()
