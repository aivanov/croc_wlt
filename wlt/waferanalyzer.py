#!/usr/bin/env python3
#pylint: disable = E1101, E1135, E1136, E1137, R1704
"""
Module used to analyzes the results of the wafer-level testing.

This module defines the WaferAnalyzer class, which performs data analysis on the
test results and produces distributions and wafer maps. The wlt.wafermap module
is used to draw these wafer maps.
"""

import os
import sys
import re
import json
import shutil

from PyPDF2 import PdfFileReader
from PyPDF2 import PdfFileWriter
from PyPDF2 import PdfFileMerger

import numpy as np
import matplotlib.pyplot as plt

from wlt.duts import get_wafer
from wlt.wafermap import WaferMap
from wlt.jsonplotter import JsonPlotter
from wlt.analysis.utils import parse_filename, parse_json_in, chip_name_to_id


class WaferAnalyzer(JsonPlotter):
	"""Performs analysis of a complete wafer combining quality metrics across
	configured tests"""

	# Priority list of the chip status codes in the summary map: [red, grey, yellow, green]
	STATUS_PRIORITY = [2, -1, 1, 0]

	# Regular expressions for matching the wafer JSON data
	RE_WAFER_JSON = [
		# new format used in current tests
		re.compile(r'wafer_\w+-\w+_\d+_\d+.json', re.IGNORECASE),
		# old format used for wafer N6Y250-04F3
		re.compile(r'wafer_\w+_\d+_\d+.json', re.IGNORECASE),
	]

	def __init__(self, in_dir, config, regions, out_dir, verbose=0, dry_run=False):
		super().__init__(verbose=verbose, dry_run=dry_run)
		self.in_dir = in_dir
		self.out_dir = out_dir
		self.stat_regions = None
		self.save_wafer_maps = True
		self.save_plots = True
		# Ensuring that the output folder exists
		os.makedirs(os.path.join(self.out_dir, 'chips'), exist_ok=True)
		# Creating the bookeeping data
		self.chips_to_skip = set()
		self.wafer_data = {}
		self.wafer_mask = {}
		self.chip_status = {}
		self.chip_statuses = {}
		self.test_status = None
		self.wafer_map = None
		# Loading the analysis configuration
		self.load_config(config, regions)
		# Defining the summary-file postfix
		self.merge_postfix = ''


	def load_data(self, file_in = None):
		"""Read all the test data from the input folder"""
		if file_in is not None:
			super().load_data(file_in)

		if not os.path.isdir(self.in_dir):
			print("ERROR: Input folder doesn't exist")
			sys.exit(1)

		# Reading the wafer JSON file
		wafer_json_file = None
		for file_in in os.listdir(self.in_dir):
			if any(reg.match(file_in) for reg in self.RE_WAFER_JSON):
				wafer_json_file = os.path.join(self.in_dir, file_in)
				# Extracting the postfix to be used in the summary output files
				self.merge_postfix = os.path.splitext(file_in)[0][6:]
				break
		if not wafer_json_file:
			print(f'ERROR: No wafer JSON file found in: {self.in_dir}')
			sys.exit(1)
		# Ensuring that the necessary data is present for the wafer
		with open(wafer_json_file, encoding='utf-8') as json_in:
			json_data = json.load(json_in, object_hook=parse_json_in)
			self.wafer_data.update(json_data)
		if 'chip_type' not in self.wafer_data:
			print(f'ERROR: No `chip_type` defined in the wafer JSON file: {wafer_json_file}')
			sys.exit(1)

		# Reading the chip JSON files one by one
		chip_dir = os.path.join(self.in_dir, 'chips')
		if not os.path.isdir(chip_dir):
			print(f'WARNING: No folder with chiptester JSON files: {chip_dir}')
			return
		# Skipping all chips if requested
		if 'ALL' in self.chips_to_skip:
			return
		# Grouping chips by chip ID
		files_in = {}
		for file_in in os.listdir(chip_dir):
			if os.path.splitext(file_in)[1] != '.json':
				continue
			file_path = os.path.join(chip_dir, file_in)
			scan_info = parse_filename(file_in, strict=True)
			if scan_info is None:
				continue
			chip_id = scan_info['chip']
			# Skipping if this chip ID should be skipped
			if chip_id in self.chips_to_skip:
				continue
			# Putting the file in the list for this chip
			if chip_id not in files_in:
				files_in[chip_id] = []
			files_in[chip_id].append({'path': file_path,
				'scan_time': scan_info['scan_time']})
		# Loading the most recent JSON file for each chip if there are more than one
		for chip_id, files in files_in.items():
			files.sort(key=lambda x: x['scan_time'])
			file_path = files[-1]['path']
			if self.verbose:
				print(f'Loading file: {file_path}')
			super().load_data(files[-1]['path'])
		print(f'Loaded chip data from {len(self.data)} files')


	def load_wafer_mask(self, path):
		"""Loads the wafer mask of discarded chips"""
		if not os.path.isfile(path):
			print(f'ERROR: Mask file doesn\'t exist: {path}')
			sys.exit(1)
		with open(path, encoding='utf-8') as json_in:
			self.wafer_mask = json.load(json_in)

		# Inserting reasons of discarded chips as virtual tests in the configuration
		discarded_chips = []
		for reason, chip_names in self.wafer_mask.items():
			# Validating the provided chip names
			for name in chip_names:
				chip_name_to_id(name)
			if len(chip_names) == 0:
				continue
			# Adding configuration for the virtual test
			self.config[reason] = {
				'input': {
					'+name': '@chip_name'
				},
				'output': lambda i, chips=self.wafer_mask[reason]: int(i['+name'] in chips),
				'regions': [0,0,0, 1,1, 2],
				'bins': 1,
				'group': 0,
				'priority': 1,
				'aggregate': 'histogram',
				'title': f'{reason};;Chips',
				'bins.int': {0: 'NO', 1: 'YES'},
				'skip': False
			}
			discarded_chips += chip_names
		n_discarded_chips = len(set(discarded_chips))
		print(f'Loaded list of discarded chips: {n_discarded_chips}\n {path}')


	def pre_plotting(self):
		"""Additional pre-processing steps"""
		# Executing the standard preprocessing
		super().pre_plotting()


	def fill_wafer_map(self):
		"""Update the wafer map with data from the current test"""
		config, name = (self.plot_data[n] for n in ['config', 'name'])
		# Getting the default overview dictionaries with keys converted from
		# filenames to chip IDs
		chip_data, chip_status = {}, {}
		for (d_new, d_old) in zip((chip_data, chip_status), self.process_stats()):
			for k, v in d_old.items():
				scan_info = self.scan_info_dict(k)
				chip_id = (scan_info['chip_column'], scan_info['chip_row'])
				d_new[chip_id] = v
		# Adding statistics to the wafer data
		if 'statistics' not in self.wafer_data:
			self.wafer_data['statistics'] = {}
		self.wafer_data['statistics'][name] = self.plot_data['stats']

		# Limiting the maximum status value to 2
		for chip_id, status in chip_status.items():
			chip_status[chip_id] = min(status, 2)
			# Storing the status of each chip in this test to the global collection
			if chip_id not in self.chip_statuses:
				self.chip_statuses[chip_id] = {}
			self.chip_statuses[chip_id][self.plot_data['name']] = chip_status[chip_id]

		# Updating the global status of each chip
		if config['priority'] >= 0 and 'idx' in config:
			for chip, status in chip_status.items():
				self.chip_status[chip][config['idx']] = status
		# Marking the status of the test in the summary
		if config['priority'] > 0:
			self.test_status[config['idx']] = 1

		# Not updating chips in the wafer map if it will not be saved
		if not self.save_wafer_maps or config['priority'] < 0:
			return

		# Setting the test status for the wafer
		self.wafer_map.reset_chips()
		if config['priority'] < 1:
			self.wafer_map.set_ignored(True)

		# Updating each chip in the wafer map
		for chip, status in chip_status.items():
			data = chip_data[chip]
			str_format = '{0}'
			if isinstance(data, float):
				str_format = '{0:.3f}'
			self.wafer_map.set_chip(chip, status, value=data, str_format=str_format)

		# Updating the title of the wafer map
		wafer_title = f"{self.wafer_data['wafer_id']}/{self.plot_data['name']}"
		self.wafer_map.set_title(wafer_title, self.wafer_data['wafer_id'])


	def skip_chip(self, chip_id):
		"""Adds a chip to the set of chips to be skipped during analysis"""
		if chip_id.lower() == 'all':
			self.chips_to_skip.add('ALL')
			return
		# Converting from the hexadecimal ID if provided as a string
		chip_id = chip_name_to_id(chip_id)
		self.chips_to_skip.add(chip_id)


	def draw_plot(self):
		"""Performing additional plotting steps after the main plot"""
		if self.dry_run:
			return

		# Updating wafer-level data only for plots that enter into the summary
		self.fill_wafer_map()

		# Drawing plots if enabled
		if self.save_plots:
			super().draw_plot()


		# Not storing the wafer if drawing is done for a single chip
		if self.plot_data['file_id'] is not None:
			return

		# Not storing the wafer if drawing is done for low-priority plots
		config = self.plot_data['config']
		if 'priority' in config and config['priority'] < 0:
			return

		# Saving the wafer map
		if self.save_wafer_maps:
			out_path = self.output_path(self.plot_data['name']+'_map')
			self.wafer_map.save(out_path)


	def draw_summary_map(self):
		"""Drawing a summary wafer map for the executed tests"""
		self.wafer_map.reset_chips()
		wafer_title = 'UNDEFINED TITLE'
		# Updating each chip in the wafer map
		status_map = {}
		for chip, statuses in self.chip_status.items():
			# Selecting only non-ignored tests to define the chip status
			relevant_statuses = statuses[self.test_status > 0]
			status = -2
			for status_p in self.STATUS_PRIORITY:
				if status_p in relevant_statuses:
					status = status_p
					break
			# Updating the map if status is meaningful
			if status in self.wafer_map.STATUS_COLORS:
				# Counting the number of tests with this status
				n_tests = np.count_nonzero(relevant_statuses == status)
				self.wafer_map.set_chip(chip, status, value=n_tests)
			# Saving the status for the presentation map
			status_map[chip] = status
			# Adding a count of grey chips
			n_tests_grey = np.count_nonzero(relevant_statuses == -1)
			if status != -1 and n_tests_grey > 0:
				self.wafer_map.set_chip_subtext(chip, str(n_tests_grey))
		# Setting the counts of different chip statuses in the wafer map
		self.wafer_map.update_chip_counts()
		# Setting the wafer title
		n_chips_tested = len(self.chip_statuses)
		try:
			wafer_title = '{sn}/{batch}:  {ntst}/{nchips} chips analysed'.format(
			                sn = self.wafer_data['wafer_id'],
			                batch = self.wafer_data['batch_id'],
			                ntst = n_chips_tested,
			                nchips = len(self.wafer_map.map))
			self.wafer_map.set_title(wafer_title, self.wafer_data['wafer_id'])
		except KeyError:
			if self.verbose:
				print('WARNING: Missing data for the wafer map title')
		# Saving the wafer map
		out_path = self.output_path('_summary_map')
		self.wafer_map.save(out_path)

		###### Saving chip counts to the wafer summary data
		statuses = list(status_map.values())
		counts = {status: statuses.count(status) for status in self.COLORS_COUNTS}
		self.wafer_data['chip_counts'] = counts


		###### Saving a presentation version of the map optimised for external use
		wafer_map = WaferMap(chip_type=self.wafer_data['chip_type'],
		                     size=(10, 10), transform=(-1, -1))
		wafer_title = ''
		try:
			wafer_title = '{sn}/{batch}:  {nchips} chips'.format(
			                sn = self.wafer_data['wafer_id'],
			                batch = self.wafer_data['batch_id'],
			                nchips = len(self.wafer_map.map))
			wafer_map.set_title(wafer_title, self.wafer_data['wafer_id'])
		except KeyError:
			pass
		# Setting each chip status
		for chip, status in status_map.items():
			# Forcing grey status to red
			if status < 0:
				status = 2
			wafer_map.set_chip(chip, status, add_status_label=False)
		# Setting the chip counts for each status
		wafer_map.update_chip_counts()
		# Saving the wafer map
		out_path = self.output_path('_summary_map_ext')
		wafer_map.save(out_path)


	def draw_summary_histos(self, order, postfix=''):
		"""Drawing summary distribution of status counts for each test"""
		plot_statuses = list(self.wafer_map.STATUS_COLORS.keys())[::-1]
		# Moving grey chips to the bottom
		plot_statuses.insert(0, plot_statuses.pop(plot_statuses.index(-1)))
		n_tests = len(order)
		if n_tests < 1:
			return
		# Adding X-axis offset to represent page number
		x_offset = 2
		v_x = np.array(range(n_tests), dtype=np.uint) + x_offset
		v_y = {status: np.zeros(len(v_x), dtype=np.uint) for status in plot_statuses}
		# Incrementing the test counts for each chip-status value
		for idx_out, name in enumerate(order):
			idx_in = self.config[name]['idx']
			for _, statuses in self.chip_status.items():
				status = statuses[idx_in]
				if status in v_y:
					v_y[status][idx_out] += 1
		# Creating the figure leaving empty space underneath for text
		fig = plt.figure(figsize=self.figsize, clear=True)
		# Making the axis occupy top 2/3 of the figure
		if 'summary_text' in self.config['GLOBAL']:
			ax = fig.add_subplot(3, 1, (1, 2))
		else:
			ax = fig.add_subplot()
		# Setting titles
		ax.set_xlabel('Page number')
		ax.set_ylabel('# of chips')
		# Setting limits
		x_min = x_offset - 0.5
		x_max = max(v_x) + 0.5
		y_max = len(self.data)
		ax.set_xlim([x_min, x_max])
		ax.set_ylim([0, y_max])
		# Optimizing the number of ticks
		ax.locator_params(nbins=min(20, len(v_x)), axis='x', integer=True, steps=[1, 2, 5, 10])

		# Reducing the margins
		plt.subplots_adjust(left=0.12, right=0.94, top=0.9, bottom=0.1)
		# Drawing the chip-count bar chart for each status
		base_y = np.zeros(len(v_x), dtype=np.uint)
		for status in plot_statuses:
			ax.bar(v_x, v_y[status], bottom=base_y, color=self.COLORS_COUNTS[status], width=0.9)
			# Updating the base values for stacking
			base_y += v_y[status]

		# Dimming bars corresponding to the ignored tests
		if n_tests == len(self.test_status):
			ax.bar(v_x, abs(self.test_status-1)*y_max, color='white', alpha=0.6, width=1.0)

		# Defining the group separation lines
		separators = []
		for idx, name in enumerate(order):
			group = self.config[name]['group']
			pos = idx + x_offset - 0.5
			if idx > 0:
				name_prev = order[idx - 1]
				# Skipping configurations with the same group ID as in previous configuration
				if group == self.config[name_prev]['group']:
					continue
			separators.append((pos, group))

		# Drawing the group ID labels
		ax.text(x_max, y_max*1.04, 'Test ID',
		        horizontalalignment='right', verticalalignment='bottom')
		for pos, group in separators:
			ax.text(pos, y_max*1.005, f'{group:d}', fontsize='small',
			        horizontalalignment='left', verticalalignment='bottom')
		# Drawing vertical lines
		ax.vlines([x for x, _ in separators[1:]], 0, y_max,
		          colors=['black']*len(separators), linewidth=1)

		self.draw_summary_text(fig, top_boundary=0.3)

		# Saving the plot
		out_path = self.output_path('_summary'+postfix)
		fig.savefig(out_path)


	def draw_summary_text(self, fig, top_boundary=1.0):
		"""Draws summary text about the test under the top boundary"""
		if 'summary_text' not in self.config['GLOBAL']:
			return
		txt_rows = self.config['GLOBAL']['summary_text']
		row_height = top_boundary/(len(txt_rows) + 1)
		# Building text box row by row
		for i_row, keys_row in enumerate(txt_rows):
			txt_norm = []
			txt_bold = []
			# Building the row from chunks of text in the normal/bold font
			for key in keys_row:
				if key not in self.wafer_data:
					if self.verbose:
						print(f'WARNING: Summary key `{key}` not in wafer data')
					continue
				txt_norm.append(f'{key}: {" "*len(str(self.wafer_data[key]))}')
				txt_bold.append(f'{key}: {self.wafer_data[key]}')
			# Drawing each line from top to bottom
			pos_top = top_boundary - (i_row + 1) * row_height
			fig.text(0.04, pos_top, '   '.join(txt_norm),
			         family='monospace', fontsize=8, weight='bold', wrap=True,
			         horizontalalignment='left', verticalalignment='bottom')
			fig.text(0.04, pos_top, ' | '.join(txt_bold),
			         family='monospace', fontsize=8, wrap=True,
			         horizontalalignment='left', verticalalignment='bottom')


	def chip_status_tests(self, chip_id, chip_status, priority_min=0):
		"""Returns a list of tests responsible for the status of the chip'"""
		names = [name for name, status in self.chip_statuses[chip_id].items()
		              if status == chip_status]
		# Leaving only tests with the right priority
		names = [name for name in self.order_waferwise[priority_min] if name in names]

		return names


	def write_chip_statuses(self):
		"""Saves the list of chip statuses in JSON format"""
		statuses = (2, 1, -1)
		data = {self.STATUS_NAMES[status]: {} for status in statuses}
		# Looping over chips with their final statuses
		for chip_id, chip_status in self.wafer_map.chip_status.items():
			# Skipping statuses that are excluded
			if chip_status not in statuses:
				continue
			# Setting key names
			chip_key = f'{chip_id[0]:X}{chip_id[1]:X}'
			status_name = self.STATUS_NAMES[chip_status]
			# Getting the list of relevant tests for this chip status
			data[status_name][chip_key] = self.chip_status_tests(chip_id, chip_status, 1)
		# Saving to JSON file
		out_path = self.output_path(f'_chip_statuses_{self.merge_postfix}', extension='.json')
		with open(out_path, 'w', encoding='utf-8') as f_out:
			json.dump(data, f_out, indent = '\t')
		print(f'Chip statuses saved to: {out_path}')


	def write_wafer_data(self):
		"""Saves summary wafer data in JSON format"""
		out_path = self.output_path(f'_wafer_data_{self.merge_postfix}', extension='.json')
		with open(out_path, 'w', encoding='utf-8') as f_out:
			json.dump(self.wafer_data, f_out, indent = '\t')
		print(f'Wafer data saved to: {out_path}')


	def write_db_data(self):
		"""Adds summary information to the DB data"""
		for chip_id, chip_status in self.wafer_map.chip_status.items():
			# Getting the grade of each chip
			chip_grade =  self.STATUS_NAMES[chip_status]
			# Forcing grade of grey chips to red
			if chip_status < 0:
				chip_grade = self.STATUS_NAMES[2]
			failure_reason = None
			if chip_status != 0:
				plot_name = self.chip_status_tests(chip_id, chip_status, 1)[0]
				# Converting plot name to the group name
				group_id = self.config[plot_name]['group']
				try:
					failure_reason = self.config['GLOBAL']['group_names'][group_id]
				except KeyError:
					# Using directly the name of the plot if no group defined for it
					failure_reason = plot_name
			# Updating the DB dictionary
			self.db_data[chip_id].update({
				'GRADE': chip_grade,
				'FAILURE_REASON': failure_reason,
			})
		# Continue with the default JSON writing
		super().write_db_data()

		# Constructing the label of the wafer
		wafer_batch, wafer_id = (self.wafer_data[n] for n in ['batch_id', 'wafer_id'])
		wafer = get_wafer(wafer_batch, wafer_id)
		if not wafer:
			raise RuntimeError(f"Failed retrieving wafer ID from file: {self.plot_data['file_id']}")
		# Label format: <BatchN>_<WaferID>_<ChipRow><ChipColumn>
		label = f"{wafer.foundry_batch_number}_{wafer_id}"
		# Saving additional DB data for the wafer
		wafer_db_data = {
			'LABEL': label,
			'WAFER_ID': self.wafer_data['wafer_id'],
			'BATCH_ID': self.wafer_data['batch_id'],
			'PROBE_CARD_ID': self.wafer_data['probe_card_id'],
			'PROBE_CARD_VERSION': self.wafer_data['probe_card_version'],
			'PROBE_SITE': 'Torino',  # FIXME: to be taken from the global config file
			'TEST_START': self.wafer_data['start_time'],
			'CUT_VERSION': self.config['GLOBAL']['version'],
			# FIXME: Replace with an actual bit string
			'ENABLED_TESTS': '1'*len(self.wafer_data['enabled_tests']),
			'NOTES': self.wafer_data['notes']
		}
		out_path = self.output_path(f'db/wafer_{self.merge_postfix}', extension='.json')
		with open(out_path, 'w', encoding='utf-8') as f_out:
			json.dump(wafer_db_data, f_out, indent='\t')
		print(f'Wafer DB data saved to: {out_path}')


	def run(self, save_summary=True):
		"""Running the configured analysis"""
		# Sorting plot configurations
		self.sort_configs()

		# Loading the data first
		self.load_data()

		# Creating the wafer map for visualisation
		self.wafer_map = WaferMap(chip_type=self.wafer_data['chip_type'],
		                          size=(10, 10))

		# Creating the arrays of test statuses for each chip
		for chip_id in self.wafer_map.map:
			self.chip_status[chip_id] = np.ones(len(self.order_waferwise[0]), dtype=np.int8) * -2
		self.test_status = np.zeros(len(self.order_waferwise[0]), dtype=np.int8)

		# Performing the analysis of each test
		super().run()

		if save_summary:
			# Drawing the summary wafer map
			self.draw_summary_map()

			# Writing lists of chip statuses
			self.write_chip_statuses()

			# Writing wafer summary data
			self.write_wafer_data()

			# Writing data for the DB upload
			self.write_db_data()

			# Drawing the summary histograms
			for prt, order in self.order_waferwise.items():
				postfix = '' if prt == 0 else f'_{prt}'
				self.draw_summary_histos(order, postfix)


	def merge_summary(self):
		"""Generates summary PDFs merging together the plots from individual tests"""
		out_dir = os.path.join(self.out_dir, 'summary')
		if not os.path.isdir(out_dir):
			os.mkdir(out_dir)

		# Path to the summary histogram with test info
		path_hst_summary = None

		# Processing the wafer-wise summary plots
		for priority, prefix in {0: 'all_', 1: ''}.items():
			if self.n_plotted['wafer'] < 1:
				break
			in_dir = self.out_dir
			pdf_writer = PdfFileWriter()
			out_path = os.path.join(out_dir, f'wafer_wise_{prefix}{self.merge_postfix}.pdf')
			pdf_file = open(out_path, 'wb')
			# Building the list of filenames without the extension
			names = list(self.order_waferwise[priority])
			# Putting the summary map at the beginning
			if priority == 0:
				names.insert(0, '_summary')
			else:
				names.insert(0, f'_summary_{priority}')
			# Calculating the merged page size based on the wafer-map dimensions
			size_out = (21, 10)
			sum_map = os.path.join(in_dir, '_summary_map.pdf')
			if os.path.isfile(sum_map):
				page_map = PdfFileReader(sum_map).getPage(0)
				size_map = tuple(page_map.mediaBox[2:])
				size_out = (size_map[0]*2.1, size_map[1])

			# Opening the histogram and map file
			group_id_prev = None
			config_glb = self.config['GLOBAL']
			for iname, name in enumerate(names):
				print(f'\rMerging wafer-wise plots: {iname+1}/{len(names)}',
				      end='', flush=True)
				# Adding a blank page
				page_out = pdf_writer.addBlankPage(size_out[0], size_out[1])
				# Placing map on the left
				path_map = os.path.join(in_dir, name+'_map.pdf')
				# Always using the same summary map file
				if name.startswith('_summary'):
					path_map = sum_map
				if os.path.isfile(path_map):
					page_map = PdfFileReader(path_map).getPage(0)
					page_out.mergeTranslatedPage(page_map, 0, 0, False)
				elif self.verbose:
					print(f'\nWARNING: Summary map not found: {path_map}')
				# Placing histogram on the right
				path_hst = os.path.join(in_dir, name+'.pdf')
				if os.path.isfile(path_hst):
					page_hst = PdfFileReader(path_hst).getPage(0)
					size_hst = tuple(page_hst.mediaBox[2:])
					# Scaling the histogram to the size of the map
					scale_hst = min(size_map[0]/size_hst[0], size_map[1]/size_hst[1])
					page_out.mergeScaledTranslatedPage(page_hst, scale_hst,
					                                   size_out[0] - size_hst[0]*scale_hst, 0,
					                                   False)
				elif self.verbose:
					print(f'\nWARNING: Summary histogram not found: {path_hst}')
				# Saving the path to the summary page for other merged PDFs
				if name == '_summary':
					path_hst_summary = path_hst
					continue
				# Adding the bookmark to the PDF if it's the first page in the group
				if name in self.config:
					group_id = self.config[name]['group']
					if group_id != group_id_prev:
						bkm_name = f'{group_id}. -'
						if 'group_names' in config_glb and group_id in config_glb['group_names']:
							group_name = config_glb['group_names'][group_id]
							bkm_name = f'{group_id}. {group_name}'
						pdf_writer.addBookmark(bkm_name, iname)
					group_id_prev = group_id
			print()
			pdf_writer.write(pdf_file)
			pdf_file.close()

		# Processing low-priority wafer-wise summary plots
		for priority, names in self.order_low_priority.items():
			if len(names) < 1:
				continue
			if priority not in self.config['GLOBAL']['low_priority_filenames']:
				print(f'WARNING: No summary filename configured for priority: {priority}')
				continue
			out_name = self.config['GLOBAL']['low_priority_filenames'][priority]
			# Creating the output file
			in_dir = self.out_dir
			pdf_writer = PdfFileWriter()
			page_out = None
			# Building the list of input paths
			files = [os.path.join(in_dir, name+'.pdf') for name in names]
			files = [f for f in files if os.path.isfile(f)]
			if not files:
				continue
			for iname, path_in in enumerate(files):
				print(f'\rMerging low-priority plots: [{out_name}] {iname+1}/{len(files)}',
				      end='', flush=True)
				# Reading the PDF to be added
				page_in = PdfFileReader(path_in).getPage(0)
				size_in = tuple(page_in.mediaBox[2:])
				size_out = (size_in[0]*2.1, size_in[1])
				# Adding new page on every even plot
				if iname%2 == 0:
					page_out = pdf_writer.addBlankPage(size_out[0], size_out[1])
				if iname%2 == 0:
					# Placing plot on the left
					page_out.mergeTranslatedPage(page_in, 0, 0, False)
				else:
					# Placing plot on the right
					page_out.mergeTranslatedPage(page_in, size_out[0] - size_in[0], 0, False)
			print()
			out_path = os.path.join(out_dir, f'{out_name}_{self.merge_postfix}.pdf')
			pdf_file = open(out_path, 'wb')
			pdf_writer.write(pdf_file)

		# Merging the per-chip plots sorted first by name, then by chip ID
		if self.n_plotted['chip'] > 0:
			in_dir = os.path.join(self.out_dir, 'chips')
			names = self.order_chipwise
			print(f'Merging chip-wise plots: {len(names)}')
			for iname, name in enumerate(names):
				print(f'{iname+1:>2d}. {name}')
				files = [f for f in os.listdir(in_dir) if re.search(f'^{name}_..\\.pdf', f)]
				if len(files) < 1:
					continue
				files.sort(key=lambda s: s.split('_'))
				files = [os.path.join(in_dir, name) for name in files]
				# Adding the summary histogram as the 1st page
				if path_hst_summary:
					files.insert(0, path_hst_summary)
				# Merging all the pages
				pdf_merger = PdfFileMerger()
				for fpath in files:
					pdf_merger.append(PdfFileReader(fpath, 'rb'))

				out_path = os.path.join(out_dir, f'{name}_{self.merge_postfix}.pdf')
				pdf_merger.write(out_path)
				pdf_merger.close()



if __name__ == "__main__":

	import argparse

	parser = argparse.ArgumentParser(description='Wafer-level analyzer of test output')
	parser.add_argument('in_dir', metavar='INPUT_DIR', type=str,
		help='Input directory')
	parser.add_argument('-o', '--out_dir', metavar='OUTPUT_DIR', type=str,
		help='Path to the output directory', default=None)
	parser.add_argument('-c', '--config', metavar='ID', type=str,
	                    help='Version number or path to the analyzer configuration file')
	parser.add_argument('-d', '--dry_run', action='store_true',
	                    help='Just produce book-keeping files without any plotting')
	parser.add_argument('-g', '--group', metavar='GROUP', type=int, nargs='+',
	                    help='Group IDs of the tests to be considered ', default=[])
	parser.add_argument('-k', '--keep', action='store_true',
	                    help='Keep all the individual plots in addition to the merged PDFs')
	parser.add_argument('-m', '--mask', metavar='FILE', type=str,
	                    help='Mask of chips to be marked as discarded')
	parser.add_argument('-n', '--name', metavar='NAME', type=str,
	                    help='Only produce plots matching the NAME pattern', default=None)
	parser.add_argument('--nomaps', action='store_true',
	                    help='Do not produce wafer maps to reduce plotting time')
	parser.add_argument('--nomerge', action='store_true',
	                    help='Skip merging of all plots into a single summary PDF')
	parser.add_argument('--noplots', action='store_true',
	                    help='Do not produce plots to save processing time')
	parser.add_argument('-p', '--priority', metavar='PRIORITY', type=int,
	                    help='Minimum priority of tests to be considered', default=-3)
	parser.add_argument('-r', '--regions', metavar='FILE', type=str,
	                    help='Path to the python file with region definitions to be used')
	parser.add_argument('-s', '--skip', metavar='CHIP_ID', type=str, nargs='+',
	                    help='List of chips to be skipped during the analysis', default=[])
	parser.add_argument('-u', '--update', action='store_true',
	                    help='Only update single plots instead of clear start')
	parser.add_argument('--stat_regions', type=float, nargs='+',
	                    help='Set chip-quality regions based on multiples of RMS', default=[])
	parser.add_argument('-v', '--verbose', metavar='1', type=int,
	                    help='Verbosity level', default=0)
	parser.add_argument('-w', '--waferonly', action='store_true',
	                    help='Only produce wafer-wise plots')
	args = parser.parse_args()

	# Setting the default output folder
	if args.out_dir is None:
		args.out_dir = os.path.join(args.in_dir, 'plots/')

	# Removing the existing output folder
	if os.path.isdir(args.out_dir) and not args.update:
		shutil.rmtree(args.out_dir)
	if args.update:
		print("WARNING: You're updating single plots")
		print("         Summary map/histogram will not be updated")

	# Creating the plotter instance
	analyzer = WaferAnalyzer(args.in_dir, args.config, args.regions,
	                         out_dir=args.out_dir, verbose=args.verbose, dry_run=args.dry_run)
	if args.nomaps:
		print('WARNING: No wafer maps will be produced')
		analyzer.save_wafer_maps = False
	if args.noplots:
		print('WARNING: No plots will be produced')
		analyzer.save_plots = False
	print(f'Analysing data from: {args.in_dir}')

	# Preselecting tests to be plotted based on command-line parameters
	for p_name, p_config in analyzer.config.items():
		# Skipping if priority is lower than requested
		if p_config['priority'] < args.priority:
			p_config['skip'] = True
			continue
		# Skipping if group is not in the list of requested
		if len(args.group) > 0 and p_config['group'] not in args.group:
			p_config['skip'] = True
			continue
		# Skipping if doesn't match the configuration name pattern
		if args.name is not None and not re.search(args.name, p_name):
			p_config['skip'] = True
			continue
		# Skipping the chip-wise plots if requested
		if args.waferonly and 'aggregate' not in p_config:
			p_config['skip'] = True

	# Ensuring that no dependencies are skipped
	for p_name, p_config in analyzer.config.items():
		if p_config['skip']:
			continue
		if 'dependencies' in p_config:
			for d_name in p_config['dependencies']:
				analyzer.config[d_name]['skip'] = False
	# Checking if any plot with DB data is skipped
	if args.verbose > 0:
		for p_name, p_config in analyzer.config.items():
			if 'db' in p_config and p_config['skip']:
				print(f'WARNING: DB data from plot `{p_name}` will be skipped')

	# Setting chips to be skipped
	for i in args.skip:
		analyzer.skip_chip(i)

	# Applying the mask with discarded chips
	if args.mask:
		analyzer.load_wafer_mask(args.mask)

	# Setting region-definition mode
	analyzer.set_stat_regions(args.stat_regions)

	# Executing the actual plotting
	analyzer.run(save_summary = not args.update)

	# Producing the merged PDFs
	if not args.nomerge and not args.dry_run:
		analyzer.merge_summary()
	else:
		args.keep = True

	# Deleting the individual plots after merging
	if not args.keep:
		for name in os.listdir(analyzer.out_dir):
			if name.startswith('_'):
				continue
			if name in ['summary', 'db']:
				continue
			path = os.path.join(analyzer.out_dir, name)
			if os.path.isfile(path):
				os.remove(path)
			elif os.path.isdir(path):
				shutil.rmtree(path)


	print(f'Saved plots: {analyzer.n_plotted}')
	print(f'         in: {analyzer.out_dir}')
