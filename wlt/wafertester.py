#!/usr/bin/env python3
"""
Module used for handling the wafer probing component of the testing.

This module defines the WaferTester class, used to handle the communication with
the probe station, along with Exception classes related to the probe station.

This module imports the probe station library from wlt.hardware.probe_station.
"""

import os
import logging
import time
import json
import sys
import gpib

from wlt.duts.chips import CHIPS
from wlt.duts.wafers import WAFER_MAPS

import wlt.chiptester
from wlt import AbortWaferprobing
from wlt import HighContactResistance
from wlt.config import main_config
from wlt.hardware.probe_station import ProberError, cm300xi


#TODO: move part of the probe station control in the dedicated library
#TODO: add optional list of chips to test?


#TODO: move to probe station library
class DieAddressError(Exception):
	"""Exception raised when a non-existent die on the wafer map is requested
	for probe station movement."""


#TODO: move to probe station library
class PatternMatchingError(Exception):
	"""Exception raised when the probe station fails to perform pattern
	matching."""


#TODO: move to probe station library
class CommunicationError(Exception):
	"""Exception raised when it's not possible to communicate with the probe
	station."""


class ProbingError(Exception):
	"""Exception raised when a problem during the probing of the wafer has been 
	detected."""


class WaferTester():
	"""
	Class used for managing the wafer probing component of the testing.

	This class interacts with the probe station in order to move to the chip
	under test, set the chuck to a certain distance from the wafer and check
	that no pattern matching errors occur.
	"""

	#TODO: remove mov_wait_time
	def __init__(self, batch_id, wafer_id, chip_type, gpib_address = None,
			n_chips_test = None, start_column = None, start_row = None,
			test_chips = False, log_level = logging.INFO,
			separate_after_test = False, mov_wait_time = 5, metadata = None,
			double_contact = False):
		"""
		Initializes the WaferTester object.

		This method sets up the class-level logging, initializes the
		communication with the probe station and performs various sanity checks
		before the testing process begins.

		Parameters
		----------
		batch_id : str
			Foundry batch identifier for the wafer under test.
		wafer_id : str
			Foundry identifier for the wafer under test.
		chip_type : str
			Type of the chips under test.
		gpib_address : int
			GPIB address of the probe station.
		n_chips_test : int, optional
			Number of chips on which WaferTester will travel to.
		start_column : int, optional
			Column index of the first die to test.
		start_row : int, optional
			Row index of the first die to test.
		test_chips : bool, optional
			Selects whether to test the chips or not. If set to False, 
			WaferTester only performs the so-called wafer travel.
		log_level : int, optional
			Verbosity of the logging information.
		separate_after_test : bool, optional
			Parameter which sets whether the probe station will go to 
			separation height after completing the testing of all the chips or 
			not.
		mov_wait_time : float, optional
			Time to wait for the completion of wafer chuck movements.
		metadata : dict or None, optional
			Dictionary containing waferprobing metadata, such as the operator, 
			the hardware used and so on.
		double_contact : bool
			Boolean that configures whether to perform the touchdown two times 
			when contacting the wafer. This procedure can reduce the contact 
			resistance between the probe card needles and the pads.

		Raises
		------
		ValueError
			Raised when the chip_type parameter is invalid.
		CommunicationError
			Raised when failing to communicate with the probe station after 
			initialization.
		"""

		#setting the serial number
		self.batch_id = batch_id
		self.wafer_id = wafer_id

		#string representation of wafer information
		self.wafer_str = f"{self.batch_id}-{self.wafer_id}"

		#wait time attribute for probe station movements
		self.mov_wait_time = mov_wait_time

		#attribute which sets whether the probe station should separate from
		#the wafer at the end of the testing or not
		self.separate_after_test = separate_after_test

		#variable which stores the init time as string
		init_time = time.strftime("%Y%m%d_%H%M%S")

		#################
		# logging setup #
		#################

		#create a folder with the wafer SN (parent of work directory)
		log_dir_par_name = f"wafer_{self.wafer_str}"
		log_dir_par_path = os.path.join(main_config.DATA_DIR, log_dir_par_name)
		os.makedirs(log_dir_par_path, exist_ok=True)

		#subfolder with date and time
		log_dir_name = init_time
		log_dir_path = os.path.join(log_dir_par_path, log_dir_name)

		#try to create the folder. If it already exists, try adding an index to
		#the folder name, starting from `i` equal to 2. If this fails up to `i`
		#equal to `n_stop`, raise a RuntimeError exception
		n_stop = 5
		for i in range(2, n_stop + 1):
			try:
				os.makedirs(log_dir_path, exist_ok=False)
				break
			except FileExistsError:
				if i == n_stop:
					raise RuntimeError("Could not create log folder!") from None
				log_dir_name = f"{init_time}-{i}"
				log_dir_path = os.path.join(log_dir_par_path, log_dir_name)

		#setting data_dir attribute
		self.data_dir = log_dir_path

		#create a folder for ChipTester
		log_dir_chips = os.path.join(self.data_dir, "chips")
		os.makedirs(log_dir_chips, exist_ok=False)

		#log level attribute
		self.log_level = log_level

		#main logger
		self.logger = logging.getLogger(f"WaferTester ({self.wafer_id})")
		self.logger.setLevel(self.log_level)

		#file handler
		log_file_name = f"wafer_{self.wafer_str}_{init_time}.log"
		log_file_path = os.path.join(self.data_dir, log_file_name)
		file_handler = logging.FileHandler(log_file_path)
		file_handler.setLevel(self.log_level)

		#console handler
		console_handler = logging.StreamHandler()
		console_handler.setLevel(self.log_level)

		#defining formatter
		log_formatter = logging.Formatter(
			"%(asctime)s | %(name)-20s | %(levelname)-8s | %(message)s"
			#, "%Y-%m-%d %H:%M:%S"
		)

		#setting formatters
		file_handler.setFormatter(log_formatter)
		console_handler.setFormatter(log_formatter)

		#adding handlers
		self.logger.addHandler(file_handler)
		self.logger.addHandler(console_handler)

		###########
		# testing #
		###########

		#setting the test_chips data member
		# True: perform chip testing
		# False: only perform "wafer travel"
		self.test_chips = test_chips
		test_status = "enabled" if self.test_chips is True else "disabled"
		self.logger.warning(f"Chip testing is {test_status}")

		###############
		# wafer setup #
		###############

		#chip type
		if chip_type not in CHIPS:
			message = f"Wrong chip type: {chip_type}!"
			self.logger.error(message)
			raise ValueError(message)
		self.chip_type = chip_type

		#wafer map
		self.wafer_map = WAFER_MAPS[self.chip_type]["map"]
		self.n_chips_wafer = WAFER_MAPS[self.chip_type]["chip_count"]

		#number of chips to test
		if n_chips_test is not None:
			if n_chips_test in range(0, self.n_chips_wafer + 1):
				self.n_chips_test = n_chips_test
			else:
				self.logger.error("Invalid number of chips to test: "
					f"{n_chips_test}. Setting it to 0")
				self.n_chips_test = 0
		else:
			self.n_chips_test = self.n_chips_wafer

		#starting position
		if (start_column is not None) and (start_row is not None):
			if (start_column, start_row) not in self.wafer_map:
				message = ("Invalid starting position: "
					f"{start_column}-{start_row}")
				self.logger.error(message)
				raise DieAddressError(message)
			self.start_column = start_column
			self.start_row = start_row
		else:
			#if an address is not provided explicitly, the probe station is
			#instructed to go to the first die
			self.start_column = None
			self.start_row = None

		#######################
		# probe station setup #
		#######################

		#TO DO: to be moved to dedicated method; should be run in the main

		#initialization of probe station object
		self.gpib_address = gpib_address
		self.probe_station = cm300xi.CM300xi(gpib_address) #can raise exception

		try:
			#getting PS ID
			self.logger.info(self.probe_station.get_id())
		except gpib.GpibError:
			message = "Couldn't communicate with probe station"
			self.logger.error(message)
			raise CommunicationError(message) from None

		self.logger.info("Communication with probe station initialized")

		#setting probe station position
		self.current_col, self.current_row = self.get_current_die()

		#attribute for controlling double chip contact
		self.double_contact = double_contact

		###########################################
		# setting up wafer probing data attribute #
		###########################################

		#initialization of dictionary attribute used to store wafer probing data
		self.data = {}
		self.data["batch_id"] = self.batch_id
		self.data["wafer_id"] = self.wafer_id
		self.data["chip_type"] = self.chip_type
		self.data["n_chips_test"] = self.n_chips_test
		self.data["start_column"] = self.start_column
		self.data["start_row"] = self.start_row
		self.data["current_col"] = self.current_col
		self.data["current_row"] = self.current_row
		if metadata is not None:
			try:
				self.data.update(metadata)
			except Exception:
				self.logger.warning("Could not add input metadata to "
					"waferprobing data")

		#initialization of results file
		data_file_name = f"wafer_{self.wafer_str}_{init_time}.json"
		self.data_file = os.path.join(self.data_dir, data_file_name)

	def close(self):
		"""Safely terminates the execution of the wafer probing process."""

		#closing and removing log handlers
		for handler in self.logger.handlers:
			handler.close()
		self.logger.handlers.clear()

	def move_to_chip(self, chip_col, chip_row):
		"""
		Moves the probe station to (chip_col, chip_row).

		It checks whether the probe station is already on the selected die and,
		in that case, no further actions are taken.

		After the movement, the probe station reply is inspected to search for
		pattern matching errors. If none are found, the current_col and
		current_row attributes are updated and the absolute position of the
		probe station chuck is printed in the log.

		Parameters
		----------
		chip_col : int
			Column index of the die to move to.
		chip_row : int
			Row index of the die to move to.

		Raises
		------
		DieAddressError
			Raised in case (chip_col, chip_row) is not present in the wafer map.
		"""

		#check if (chip_col, chip_row) is in the wafer map
		if (chip_col, chip_row) not in self.wafer_map:
			message = f"Requested movement to invalid position: {chip_col:X}{chip_row:X}"
			self.logger.error(message)
			raise DieAddressError(message)

		#check if already on that chip
		is_current_col = (self.current_col == chip_col)
		is_current_row = (self.current_row == chip_row)
		if is_current_col and is_current_row:
			self.logger.debug(f"Already on chip {chip_col:X}{chip_row:X}")
			return

		#performing the requested movement
		self.logger.info(f"Moving to die {chip_col:X}{chip_row:X}")
		self.probe_station.goto_die(chip_col, chip_row)

		#sleeping and checking for pattern matching errors. Both should be moved
		#to the probe station library
		time.sleep(self.mov_wait_time)
		self._check_movement_message()

		#updating position
		self.current_col, self.current_row = self.get_current_die()

		#printing absolute position
		x_pos, y_pos, z_pos = self.probe_station.get_position()
		self.logger.debug(f"x = {x_pos}, y = {y_pos}, z = {z_pos}")

	def move_to_chip_number(self, number):
		"""
		Moves the probe station to the die with the requested number.

		It checks whether the probe station is already on the selected die and,
		in that case, no further actions are taken.

		After the movement, the probe station reply is inspected to search for
		pattern matching errors. If none are found, the current_col and
		current_row attributes are updated and the absolute position of the
		probe station chuck is printed in the log.

		Parameters
		----------
		number : int
			Chip number on the probe station wafer map.

		Raises
		------
		DieAddressError
			Raised in case (chip_col, chip_row) is not present in the wafer map.
		"""

		#check if `number` is in the wafer map
		if number not in range(1, len(self.wafer_map) + 1):
			message = f"Requested movement to invalid chip number: {number}"
			self.logger.error(message)
			raise DieAddressError(message)

		#check if already on that chip
		current_col, current_row = self.get_current_die()
		current_number = self.probe_station.get_die_number(current_col, current_row)
		if (current_number == number):
			self.logger.debug(f"Already on chip number {number}")
			return

		#performing the requested movement
		self.logger.info(f"Moving to die number {number}")
		self.probe_station.goto_die_number(number)

		#sleeping and checking for pattern matching errors. Both should be moved
		#to the probe station library
		time.sleep(self.mov_wait_time)
		self._check_movement_message()

		#updating position
		self.current_col, self.current_row = self.get_current_die()

		#printing absolute position
		x_pos, y_pos, z_pos = self.probe_station.get_position()
		self.logger.debug(f"x = {x_pos}, y = {y_pos}, z = {z_pos}")

	def move_to_first_chip(self):
		"""
		Moves the probe station to the first die in the wafer.

		After the movement, the probe station reply is inspected to search for
		pattern matching errors. If none are found, the current_col and
		current_row attributes are updated and the absolute position of the
		probe station chuck is printed in the log.
		"""

		#performing the requested movement
		self.logger.info("Moving to the first die")
		try:
			self.probe_station.goto_first_die()
		except ProberError as e:
			self.logger.exception(e)
			raise

		#updating position
		self.current_col, self.current_row = self.get_current_die()

		#printing absolute position
		x_pos, y_pos, z_pos = self.probe_station.get_position()
		self.logger.debug(f"x = {x_pos}, y = {y_pos}, z = {z_pos}")

	def move_to_next_chip(self):
		"""
		Moves the probe station to the next die in the wafer.

		After the movement, the probe station reply is inspected to search for
		pattern matching errors. If none are found, the current_col and
		current_row attributes are updated and the absolute position of the
		probe station chuck is printed in the log.
		"""

		#performing the requested movement
		self.logger.info("Moving to the next die")
		try:
			self.probe_station.goto_next_die()
		except ProberError:
			self.logger.exception("Failed to go to the next die!")
			raise

		#updating position
		self.current_col, self.current_row = self.get_current_die()

		#printing absolute position
		x_pos, y_pos, z_pos = self.probe_station.get_position()
		self.logger.debug(f"x = {x_pos}, y = {y_pos}, z = {z_pos}")

	def get_current_die(self):
		"""
		Queries the probe station for the current position on the wafer.

		Returns
		-------
		tuple of int
			Tuple with the column and row indices with the following format:
			(column_index, row_index).
		"""

		return self.probe_station.get_die()

	def _check_movement_message(self):
		"""
		Checks the probe station message after performing the required movement.

		If a pattern matching error is detected, a PatternMatchingError
		exception is raised.

		If an empty reply from the probe station is detected, a warning is added
		to the log.

		Raises
		------
		PatternMatchingError
			Raised when a pattern matching error is detected in the probe 
			station reply to a movement message.
		"""

		#TO DO: parse position information if possible

		#this is the source of the "empty reply problems". The movement methods
		#in the probe station library do **not** update `self.reply`
		#message = self.probe_station.read_probe_message()
		message = ""
		self.logger.debug(f"Probe station message: {message}")

		#error string to search
		pattern_matching_error = self.probe_station.pattern_matching_error

		#search for error string in message
		if message is not None:
			if pattern_matching_error in message:
				self.logger.error("Pattern matching failed!")
				raise PatternMatchingError(message)
			self.logger.debug("Pattern matching performed")
		else:
			self.logger.warning("Empty reply from probe station")

	def contact_wafer(self):
		"""Moves the wafer chuck to contact height."""

		self.logger.info("Moving wafer chuck to contact height")
		self.probe_station.contact()

	def separate_wafer(self):
		"""Moves the wafer chuck to separation height."""

		self.logger.info("Moving wafer chuck to separation height")
		self.probe_station.separate()

	def dump_data(self):
		"""Saves the wafer probing data to a json file."""

		#saving data to file
		try:
			with open(self.data_file, "w") as file_:
				json_data = json.dumps(self.data, indent=4)
				file_.write(json_data)
		except Exception:
			self.logger.warning("Failed to write data to results file "
				f"{self.data_file}")

	def test_chip(self, go_to_separation = True):
		"""
		Manages the testing of the chips on the wafer.

		This method creates an instance of a `ChipTester` object and uses it in 
		order to test the current die.

		Parameters
		----------
		go_to_separation : bool, optional
			Go to separation height at the end of the testing.

		Raises
		------
		AbortWaferprobing
			If the wafer-level testing procedure has to be stopped.
		HighContactResistance
			If the contact resistance is too high.
		"""

		#initial logging
		chip_column, chip_row = self.get_current_die()
		chip_str = f"{self.wafer_str}-{chip_column:X}{chip_row:X}"
		self.logger.info(f"Testing chip {chip_str}")

		#going to contact height if not already there
		if self.probe_station.is_in_contact() is True:
			self.logger.info("Probe station already at contact height")
		else:
			self.logger.info("Contacting the wafer")
			self.contact_wafer()
			time.sleep(0.5)
			if self.double_contact is True:
				self.separate_wafer()
				time.sleep(0.5)
				self.contact_wafer()
				time.sleep(0.5)

		#check that it's in contact
		if self.probe_station.is_in_contact() is False:
			raise ProbingError("The probe station is not at contact height!")

		#data dir for ChipTester
		data_dir_chiptester = os.path.join(self.data_dir, "chips")

		#initializing an instance of ChipTester
		chip_tester = wlt.chiptester.ChipTester(
			chip_type=self.chip_type,
			chip_col=chip_column,
			chip_row=chip_row,
			batch_id=self.batch_id,
			wafer_id=self.wafer_id,
			enabled_tests=main_config.CHIPTESTER_CONF["enabled_tests"],
			config_tests=main_config.CHIPTESTER_CONF["config_tests"],
			data_dir=data_dir_chiptester,
			log_level=main_config.CHIPTESTER_CONF["log_level"],
		)

		#get the file handler from self.logger. There should be only **one**
		#FileHandler in self.logger.handlers
		file_handler = [fh for fh in self.logger.handlers
			if isinstance(fh, logging.FileHandler)][0]

		#add WaferTester file handler to the ChipTester logger in order to get
		#the CT log entries also in the WT log
		chip_tester.logger.addHandler(file_handler)

		#start testing
		try:
			chip_tester.main()
		except KeyboardInterrupt:
			self.logger.warning("Caught a keyboard interrupt, stopping the "
				"testing")
			raise
		except HighContactResistance:
			raise
		except AbortWaferprobing:
			raise
		#the `finally` block gets executed **before** the exception is raised
		finally:
			#explicitly remove log handler from chiptester
			chip_tester.logger.removeHandler(file_handler)
			chip_tester.close()
			if go_to_separation is True:
				self.separate_wafer()
		self.logger.info("Testing completed")

	def main(self):
		"""
		Wafer probing main: loops on all requested dies and performs the enabled
		tests.

		If the `test_chips` attribute is True, an instance of ChipTester is 
		created for the chip under test. If test_chips is False, no chip-level 
		testing is performed and this method performs a so-called wafer travel.

		The logging information from ChipTester is added to the WaferTester 
		logger.
		"""

		#logging
		current_col, current_row = self.get_current_die()
		self.logger.info(f"Starting testing of wafer {self.wafer_str}")
		self.logger.info(f"Chip type: {self.chip_type}")
		self.logger.info(f"Chip count: {self.n_chips_wafer}")
		self.logger.info(f"Chips to test: {self.n_chips_test}")
		self.logger.info(f"Starting die: {self.start_column}-{self.start_row}")
		self.logger.info(f"Current position: {current_col}-{current_row}")

		#store the starting time in the `data` attribute
		time_start = time.time()
		time_fmt_str = "%Y-%m-%d %H:%M:%S"
		start_time_struct = time.localtime(time_start)
		self.data["start_time"] = time.strftime(time_fmt_str, start_time_struct)
		self.data["stop_time"] = None
		self.data["elapsed_time"] = 0

		#writing `data` attribute to JSON summary file
		self.dump_data()

		#getting the number of the first die
		self.logger.info("Getting number of starting die")
		if (self.start_column is not None) and (self.start_row is not None):
			start_num = self.probe_station.get_die_number(
				self.start_column,
				self.start_row
			)
		else:
			start_num = 1

		#constructing the range of die numbers
		die_numbers = range(start_num, start_num + self.n_chips_test)

		#counter and threshold for the number of consecutive resistance errors
		#that can happen before stopping the waferprobing process
		#TODO: make the threshold configurable
		res_err_cntr = 0
		res_err_thr = 3

		#loop on requested dies
		for i in die_numbers:
			#moving the probe station to the correct die
			try:
				self.move_to_chip_number(i)
			except ProberError:
				self.logger.error("Skipping chip due to probe station error!")
				continue

			#getting the current die
			chip_column, chip_row = self.get_current_die()
			self.logger.info(f"Chip {i}, ID: {chip_column:X}{chip_row:X}")

			#booleans to control whether to go to separation height after
			#testing a chip or not
			is_last_die = (i == start_num + self.n_chips_test - 1)
			is_separation_enabled = ((not is_last_die) or
				(is_last_die and self.separate_after_test))

			#store the current die in the wafer JSON
			self.data["current_col"] = self.current_col
			self.data["current_row"] = self.current_row
			self.dump_data()

			#chip testing is performed here if it is enabled
			if self.test_chips is True:
				try:
					#test the chip
					self.test_chip(go_to_separation=is_separation_enabled)
					#reset the resistance errors counter
					res_err_cntr = 0
					#update the elapsed time in the JSON
					elapsed_time_part_s = time.time() - time_start
					self.data["elapsed_time"] = float(f"{elapsed_time_part_s:.3f}")
					self.dump_data()
				#checking for high contact resistance
				except HighContactResistance:
					res_err_cntr += 1
					if res_err_cntr >= res_err_thr:
						#if the double contact is disabled, enable it
						if self.double_contact is False:
							self.logger.warning(f"Catched {res_err_thr} "
								"contact resistance errors consecutively, "
								"enabling double contact!"
							)
							self.double_contact = True
						#if the double contact is already enabled, stop testing
						else:
							self.logger.error("Waferprobing has been aborted due "
								f"to {res_err_thr} consecutive measurements of "
								"high contact resistance!")
							break
				#checking for a probing error
				except ProbingError:
					self.logger.error("A probing error has been detected, "
						"skipping chip!")
				#checking for other problems
				except (AbortWaferprobing, KeyboardInterrupt):
					self.logger.error("Waferprobing has been aborted!")
					break
			else:
				self.logger.info("Skipping testing of chip "
					f"{chip_column:X}{chip_row:X}")

		#get the elapsed time
		time_stop = time.time()
		elapsed_time_s = time_stop - time_start
		elapsed_time_h = elapsed_time_s / 3600.

		#store the elapsed time
		stop_time_struct = time.localtime(time_stop)
		self.data["stop_time"] = time.strftime(time_fmt_str, stop_time_struct)
		self.data["elapsed_time"] = float(f"{elapsed_time_s:.3f}")
		self.dump_data()

		#final logging
		self.logger.info(f"Completed testing of wafer {self.wafer_str}")
		self.logger.info(f"Elapsed time [h]: {elapsed_time_h:2.2f}")


if __name__ == "__main__":
	#WaferTester object initialization
	try:
		wafer_tester = WaferTester(**main_config.WAFERTESTER_CONF)
	except Exception as e:
		sys.exit("An exception has occurred while trying to initialize a "
			f"WaferTester instance. Error: {e}")

	#running the main() method
	try:
		wafer_tester.main()
	except Exception as e:
		print(f"Caught exception from main(): {e}")
	finally:
		#try closing the WaferTester object whether the main method raised an
		#exception or not
		try:
			wafer_tester.close()
		except Exception as e:
			sys.exit("An exception has occurred while trying to close a "
				f"ChipTester instance. Error: {e}")
